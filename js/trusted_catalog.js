
    window.TC_PRODUCT_WIDGET = true;

    

    var loadState = 0;
    var detailsData;

    var assetLoaded = function () {
        loadState++;
        if (loadState >= 2) drawWidgets();
    };

    var jQueryLoaded = function(){
        drawRichSnippet();
        assetLoaded();
    }

    // Load jQuery
    var h = document.getElementsByTagName('head')[0];
    var j = document.createElement('script');
    j.onload = jQueryLoaded;
    j.onreadystatechange = function(){
        if (this.readyState == 'complete') {
            jQueryLoaded();
        }
    }
    j.src = 'https://code.jquery.com/jquery-2.2.4.min.js';
    h.appendChild(j);

    // Load CSS
    var c = document.createElement('link');
    c.rel = 'stylesheet';
    c.type = 'text/css';
    c.onload = assetLoaded;
    c.onreadystatechange = function(){
        if (this.readyState == 'complete') {
            assetLoaded();
        }
    }
    c.href = "//assets.trustedcompany.com/1.38.3/statics/stylesheets/product_widget.css";
    h.appendChild(c);

    

    var productWidget = "<div class=\"trustedcompany-top-details\"><div class=\"trustedcompany-rating-score\"><div class=\"trustedcompany-numberic-rate\"><div class=\"trustedcompany-star-review\"><span>%STARS%</span></div></div><div class=\"trustedcompany-number-rate trustedcompany-preview-desktop\"><span>%TRUST_SCORE% de 5 estrelas</span></div><div class=\"trustedcompany-reviews\">(%REVIEW_COUNT_STRING%)</div></div><div class=\"trustedcompany-table-stars\"><div class=\"trustedcompany-progress\"><a href=\"javascript:void(0);\" onclick=\"drawDetailWidgetByRating(5)\"><span class=\"trustedcompany-groupStars\"><svg viewBox=\"0 0 24 24\" style=\"display: inline;\"><path class=\"trustedcompany-full-star\" d=\"M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z\"></path></svg><svg viewBox=\"0 0 24 24\" style=\"display: inline;\"><path class=\"trustedcompany-full-star\" d=\"M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z\"></path></svg><svg viewBox=\"0 0 24 24\" style=\"display: inline;\"><path class=\"trustedcompany-full-star\" d=\"M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z\"></path></svg><svg viewBox=\"0 0 24 24\" style=\"display: inline;\"><path class=\"trustedcompany-full-star\" d=\"M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z\"></path></svg><svg viewBox=\"0 0 24 24\" style=\"display: inline;\"><path class=\"trustedcompany-full-star\" d=\"M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z\"></path></svg></span></a><p><a href=\"javascript:void(0);\" onclick=\"drawDetailWidgetByRating(5)\">(%NUM_STARS_5%)</a></p><div class=\"trustedcompany-progress-bar\"><div class=\"trustedcompany-grey-bar\"><div class=\"trustedcompany-yellow-bar\" title=\"%NUM_STARS_5%\"><a href=\"javascript:void(0);\" onclick=\"drawDetailWidgetByRating(5)\"><progress class=\"trustedcompany-stars5\" max=\"100\" value=\"%PCT_STARS_5%\"></progress></a></div></div></div></div><div class=\"trustedcompany-progress\"><a href=\"javascript:void(0);\" onclick=\"drawDetailWidgetByRating(4)\"><span class=\"trustedcompany-groupStars\"><svg viewBox=\"0 0 24 24\" style=\"display: inline;\"><path class=\"trustedcompany-full-star\" d=\"M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z\"></path></svg><svg viewBox=\"0 0 24 24\" style=\"display: inline;\"><path class=\"trustedcompany-full-star\" d=\"M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z\"></path></svg><svg viewBox=\"0 0 24 24\" style=\"display: inline;\"><path class=\"trustedcompany-full-star\" d=\"M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z\"></path></svg><svg viewBox=\"0 0 24 24\" style=\"display: inline;\"><path class=\"trustedcompany-full-star\" d=\"M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z\"></path></svg><svg viewBox=\"0 0 24 24\" style=\"display: inline;\"><path class=\"trustedcompany-none-star\" d=\"M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z\" fill=\"#DADADA\"></path></svg></span></a><p><a href=\"javascript:void(0);\" onclick=\"drawDetailWidgetByRating(4)\">(%NUM_STARS_4%)</a></p><div class=\"trustedcompany-progress-bar\"><div class=\"trustedcompany-grey-bar\"><div class=\"trustedcompany-yellow-bar\" title=\"%NUM_STARS_4%\"><a href=\"javascript:void(0);\" onclick=\"drawDetailWidgetByRating(4)\"><progress class=\"trustedcompany-stars4\" max=\"100\" value=\"%PCT_STARS_4%\"></progress></a></div></div></div></div><div class=\"trustedcompany-progress\"><a href=\"javascript:void(0);\" onclick=\"drawDetailWidgetByRating(3)\"><span class=\"trustedcompany-groupStars\"><svg viewBox=\"0 0 24 24\" style=\"display: inline;\"><path class=\"trustedcompany-full-star\" d=\"M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z\"></path></svg><svg viewBox=\"0 0 24 24\" style=\"display: inline;\"><path class=\"trustedcompany-full-star\" d=\"M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z\"></path></svg><svg viewBox=\"0 0 24 24\" style=\"display: inline;\"><path class=\"trustedcompany-full-star\" d=\"M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z\"></path></svg><svg viewBox=\"0 0 24 24\" style=\"display: inline;\"><path class=\"trustedcompany-none-star\" d=\"M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z\" fill=\"#DADADA\"></path></svg><svg viewBox=\"0 0 24 24\" style=\"display: inline;\"><path class=\"trustedcompany-none-star\" d=\"M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z\" fill=\"#DADADA\"></path></svg></span></a><p><a href=\"javascript:void(0);\" onclick=\"drawDetailWidgetByRating(3)\">(%NUM_STARS_3%)</a></p><div class=\"trustedcompany-progress-bar\"><div class=\"trustedcompany-grey-bar\"><div class=\"trustedcompany-yellow-bar\" title=\"%NUM_STARS_3%\"><a href=\"javascript:void(0);\" onclick=\"drawDetailWidgetByRating(3)\"><progress class=\"trustedcompany-stars3\" max=\"100\" value=\"%PCT_STARS_3%\"></progress></a></div></div></div></div><div class=\"trustedcompany-progress\"><a href=\"javascript:void(0);\" onclick=\"drawDetailWidgetByRating(2)\"><span class=\"trustedcompany-groupStars\"><svg viewBox=\"0 0 24 24\" style=\"display: inline;\"><path class=\"trustedcompany-full-star\" d=\"M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z\"></path></svg><svg viewBox=\"0 0 24 24\" style=\"display: inline;\"><path class=\"trustedcompany-full-star\" d=\"M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z\"></path></svg><svg viewBox=\"0 0 24 24\" style=\"display: inline;\"><path class=\"trustedcompany-none-star\" d=\"M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z\" fill=\"#DADADA\"></path></svg><svg viewBox=\"0 0 24 24\" style=\"display: inline;\"><path class=\"trustedcompany-none-star\" d=\"M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z\" fill=\"#DADADA\"></path></svg><svg viewBox=\"0 0 24 24\" style=\"display: inline;\"><path class=\"trustedcompany-none-star\" d=\"M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z\" fill=\"#DADADA\"></path></svg></span></a><p><a href=\"javascript:void(0);\" onclick=\"drawDetailWidgetByRating(2)\">(%NUM_STARS_2%)</a></p><div class=\"trustedcompany-progress-bar\"><div class=\"trustedcompany-grey-bar\"><div class=\"trustedcompany-yellow-bar\" title=\"%NUM_STARS_2%\"><a href=\"javascript:void(0);\" onclick=\"drawDetailWidgetByRating(2)\"><progress class=\"trustedcompany-stars2\" max=\"100\" value=\"%PCT_STARS_2%\"></progress></a></div></div></div></div><div class=\"trustedcompany-progress\"><a href=\"javascript:void(0);\" onclick=\"drawDetailWidgetByRating(1)\"><span class=\"trustedcompany-groupStars\"><svg viewBox=\"0 0 24 24\" style=\"display: inline;\"><path class=\"trustedcompany-full-star\" d=\"M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z\"></path></svg><svg viewBox=\"0 0 24 24\" style=\"display: inline;\"><path class=\"trustedcompany-none-star\" d=\"M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z\" fill=\"#DADADA\"></path></svg><svg viewBox=\"0 0 24 24\" style=\"display: inline;\"><path class=\"trustedcompany-none-star\" d=\"M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z\" fill=\"#DADADA\"></path></svg><svg viewBox=\"0 0 24 24\" style=\"display: inline;\"><path class=\"trustedcompany-none-star\" d=\"M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z\" fill=\"#DADADA\"></path></svg><svg viewBox=\"0 0 24 24\" style=\"display: inline;\"><path class=\"trustedcompany-none-star\" d=\"M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z\" fill=\"#DADADA\"></path></svg></span></a><p><a href=\"javascript:void(0);\" onclick=\"drawDetailWidgetByRating(1)\">(%NUM_STARS_1%)</a></p><div class=\"trustedcompany-progress-bar\"><div class=\"trustedcompany-grey-bar\"><div class=\"trustedcompany-yellow-bar\" title=\"%NUM_STARS_1%\"><a href=\"javascript:void(0);\" onclick=\"drawDetailWidgetByRating(1)\"><progress class=\"trustedcompany-stars1\" max=\"100\" value=\"%PCT_STARS_1%\"></progress></a></div></div></div></div><div><a id=\"tc-prod-details-widget-show-all-entries\" href=\"javascript:void(0);\" onclick=\"drawDetailWidgetByRating(-1)\" style=\"display: none;\" class=\"trustedcompany-show-all-link\">Mostrar todas as avaliações</a></div></div></div>%REVIEWS%<div class=\"trustedcompany-credit\" align=\"left\"><p>Distribuído por</p><img src=\"//assets.trustedcompany.com/1.38.3/statics/images/product-widget/trustedcompany-logo.png\"></div>";
    var individualReviewTemplate = "<div class=\"trustedcompany-body-details\"><div class=\"trustedcompany-user-date\"><span>%DATE%</span></div><div class=\"trustedcompany-user-avatar\"><span class=\"author-name\">%USERNAME%</span></div><div class=\"trustedcompany-review-body\"><div class=\"trustedcompany-user-review-wrapper\"><div class=\"trustedcompany-user-profile\"><div class=\"trustedcompany-user-name\">%USERNAME%<span class=\"verified\">Comprador Verificado</span></div><div class=\"trustedcompany-user-rate\"><div class=\"trustedcompany-star-review\">%STARS%</div></div></div><div class=\"trustedcompany-user-feedback\"><div class=\"trustedcompany-user-title\"><span>%REVIEW_TITLE%</span></div><div class=\"trustedcompany-user-comment\"><span>%REVIEW_BODY%</span></div></div></div></div>%REPLY%</div>";
    var jsonLdTemplate = "<script type=\"application/ld+json\">{ \"@context\": \"http://schema.org\", \"@type\": \"Product\", \"aggregateRating\": { \"@type\": \"AggregateRating\", \"ratingValue\": \"%TRUST_SCORE%\", \"reviewCount\": \"%REVIEW_COUNT_STRING%\" }, \"name\":\"%PRODUCT_NAME%\"}</script>";
    var replyTemplate = "<div class=\"trustedcompany-company-response-wrapper\"><div class=\"trustedcompany-company-date\"><span>%DATE%</span></div><div class=\"trustedcompany-company-profile\"></div><div class=\"trustedcompany-company-reply\">%COMPANY%&#x27;s reply</div><div class=\"trustedcompany-company-response\"><span>%REPLY_BODY%</span></div></div>";
    var starWidget = "<div><div class=\"trustedcompany-star-review\">%STARS%<div class=\"trustedcompany-reviews\">%REVIEW_COUNT_STRING%</div></div></div>";
    var stars = {
        full: "<svg viewBox=\"0 0 24 24\" style=\"display: inline;\"><path class=\"trustedcompany-full-star\" d=\"M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z\"></path></svg>",
        half: "<svg viewBox=\"0 0 24 24\" style=\"display: inline;\"><path class=\"trustedcompany-half-star\" d=\"M12 17.27 L18.18 21 l-1.64-7.03 L22 9.24 l-7.19-.61 L12 2z\" fill=\"#DADADA\"/><path class=\"trustedcompany-half-star-fill\" d=\"M12 2 L9.19 8.63 2 9.24 l5.46 4.73 L5.82 21 12 17.27z\"/></svg>",
        none: "<svg viewBox=\"0 0 24 24\" style=\"display: inline;\"><path class=\"trustedcompany-none-star\" d=\"M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z\" fill=\"#DADADA\"></path></svg>"
    };
    var customCSS = "<style id=\"TRUSTEDCOMPANY_CUSTOM_CSS\">.TRUSTEDCOMPANY_PRODUCT_WIDGET, .TRUSTEDCOMPANY_PRODUCT_WIDGET .trustedcompany-user-feedback, .TRUSTEDCOMPANY_PRODUCT_WIDGET .trustedcompany-reviews, .TRUSTEDCOMPANY_PRODUCT_WIDGET .trustedcompany-number-rate span, .TRUSTEDCOMPANY_PRODUCT_WIDGET .trustedcompany-user-name, .TRUSTEDCOMPANY_PRODUCT_WIDGET .trustedcompany-user-name .verified, .TRUSTEDCOMPANY_PRODUCT_WIDGET .trustedcompany-company-reply, .TRUSTEDCOMPANY_PRODUCT_WIDGET .trustedcompany-user-avatar .author-name { color: %TEXT_COLOR%; } .TRUSTEDCOMPANY_PRODUCT_WIDGET .trustedcompany-top-details, .TRUSTEDCOMPANY_PRODUCT_WIDGET .trustedcompany-body-details, .TRUSTEDCOMPANY_PRODUCT_WIDGET .trustedcompany-credit { background-color: %BG_COLOR%; width: %WIDTH%; } .TRUSTEDCOMPANY_PRODUCT_WIDGET .trustedcompany-top-details { display: %SHOW_SUMMARY%; } .TRUSTEDCOMPANY_PRODUCT_WIDGET .trustedcompany-number-rate { color: %PRIMARY_COLOR%; } .TRUSTEDCOMPANY_PRODUCT_WIDGET .trustedcompany-reviews { display: %SHOW_COUNT%; } .TRUSTEDCOMPANY_PRODUCT_WIDGET svg .trustedcompany-full-star, .TRUSTEDCOMPANY_PRODUCT_WIDGET svg .trustedcompany-half-star-fill { fill: %STARS_COLOR%; } .TRUSTEDCOMPANY_PRODUCT_WIDGET progress[value]::-webkit-progress-value { background-color:%PRIMARY_COLOR%; } .TRUSTEDCOMPANY_PRODUCT_WIDGET progress[value]::-moz-progress-bar { background-color:%PRIMARY_COLOR%; }</style>";
    var jQuery;
    var $;

    var ajaxUrl = {
        stars: 'https://trustedcompany.com/product-widget/stars',
        detail: 'https://trustedcompany.com/product-widget/detail'
    }

    function dataUrl (type, element) {
        return ajaxUrl[type] + '?w=b308c887-7466-43d3-9f50-899d379fb913&product_id=' + element.data('product-id')
            + '&product_url=' + encodeURIComponent(element.data('product-url'))
            + '&product_name=' + element.data('product-name')
            + '&group_id=' + element.data('group-id')
            + '&product_img=' + encodeURIComponent(element.data('product-img'));
    }

    

function simpleTemplate (templateString, mapping) {
	for (var name in mapping) {
		templateString = templateString.replace(new RegExp(name,'g'), mapping[name]);
	}
	return templateString;
}


function sortReviewsbyTimestamp (arr) {

    return arr.sort(function(a,b) {
        return b.created - a.created;
    });
}

function drawStars (rating) {
	var full_stars = Math.floor(rating);
	var half_star = (rating % 1) >= 0.5;
	var zero_stars = 5 - full_stars - (+half_star);

	var ret = '';

	while (full_stars-- > 0) {
		ret += stars.full;
	}
	if (half_star) {
		ret += stars.half;
	}
	while (zero_stars-- > 0) {
		ret += stars.none;
	}

	return ret;
}

function renderReplyText(reply){
	//if there's no reply
	if(reply === null){
		return '';
	}
	else{
		return simpleTemplate(replyTemplate,{
			'%DATE%' : timestampToDate(reply.updated),
			'%COMPANY%' : reply.name,
			'%REPLY_BODY%' : reply.feedback
		});

	}
}

function timestampToDate (UNIX_timestamp){
	var a = new Date(UNIX_timestamp * 1000);

	var months = [
		'Jan',
		'Fev',
		'Mar',
		'Abr',
		'Mai',
		'Jun',
		'Jul',
		'Ago',
		'Set',
		'Out',
		'Nov',
		'Dez'
	];

	var hours = a.getHours();
	return simpleTemplate('%DAY_OF_MONTH% %SHORT_MONTH% %FULL_YEAR%',{
		'%DAY_OF_MONTH%' : a.getDate(),
		'%SHORT_MONTH%' : months[a.getMonth()],
		'%FULL_YEAR%' : a.getFullYear()
	});
}

function ratingsCount(data, rating) {
	return data.ratingsCount ? data.ratingsCount[rating] : 0;
}

function ratingsPercent(data, rating) {
	var r = data.ratingsCount;
	if (r) {
		var total = r[5]+r[4]+r[3]+r[2]+r[1];
		var count = r[rating];
		return (count/total)*100;
	}
	else {
		return 0;
	}
}

function roundRating (n) {
	return n.toFixed(1).replace(/\.0$/,'');
}

function reviewCountString (count) {
    var plural_count = "<span class=\"trustedcompany-reviews-number\">%REVIEW_COUNT%&nbsp;</span>Avaliações ";
    var singular_count = "<span class=\"trustedcompany-reviews-number\">%REVIEW_COUNT%&nbsp;</span>Avaliação ";

    if (count > 1) {
        return simpleTemplate(plural_count, {
            '%REVIEW_COUNT%': count
        });
    }
    return simpleTemplate(singular_count, {
        '%REVIEW_COUNT%': count
    });
}

function validateRichsnippetData(data) {
    if(data &&
        (data.rating && data.rating > 0) &&
        (data.numberOfReviews && data.numberOfReviews > 0) &&
        (data.productName && data.productName.length > 0 )
    ){
        return true;
    }
    return false;
}

function renderRichSnippet (data){
    if(validateRichsnippetData(data)){
        return simpleTemplate(jsonLdTemplate,{
            '%TRUST_SCORE%' : roundRating(data.rating),
            '%REVIEW_COUNT_STRING%' : data.numberOfReviews,
            '%PRODUCT_NAME%'    :   data.productName
        });
    }
    return "";
}

function renderDetailWidget (data, rating) {

    var reviewsHtml = '';
    if (data.reviews) {
        reviewsHtml = $.map(sortReviewsbyTimestamp(data.reviews),function(r){
            if(rating==-1 || r.rating==rating){
                    return simpleTemplate(individualReviewTemplate,{
                        '%DATE%' : timestampToDate(r.created),
                        '%USERNAME%' : r.name,
                        '%STARS%' : drawStars(r.rating),
                        '%REVIEW_TITLE%' : r.title,
                        '%REVIEW_BODY%' : r.feedback,
                        '%REPLY%' : renderReplyText(r.reply)
                    });
            }
        }).slice(0,5).join('');
    }

    if (data.exist && data.numberOfReviews > 0) {
        return simpleTemplate(productWidget,{
            '%TRUST_SCORE%' : roundRating(data.rating),
            '%STARS%' : drawStars(data.rating),
            '%REVIEW_COUNT_STRING%' : reviewCountString(data.numberOfReviews),
            '%NUM_STARS_5%' : ratingsCount(data,5),
            '%NUM_STARS_4%' : ratingsCount(data,4),
            '%NUM_STARS_3%' : ratingsCount(data,3),
            '%NUM_STARS_2%' : ratingsCount(data,2),
            '%NUM_STARS_1%' : ratingsCount(data,1),
            '%PCT_STARS_5%' : ratingsPercent(data,5),
            '%PCT_STARS_4%' : ratingsPercent(data,4),
            '%PCT_STARS_3%' : ratingsPercent(data,3),
            '%PCT_STARS_2%' : ratingsPercent(data,2),
            '%PCT_STARS_1%' : ratingsPercent(data,1),
            '%REVIEWS%': reviewsHtml
        });
    }
    return '';
}

function renderStarWidget (data) {
    return simpleTemplate(starWidget,{
        '%STARS%': drawStars(data.rating),
        '%REVIEW_COUNT%': data.numberOfReviews,
        '%REVIEW_COUNT_STRING%' : reviewCountString(data.numberOfReviews)
    })
}


    function addRichSnippetToDom(data){
        $(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).append(renderRichSnippet(data));
    }

    function addDetailWidgetToDom(data,rating){
        var el = $('.TRUSTEDCOMPANY_PRODUCT_WIDGET');
        //todo - do not waste a call to server if widget data not available
        el.html(renderDetailWidget(data,rating));

        //if we are going to show all the review, hide the "Show all reviews" link
        if(rating == -1){
            $('#tc-prod-details-widget-show-all-entries').hide();
        }
        else{
            $('#tc-prod-details-widget-show-all-entries').show();

        }
    }

    function checkDetailsDataValidity(){
        if(detailsData &&
            (detailsData.nid && detailsData.nid > 0) &&
            (detailsData.productId && detailsData.productId.length > 0)
        ){
            return true;
        }
        return false;
    }

    function validateWidgetEntry(entry){
        if(entry.data('product-id').toString().length > 0 &&
            (entry.data('product-url').toString().length > 0) &&
            (entry.data('product-name').toString().length > 0)){
            return true;
        }
        return false;
    }

    function fetchDetailsData(callback,rating){
        if (checkDetailsDataValidity()){
            callback(detailsData,rating);
        } else{

            $('.TRUSTEDCOMPANY_PRODUCT_WIDGET').each(function () {
                //validate before making the API call
                var el = $(this);

                if(validateWidgetEntry(el)){
                    $.get(dataUrl('detail',el),function(data){
                        detailsData = data;
                        callback(detailsData,rating);
                    });
                }

            });


        }
    }

    function drawRichSnippet(){
        $ = window.jQuery.noConflict(true);
        jQuery = $;

        fetchDetailsData(addRichSnippetToDom,-1);
    }

    function drawDetailWidget (rating) {
        fetchDetailsData(addDetailWidgetToDom,rating);
    }

    var drawStarWidget = function() {
        console.log('media');
        $('.TRUSTEDCOMPANY_STAR_WIDGET').each(function(){
            var el = $(this);
            $.get(dataUrl('stars',el),function(data){
                if (data.exist && data.numberOfReviews > 0) {
                    el.html(renderStarWidget(data));
                }
            });
        });
    }

    drawDetailWidgetByRating=function(rating){
        drawDetailWidget(rating);
    }

    var drawWidgets = function  () {
        console.log('test5');
        $('#TRUSTEDCOMPANY_CUSTOM_CSS').remove();
        $(simpleTemplate(customCSS,{
            '%TEXT_COLOR%'    : '#7B7B7B',
            '%STARS_COLOR%'   : '#F7B101',
            '%BG_COLOR%'      : 'transparent',
            '%WIDTH%'         : '100.0%',
            '%SHOW_SUMMARY%'  : 'block',
            '%PRIMARY_COLOR%' : '#F7B101',
            '%SHOW_COUNT%'    : 'inline-flex'
        })).appendTo('head');

        $(function(){
            drawDetailWidget(-1);
            drawStarWidget();
        });
    }