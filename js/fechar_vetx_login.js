$(window).load(function(){
	if ($("#vtexIdUI-global-loader").is(":visible") == true) { 
		$('.modal-header').append('<button type="button" class="close vtexIdUI-close" ng-show="canClose" data-dismiss="modal">X</button>');
		$('.vtexIdUI-close').click(function(){
			$('#vtexIdUI-global-loader, #vtexIdContainer').fadeOut();
			var novaURL = "/";
			$(window.document.location).attr('href',novaURL);
		});
	}
});