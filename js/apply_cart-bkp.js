function apply_cart() {
    $('.cart_header').bind({
        mouseenter: function() {
            if ($('.portal-minicart-ref table tbody').html() != "") {
                $('.cart_header .carrinho').addClass('ativo');
                $('.vtexsc-cart').stop(true, true).slideDown(500);
            }
        },
        mouseleave: function() {
            if ($('.portal-minicart-ref table tbody').html() != "") {
                $('.cart_header .carrinho').removeClass('ativo');
                $('.vtexsc-cart').stop(true, true).slideUp(500);
            }
        }
    });
}
apply_cart();

$(window).load(function() {
    $('.portal-minicart-ref .v2-vtexsc-cart.vtexsc-cart.mouseActivated.preLoaded').append('<div class="frete"></div>');
    // $('#busca_textos #textos_header #banner_cart .cart_header .carrinho').attr('href', 'javascript: void(0);');
});