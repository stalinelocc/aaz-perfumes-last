// Função para resolber o problema de genero repetido em categoria e filtro

//CONFIGURA GENERO
var genero_repetido = function() {

	var urlStart = window.location.href;
	var minUrlStart = urlStart.toLowerCase();

	$('.search-multiple-navigator fieldset').each(function() {
		var classGroup = $('input:eq(0)', this).attr('name');
		$(this).addClass(classGroup);		
	});

	//remove filtro genero
	if (minUrlStart.indexOf('masculino') > 0 || minUrlStart.indexOf('feminino') > 0) {
		if (minUrlStart.indexOf('specificationFilter_29') <= 0) {			
			$('.search-multiple-navigator fieldset.specificationFilter_29').remove();			
		}
	}

}

//CONFIG FILTROS
var configFiltro = function() {

	$('.bt-refinar').remove();	
	$('.search-multiple-navigator fieldset').addClass('box-filtro');
	$('.search-multiple-navigator label').each(function() {

		var typeFiltro 	 = '';
		var valueFiltro  = '';
		var newName		 = '';

		if ($('input', this).length) {

			var txtNumber  = $(this).text();
			var qtdNumber  = txtNumber.split(' (');
	        newName    	   = qtdNumber[0];

	        //PARAMETRO
	        var paramFiltro1 = $('input', this).attr('rel');	        
			var paramFiltro2 = paramFiltro1.replace('fq=', '');			
			var paramFiltro  = paramFiltro2.split(':');
			
			//filtro
			typeFiltro 	 = paramFiltro[0];
			valueFiltro  = $('input', this).attr('value');

			// marca
			if (typeFiltro == 'B') {
				typeFiltro  = 'b';
				valueFiltro = (newName.replace(/ /gi, '-')).toLowerCase();
			}

			// preco
			if (typeFiltro == 'P') {
				typeFiltro = 'priceFrom';
				valueFiltro = $('input', this).val();
				valueFiltro = valueFiltro.replace('[','');
				valueFiltro = valueFiltro.replace(']','');
				valueFiltro = valueFiltro.replace(' TO ','-a-');
				valueFiltro = valueFiltro.replace(/[.]/gi, ',');
				valueFiltro = 'de-'+valueFiltro;
			}
		
			$(this).html('<input type="checkbox" class="multi-search-checkbox" value="'+valueFiltro+'" name="'+typeFiltro+'">'+newName+'');
				
		} else {
			valueFiltro = $(this).text();
			var newName = valueFiltro; 
			var typeSelected = $(this).parents('.box-filtro').find('h5').text();

			if (typeSelected == 'Marca') {
				typeFiltro  = 'b';
				valueFiltro = (valueFiltro.replace(/ /gi, '-')).toLowerCase();
			}
			if (typeSelected == 'Estilo/Ocasião') {
				typeFiltro  = 'specificationFilter_26';
			}
			if (typeSelected == 'Família Olfativa') {
				typeFiltro  = 'specificationFilter_27';
			}
			if (typeSelected == 'Gênero') {
				typeFiltro  = 'specificationFilter_29';
			}
			if (typeSelected == 'Fragrância') {
				typeFiltro  = 'specificationFilter_30';
			}
			if (typeSelected == 'Faixa de preço') {
				typeFiltro = 'priceFrom';
				valueFiltro = valueFiltro.replace('Até R$ ','de-0-a-');				
				valueFiltro = valueFiltro.replace(' – R$ ','-a-');
				valueFiltro = valueFiltro.replace('R$ ','de-'); 
				valueFiltro = valueFiltro.replace(',00','');
			}
			$(this).html('<input type="checkbox" class="multi-search-checkbox ativo" style="display:none" value="'+valueFiltro+'" name="'+typeFiltro+'">'+newName+'');
		}


		
	});
}

//REORDENA PAGINAÇÃO
var orderByPage = function(orderTotalPager, orderNumPage) {
        
    orderIntPage = parseInt(orderNumPage);
    if (orderTotalPager > 5) {
        var lastOrderPage = orderTotalPager - 2;
        $('.pager-catalogo.page-a .gt-pages a.pager').hide();
        $('.pager-catalogo.page-b .gt-pages a.pager').hide();

        if (orderIntPage > 3) {

            if (orderIntPage < lastOrderPage) {
                $('.pager-catalogo.page-a .gt-pages a.pager').slice(orderIntPage-3, orderIntPage+2).css('display', 'block');
                $('.pager-catalogo.page-a .gt-pages a.first-page, .pager-catalogo.page-a .gt-pages a.last-page').removeClass('inativo').attr('rel','');
                
                $('.pager-catalogo.page-b .gt-pages a.pager').slice(orderIntPage-3, orderIntPage+2).css('display', 'block');
                $('.pager-catalogo.page-b .gt-pages a.first-page, .pager-catalogo.page-b .gt-pages a.last-page').removeClass('inativo').attr('rel','');

            } else {
                $('.pager-catalogo.page-a .gt-pages a.pager').slice(lastOrderPage-3, lastOrderPage+2).css('display', 'block');
                $('.pager-catalogo.page-b .gt-pages a.pager').slice(lastOrderPage-3, lastOrderPage+2).css('display', 'block');

                //ultima
                if (orderIntPage == orderTotalPager) {
                    $('.pager-catalogo.page-a .gt-pages a.last-page').addClass('inativo').attr('rel','null');
                    $('.pager-catalogo.page-a .gt-pages a.pager').last().addClass('active').attr('rel','null');
                    $('.pager-catalogo.page-b .gt-pages a.last-page').addClass('inativo').attr('rel','null');
                    $('.pager-catalogo.page-b .gt-pages a.pager').last().addClass('active').attr('rel','null');
                    $('.pager-catalogo.page-a .gt-pages a.first-page').removeClass('inativo').attr('rel','');
                	$('.pager-catalogo.page-b .gt-pages a.first-page').removeClass('inativo').attr('rel','');

                } else {
                    $('.pager-catalogo.page-a .gt-pages a.last-page').removeClass('inativo').attr('rel','');
                    $('.pager-catalogo.page-b .gt-pages a.last-page').removeClass('inativo').attr('rel','');
                }
           }
        } else {
            $('.pager-catalogo.page-a .gt-pages a.pager').slice(0,5).css('display', 'block');
            $('.pager-catalogo.page-a .gt-pages a.last-page').removeClass('inativo').attr('rel','');
            $('.pager-catalogo.page-b .gt-pages a.pager').slice(0,5).css('display', 'block');
            $('.pager-catalogo.page-b .gt-pages a.last-page').removeClass('inativo').attr('rel','');

            //primeira
            if (orderIntPage == 1) {
                $('.pager-catalogo.page-a .gt-pages a.first-page').addClass('inativo').attr('rel','null');
                $('.pager-catalogo.page-b .gt-pages a.first-page').addClass('inativo').attr('rel','null');

            } else {
                $('.pager-catalogo.page-a .gt-pages a.first-page').removeClass('inativo').attr('rel','');
                $('.pager-catalogo.page-b .gt-pages a.first-page').removeClass('inativo').attr('rel','');
            }
        }
    }
}



//NAV PAGINAÇÃO
var nav_pager = function(){
	
	$('body').on('click', '.gt-pages a.pager', function(){

		var positionPager = $(this).parents('.pager-catalogo').attr('id');		
		if (positionPager == 'pgbot') {
			$('html, body').animate({scrollTop: 0}, 500);
		}

	    if ($(this).attr('rel') != 'null') {
	        var urlLoadPage      = $('.content-catalogo').attr('rel');
	        var currentPage      = $(this).attr('rel');
	        var currentTotalPage = $(this).parent('.gt-pages').attr('rev');
	        var currentNumPage   = $(this).text();

	        $('.pager-catalogo.page-a .gt-pages a').removeClass('active').attr('rel','');
	        $('.pager-catalogo.page-b .gt-pages a').removeClass('active').attr('rel','');
	        $('.pager-catalogo.page-a .gt-pages a.np-'+currentNumPage).addClass('active').attr('rel','null');
	        $('.pager-catalogo.page-b .gt-pages a.np-'+currentNumPage).addClass('active').attr('rel','null');        
	        orderByPage(currentTotalPage, currentNumPage);
	        loadPageCatalogo(urlLoadPage, currentNumPage, false, false);

	    }
	});

	$('body').on('click', '.gt-pages a.first-page', function(){
		var positionPager = $(this).parents('.pager-catalogo').attr('id');
	    if ($(this).attr('rel') != 'null') {
	        $('#'+positionPager+' .gt-pages a.pager:eq(0)').click();
	    }
	});

	$('body').on('click', '.gt-pages a.last-page', function(){
		var positionPager = $(this).parents('.pager-catalogo').attr('id');
	    if ($(this).attr('rel') != 'null') {
	        $('#'+positionPager+' .gt-pages a.pager').last().click();
	    }
	});
}

var nav_order = function(){
	$('body').on('change', '#gt-order', function(){
		var nextOrder = $(this).val();		
		var urlNexOrder = $('.content-catalogo').attr('rel');
		if (urlNexOrder.indexOf('O=') > 0) {
			urlNexOrder = urlNexOrder.split('O=');
			urlNexOrder = urlNexOrder[0]+'O='+nextOrder;
		} else {
			urlNexOrder = urlNexOrder+'&O='+nextOrder;
		}


		$('.content-catalogo').attr('rel',urlNexOrder);
		loadPageCatalogo(urlNexOrder, 1, true, false);
		
		
		var optionValue = $(this).val();
		$('option', this).attr('selected', false);		
		$(this).val(optionValue).find("option[value=" + optionValue +"]").attr('selected', true);
		


	});
}

var _paginacao = function(){
	var totalItem   = parseInt($('.info-catalogo').attr('rel'));
	var qtyPage 	= totalItem / 48;
	
	var qtyPage		= Math.ceil(qtyPage);
	
	var firstPage 	= 0;
	var lastPage  	= 47;
	var numPage = '<a class="pager np-1 active" rel="null">'+1+'</a>';
	var css_last_page = '';
		
	if (totalItem > 0) {
		if (totalItem > 48) {
			
			for (i = 1; i < qtyPage; i++) { 
				//if (i === 5) { break }
				firstPage = firstPage+48;
				lastPage  = lastPage+48;
				numPage   = numPage + '<a class="pager np-'+(i+1)+'" style="display:none">'+(i+1)+'</a>';
			}

		} else {
			css_last_page = 'inativo';
			console.log('GT Catalogo: combinação inválida');		
		}
	} else {
		$('.result-page').html('<div class="catalogo-vazio"><strong>Ops!</strong>Nenhum produto com essa combinação</div>');
		$('.prateleiras_resultado_de_busca_nao_encontrado').show();
		return false;
	}

	htmPage = '<div class="gt-pages" rev="'+qtyPage+'" rel="'+urlEnd+'"><a class="first-page inativo" title="Primeira" rel="null"><</a>'+numPage+'<a class="last-page '+css_last_page+'" title="Última">></a></div>';
	$('.pager-catalogo').html(htmPage);
	$('.pager-catalogo.page-a .gt-pages a.pager').slice(0,5).css('display', 'block');
	$('.pager-catalogo.page-b .gt-pages a.pager').slice(0,5).css('display', 'block');
	

}

//var initPage = '0 - 47';
var firstload = true;
var loadPageCatalogo = function(urlCatalogo, pageCatalogo, repage, filtro) {
	
	//VIA API
	/*
	$.ajax({
		url: "/api/catalog_system/pub/products/search"+urlCatalogo,
		headers: {
        	'resources': pageCatalogo
     	}

	}).done(function(itensApi) {
		console.log(itensApi);		
		if(itensApi == ''){
			//busca vazia
			console.log('GT Catalogo: Nenhum produto encontrado com essa combinação');

		} else {
					
			//busca cheia 
			var name = '';		
			var len = itensApi.length;
				
			//prateleira	
			for(var i = 0;i<len;i++){						
						
				name = itensApi[i].productName;
				idProduct = itensApi[i].productId;
				urlProduct = itensApi[i].link;
				brandProduct = itensApi[i].brand;
				regularPrice = itensApi[i].items[0].sellers[0].commertialOffer.LisPrice;
				bestPrice = itensApi[i].items[0].sellers[0].commertialOffer.Price;
				if (regularPrice == bestPrice) {
					regularPrice = '';
				}
			}
		}

	}).fail(function(data){	
		console.log("GT Catalogo: "+data);
	});
	*/

	//VIA ONLOAD
	$('.produtos').addClass('gt-load-2');
	if (filtro == true) {
		$('.left-nav').addClass('carrega');
	}
	$('.content-catalogo').load(urlCatalogo+'&lid=aeb07ff9-2e30-45aa-9ac7-d5bc2693d9b9&PageNumber='+pageCatalogo+' .main', function () {
		
		
		//atualiza total
		var newTotal = $('.result-page .searchResultsTime:eq(0) .resultado-busca-numero .value').text();
		$('.info-catalogo').attr('rel',newTotal);
		$('.info-catalogo').html('<b>'+newTotal+'</b> Produtos encontrados');
		if (repage == true) {
			_paginacao();
		}

		$('.left-nav').removeClass('carrega');
		$('.produtos').removeClass('gt-load-2');
		
	});
}




// filtrar
var categoryFiltro = function() {
	
	// 1 GERA A URL	
	var numFiltro = $('.multi-search-checkbox.ativo').length;
	var valFiltro = '';
	var mapFiltro = '';

	var valColecao = '';
	var mapColecao = '';
	
	var numCat = '';
	var numCat2 = 0;
	var valCat = '';
	var mapCat = '';
	var barraCat = 0;
	
	var urlOrderby = '';

	var urlCurrentN = window.location.href;
	var lowUrlCurrent = urlCurrentN.toLowerCase();
	var minUrlCurrent = urlCurrentN.toLowerCase();
	var urlTemp = minUrlCurrent;

	if (lowUrlCurrent.indexOf('productclusterids') > 1) {
		barraCat = 1;
	}

	if (urlCurrentN.indexOf('?') > 1) {
		urlTemp = urlTemp.replace('/?','?');
		urlTemp = urlTemp.split('?');
		minUrlCurrent = urlTemp[0];
	}

	var urlNext = minUrlCurrent.split('.br/');

	var urlVal = '';
	var urlMap = '';

	//categoria
	if ($('.left-nav').attr('rel') === '-1') {

		var urlCat = urlNext[1].split('/');		
		var lastCat = urlCat[urlCat.length - barraCat - 1];		
		numCat = ((urlCat.length) - barraCat) - numFiltro;

		for (i = 0; i < numCat; i++) { 
			if (urlCat[i] != '') {
				valCat = valCat+'/'+urlCat[i];
				numCat2 = numCat2+1;
				
			}
		}

		numCat = numCat2;
		$('.left-nav').attr('rel',numCat);
		$('.left-nav').attr('rev',valCat);

	} else {		
		numCat = parseInt($('.left-nav').attr('rel'));
	}
	
	for (i = 0; i < numCat; i++) { 
		mapCat = mapCat+',c';
	}

	valCatEnd = $('.left-nav').attr('rev');
	
	
	//filtro (campo, marca e preco)
	$('.multi-search-checkbox.ativo').each(function() {		
		mapFiltro = mapFiltro+','+$(this).attr('name');
		valFiltro = valFiltro+'/'+$(this).attr('value');
	});

	//colecao
	if (barraCat == 1) {
		mapColecao = ',productClusterIds';
		lastElem = urlNext[1].split('/');
		valColecao = '/'+lastElem[lastElem.length - 1];
	}	

	//ordem
	var htmOrder = '';
	var orderTemp = '';
	var thisOrder = '';
	if ($('#gt-order').length) {
		
		htmOrder = $('#gt-order').html();
		orderTemp = $('.content-catalogo').attr('rel');
		thisOrder = $('#gt-order');
	} else {
		
		htmOrder = $('.resultado-busca-filtro:eq(0) .orderBy select').html();
		orderTemp = lowUrlCurrent;
		thisOrder = $('.result-product .resultado-busca-filtro:eq(0) .orderBy select');
	}
	
	orderTemp = orderTemp.toLowerCase();
	
	if (orderTemp.indexOf('o=') > 0) {
		orderbyVal = thisOrder.find('option:selected').val();
		urlOrderby = '&O='+orderbyVal;
		
	}

	//End
	urlVal = valCatEnd+valFiltro+valColecao;
	urlMap = 'map='+mapCat+mapFiltro+mapColecao;
	urlMap = urlMap.replace('=,','=');
	urlMap = '?PS=48&'+urlMap;

	urlEnd = urlVal + urlMap + urlOrderby;

	$('.prateleiras_resultado_de_busca_nao_encontrado').hide();
	$('.result-page').html('<div class="top-catalogo"><div class="orderby-catalogo"></div><div class="info-catalogo" rel=""></div><div class="pager-catalogo page-a" id="pgtop"></div></div><div class="content-catalogo" rel="'+urlEnd+'"></div><div class="bottom-catalogo"></div><div class="pager-catalogo page-b" id="pgbot"></div>');

	console.log(urlEnd);

	//2. GERA ORDENAR	
	$('.orderby-catalogo').html('<select id="gt-order" name="gt-order">'+htmOrder+'</select>');

	//3. LOAG PAGE
	loadPageCatalogo(urlEnd,1,true, true);

}

var nav_filtro = function() {
	$('body').on('click', '.multi-search-checkbox', function() {
		$(this).toggleClass('ativo');
		categoryFiltro();
	});
}


$(document).ready(function() {

	console.log('Novo Filtro 6');

	$('.left-nav').attr('rel','-1');
	
	if ($('body.departament').length) {

		configFiltro();
		genero_repetido();
		nav_pager();
		nav_order();
		nav_filtro();
		categoryFiltro();
	}	

});


