function addFlag(){
    $('.prateleira ul').find('li').each(function(){
        if($(this).hasClass('flagAdicionada') != true){
            $(this).addClass('flagAdicionada');
        }else{
            $(this).removeClass('flagAdicionada');
        };
    });
    $('.prateleira ul').find('li.flagAdicionada').each(function(){
        var esse = $(this);
        var economiaBoleto = "";
        var precoTotal = "";
        var ecomomiaBoleto2 = "";
        var precoTotal =  esse.find('span.preco-por').text();

        precoTotal = precoTotal.split("R$ ");
        precoTotal = precoTotal[1];
        precoTotal = parseInt(precoTotal);

        var total = precoTotal;
        if(total > 150){
            esse.find('.show-buy').append('<span class="freteGratis"></span>');
        }

    });
}

$(document).ready(function(){
    $('body').append("<a id='scrollToTop' href='#'>Topo</a>");
    $(window).scroll(function(){
        if ($(this).scrollTop() > 100) {
            $('#scrollToTop').fadeIn();
        } else {
            $('#scrollToTop').fadeOut();
        }
    });
    $('#scrollToTop').click(function(){
        $('html, body').animate({scrollTop : 0},800);
        return false;
    });
});

$(window).load(function(){
    $(".gt-vitrine.prateleira:eq(0)").append('<div id="x-infinityScroll"><a href="/black-friday-2017"><span class="sprite-icon-mais-new"></span>OFERTAS</div>');
    addFlag();
});