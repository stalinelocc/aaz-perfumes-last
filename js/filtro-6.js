// filtrar
var categoryFiltro = function() {
	
	$('.lista-produtos').addClass('gt-load');
	var allURL 		= window.location.href;
	//cat
	var catFiltro0 	= vtxctx.departmentName.toLowerCase(); // perfumaria
	var depFiltro0 	= vtxctx.categoryName.toLowerCase(); // perfumes importados
	var catFiltro = catFiltro0.replace(/ /gi, '-'); //perfumaria
	var depFiltro = depFiltro0.replace(/ /gi, '-'); // perfumes-importados
	var urlCurrent 	= vtxctx.url.toLowerCase(); //www.aazperfumes.com.br
	
	var urlParam 	= '';
	var urlMap 		= '?PS=12&map=c';
	var urlOrder	= '';

	// dep, cat
	if (catFiltro != depFiltro ) {
		urlMap = '?PS=12&map=c,c';
		catFiltro = catFiltro+'/'+depFiltro;
	}

	//dep, cat, sub
	var sParamUrl = allURL.split('.br/');
	var paramUrl = sParamUrl[1];	

	if (paramUrl.indexOf('map=') < 1) {
		if (paramUrl.indexOf('/') > 0) {
			var subUrl  = paramUrl.split('/');
			if (subUrl.length > 2) {
				var mapSub = '?PS=12&map=';
				var catSub = '_barra_';

				for (i = 0; i < subUrl.length; i++) { 
					mapSub = mapSub+',c';
					catSub = catSub+'/'+subUrl[i];
				}

				
				//catFiltro = subMap+urlMapSub+catMap;

				catFiltro = catSub.replace('_barra_/','');
				urlMap 	  = mapSub.replace('map=,','map=');
				
				
			}
		}
	}


	var urlIni 		= 'http://'+urlCurrent+'/'+catFiltro;

	//paramentos
	$('.search-multiple-navigator input.ativo').each(function() {

		var paramType 	= $(this).attr('name');
		var paramValue 	= $(this).val();
		urlParam = urlParam + '/'+ paramValue;
		urlMap 	 = urlMap + ',' + paramType;

	});

	//ordem	
		
	if (allURL.indexOf('O=') > 0) {
		var orderValue = $('.orderBy select').find('option:selected').val();
		urlOrder = '&O='+orderValue;
	}

	//url
	var linkFiltro = urlIni+urlParam+urlMap+urlOrder;
	console.log(linkFiltro);
	$('.lista-produtos').attr('rel', linkFiltro);	

	//load
	$('.result-product').load(linkFiltro+' .main', function () {
		
		var totalPage = parseInt($('.resultado-busca-numero:eq(0) .value').text());	
		if (totalPage === 0) {

			$('.result-product').html('<p class="combinacao-invalida">Ops! Nenhum produto encontrado.</p>');
			$('.prateleiras_resultado_de_busca_nao_encontrado').show();

		} else {

			$('.result-product').show();
			$('.prateleiras_resultado_de_busca_nao_encontrado').hide();

			//recalcula paginacao
			var qtdPage = totalPage/12;		
			var lastPage = parseInt(qtdPage)+1; 
			
			$('.gt-pages').html('<ul class="pages" rel="'+lastPage+'"></ul>');

			for (i = 0; i < qtdPage; i++) { 
				if (i === 5) { break }
				$('.gt-pages ul').append('<li class="page-number click-page">'+(i+1)+'</li>');		   		
			}
			$('.gt-pages ul').prepend('<li class="previous click-previous">anterior</li>');
			$('.gt-pages ul').append('<li class="next click-next">próxima</li>');
				
			
			$('.gt-pages .page-number:eq(0)').addClass('pgCurrent');		
			$('.gt-pages .page-number:eq(0)').removeClass('click-page');
			$('.gt-pages ul .previous').addClass('pgEmpty');

		}

		setTimeout(function(){            
	   		$('.lista-produtos').removeClass('gt-load');	   		
	   		$('html, body').animate({scrollTop: 0}, 600);
		}, 100);
		
	});

}

var configFiltro = function() {
	console.log('filtroV6');
	
	$('.produtos').prepend('<div class="pager gt-pages"></div>');
	$('.produtos').append('<div class="pager gt-pages"></div>');
	$('.search-multiple-navigator fieldset').addClass('box-filtro');	

	$('.search-multiple-navigator label').each(function() {

		var typeFiltro 	 = '';
		var valueFiltro  = '';
		var newName		 = '';

		if ($('input', this).length) {

			var txtNumber  = $(this).text();
			var qtdNumber  = txtNumber.split(' (');
	        newName    = qtdNumber[0];

	        //PARAMETRO
	        var paramFiltro1 = $('input', this).attr('rel');
			var paramFiltro2 = paramFiltro1.replace('fq=', '');
			var paramFiltro  = paramFiltro2.split(':');

			
			typeFiltro 	 = paramFiltro[0];
			valueFiltro  = $('input', this).attr('value');

			//categoria

			// marca
			if (typeFiltro == 'B') {
				typeFiltro  = 'b';
				valueFiltro = (newName.replace(/ /gi, '-')).toLowerCase();
			}
			// preco
			if (typeFiltro == 'P') {
				typeFiltro = 'priceFrom';
				valueFiltro = $('input', this).val();
				valueFiltro = valueFiltro.replace('[','');
				valueFiltro = valueFiltro.replace(']','');
				valueFiltro = valueFiltro.replace(' TO ','-a-');
				valueFiltro = valueFiltro.replace(/[.]/gi, ',');
				valueFiltro = 'de-'+valueFiltro;
			}			
			
		} else {
			var typeSelected = $(this).parents('.box-filtro').find('h5').text();
			valueFiltro = $(this).text();
			newName = valueFiltro;
			selectedFiltro = 'selected="selected"';
			$(this).addClass('selectedFiltro');
			
			if (typeSelected == 'Marca') {
				typeFiltro  = 'b';
				valueFiltro = (valueFiltro.replace(/ /gi, '-')).toLowerCase();
			}
			if (typeSelected == 'Estilo/Ocasião') {
				typeFiltro  = 'specificationFilter_26';
			}
			if (typeSelected == 'Família Olfativa') {
				typeFiltro  = 'specificationFilter_27';
			}
			if (typeSelected == 'Gênero') {
				typeFiltro  = 'specificationFilter_29';
			}
			if (typeSelected == 'Fragrância') {
				typeFiltro  = 'specificationFilter_30';
			}
			if (typeSelected == 'Faixa de preço') {
				typeFiltro = 'priceFrom';
				valueFiltro = valueFiltro.replace('Até R$ ','de-0-a-');				
				valueFiltro = valueFiltro.replace(' – R$ ','-a-');
				valueFiltro = valueFiltro.replace('R$ ','de-'); 
				valueFiltro = valueFiltro.replace(',00','');		
			}

		}

		$(this).html('<input type="checkbox" class="multi-search-checkbox" value="'+valueFiltro+'" name="'+typeFiltro+'">'+newName+'');
		$('.selectedFiltro input').prop('checked', true );
		$('.selectedFiltro input').addClass('ativo');        
		
	});

	$('.bt-refinar').remove();
	$('body.category .produtos').removeClass('gt-load');
	categoryFiltro();

}

var loadPageFiltro = function(numPage){
		$('.lista-produtos').addClass('gt-load');
		var intPage = parseInt(numPage);		
		var lastPage = parseInt($('.gt-pages ul').attr('rel'));		
		var urlFiltroPage = $('.lista-produtos').attr('rel');
		
		var linkFiltroPage = urlFiltroPage+'&PageNumber='+numPage;
		//console.log(linkFiltroPage);
		$('.result-product').load(linkFiltroPage+' .main', function () {
			$('.gt-pages li.page-number').addClass('click-page');
			$('.gt-pages li.pgCurrent').removeClass('pgCurrent');
			
			//last
			if (intPage == lastPage) {
				$('.gt-pages li.next').removeClass('click-next');
				$('.gt-pages li.next').addClass('pgEmpty');
			} else {
				$('.gt-pages li.next').addClass('click-next');
				$('.gt-pages li.next').removeClass('pgEmpty');				
			}

			//first
			if (intPage == 1) {
				$('.gt-pages li.previous').removeClass('click-previous');
				$('.gt-pages li.previous').addClass('pgEmpty');
			} else {
				$('.gt-pages li.previous').addClass('click-previous');
				$('.gt-pages li.previous').removeClass('pgEmpty');
			}
			

			//centraliza
			console.log('Total page: '+lastPage);
			console.log('Atual page:' +intPage);		
			if ((intPage < lastPage) && (lastPage > 5)) {

				//penultimo
				if (intPage == (lastPage-1)) {					
					$('.gt-pages li.page-number:eq(0)').text(intPage-3);
					$('.gt-pages li.page-number:eq(1)').text(intPage-2);
					$('.gt-pages li.page-number:eq(2)').text(intPage-1);
					$('.gt-pages li.page-number:eq(3)').text(intPage);
					$('.gt-pages li.page-number:eq(4)').text(intPage+1);					
					$('.gt-pages li.page-number:eq(3)').addClass('pgCurrent');
					$('.gt-pages li.page-number:eq(3)').removeClass('click-page');
				} else {					

					if (intPage > 3) {
						$('.gt-pages li.page-number:eq(0)').text(intPage-2);
						$('.gt-pages li.page-number:eq(1)').text(intPage-1);
						$('.gt-pages li.page-number:eq(2)').text(intPage);
						$('.gt-pages li.page-number:eq(3)').text(intPage+1);
						$('.gt-pages li.page-number:eq(4)').text(intPage+2);
						$('.gt-pages li.page-number:eq(2)').addClass('pgCurrent');
						$('.gt-pages li.page-number:eq(2)').removeClass('click-page');
					} else {
						var numLast = parseInt($('.last-click').text());
						$('.gt-pages li.page-number:eq(0)').text(1);
						$('.gt-pages li.page-number:eq(1)').text(2);
						$('.gt-pages li.page-number:eq(2)').text(3);
						$('.gt-pages li.page-number:eq(3)').text(4);
						$('.gt-pages li.page-number:eq(4)').text(5);
						$('.gt-pages li.page-number:eq('+(numLast-1)+')').addClass('pgCurrent');
						$('.gt-pages li.page-number:eq('+(numLast-1)+')').removeClass('click-page');
						//$('.last-click').addClass('pgCurrent');
						//$('.last-click').removeClass('click-page');
					}
				}

			} else {
				$('.last-click').addClass('pgCurrent');
				$('.last-click').removeClass('click-page');
			}

			$('.lista-produtos').removeClass('gt-load');
			$('.gt-pages li.last-click').removeClass('last-click');

		});
}

//paginacao
var pageFiltro = function() {
	$('body').on('click', '.gt-pages .click-page', function() {
		$(this).removeClass('click-page');
		$(this).addClass('last-click');
		var numPage 	= $(this).text();
		loadPageFiltro(numPage);
	});

	$('body').on('click', '.gt-pages .click-next', function() {		
		$('.pgCurrent').next('li').removeClass('click-page');
		$('.pgCurrent').next('li').addClass('last-click');
		var numPage = $('.pgCurrent').next('li').text();
		loadPageFiltro(numPage);
	});

	$('body').on('click', '.gt-pages .click-previous', function() {		
		$('.pgCurrent').prev('li').removeClass('click-page');
		$('.pgCurrent').prev('li').addClass('last-click');
		var numPage = $('.pgCurrent').prev('li').text();
		loadPageFiltro(numPage);
	});
}


$(document).ready(function() {		
	
	if ($('body.departament').length) {
		configFiltro();
		pageFiltro();

		$('body').on('click', '.multi-search-checkbox', function() {
			$(this).toggleClass('ativo');
			categoryFiltro();
		});
	}

	if ($('body.resultado-busca').length) {
		$('.bread-crumb ul').append('<li><a>Resultado de Busca</a></li>');
	}
	
});


