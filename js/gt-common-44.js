var caminhoImg = '/arquivos/';
var mobile = 'nao';
var janela = innerWidth;
if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) & janela < 1004) {
    mobile = 'sim'
}
var gt_lightbox = function(gdiv, gclasse) {
    $('html, body').animate({
        scrollTop: 0
    }, 1000);
    wHeight = $(document).height();
    $('body').prepend('<div class="gt-ext-modal" style="height:' + wHeight + 'px"><div class="gt-int-modal ' + gclasse + '"><a id="iClose" class="gt-close-modal">X</a>' + gdiv + '</div></div>');
    $('body').on('click', '.gt-close-modal', function() {
        $('.gt-ext-modal').remove();
    });
}
var espiar = function() {
    $('body').on('click', '.espiar', function() {
        var idUrl = $(this).attr('rel');
        var add_image = $(this).parents('li').find('.fotoProduto a').html();
        console.log(add_image);
        htmlEspiar = '<div class="img-add">' + add_image + '</div><iframe src="/quick-view/?idproduto=' + idUrl + '&KeepThis=true&lid=5e4d8b09-8750-4ff9-ad94-8245a4284aad" frameborder="0" scrolling="no"></iframe>'
        gt_lightbox(htmlEspiar, 'jn-add');
    });
}
var open_sub = function() {
    var newMenu = '';
    $('.gt-categoria a.pai').each(function() {
        var link_name = $(this).text();
        var link_url = $(this).attr('href');
        newMenu = newMenu + '<li><a href="' + link_url + '">' + link_name + '</a></li>';
    });
    $('.menu-mobile ul').html(newMenu);
    $('body').on('click', '.burger', function() {
        $('.gt-categoria').slideToggle('fast');
        $(this).toggleClass('ativo');
    });
    var navTimers = [];
    $(".no-mobile li.pai").hover(function() {
        var id = jQuery.data(this);
        var $this = $(this);
        navTimers[id] = setTimeout(function() {
            $this.children('.nivel-1').fadeIn(300);
            navTimers[id] = "";
        }, 300);
    }, function() {
        var id = jQuery.data(this);
        if (navTimers[id] != "") {
            clearTimeout(navTimers[id]);
        } else {
            $(this).children(".nivel-1").fadeOut(200);
        }
    });
    if (mobile === 'sim') {
        $('.sub-open').addClass('click-open');
        $('.sub-open').removeAttr('href');
        $('.hover-open').removeClass('hover-open');
        $('.hover-open-2').removeClass('hover-open-2');
    }
    $('body').on('click', '.click-open-2', function() {
        $(this).next('.drop-2').slideToggle('fast');
        $(this).toggleClass('active');
    });
    $('body').on('click', '.click-open', function() {
        $('.drop').slideUp('fast');
        $('body .click-close').addClass('click-open');
        $('body .click-open').removeClass('click-close');
        $(this).addClass('click-close');
        $(this).removeClass('click-open');
        $(this).next('.drop').slideDown();
    });
    $('body').on('click', '.click-close', function() {
        $(this).removeClass('click-close');
        $(this).addClass('click-open');
        $(this).next('.drop').slideUp();
    });
    $('body').on('click', '.close-menu', function() {
        $('.open-menu').removeClass('click-close');
        $('.open-menu').addClass('click-open');
        $('.nav-user').slideUp();
    });
}

function FormatPrecoReais(num) {
    num = num.toString().replace(/\$|\,/g, '');
    if (isNaN(num)) num = "0";
    sign = (num == (num = Math.abs(num)));
    num = Math.floor(num * 100 + 0.50000000001);
    cents = num % 100;
    num = Math.floor(num / 100).toString();
    if (cents < 10) cents = "0" + cents;
    for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++) num = num.substring(0, num.length - (4 * i + 3)) + '.' + num.substring(num.length - (4 * i + 3));
    return ((sign) ? '' : '-') + 'R$ ' + num + ',' + cents;
}
var config_departamento = function() {
    $('.orderBy:eq(0) option:eq(0)').html('Ordenar por');
    $('.bread-crumb a:eq(0)').html('Home');
    $('.menu-navegue a').removeAttr('href');
    $(".show-filtro").click(function() {
        $(".esconder").slideToggle();
    });
};
var busca_vazio = function() {
    if ($('.busca-vazio').length) {
        $('.gt-menu-departamento').remove();
        $('.bread-crumb ul').append('<li>Resultado de busca</li>');
        $('.gt-produtos').css('width', '100%');
    }
}
var logo_marcas = function() {
    var nome_marca = $('.brandName a').text();
    $('.brandName a').append('<img id="imgmarca" src="' + caminhoImg + 'marca-' + nome_marca + '.gif" alt="' + nome_marca + '" title="' + nome_marca + '" />');
    $('#imgmarca').error(function() {
        $('#imgmarca').remove();
        $('.brandName').addClass('somente-texto');
    });
}
var gt_sanfona = function() {
    $('.sanfona .show-content:eq(0)').addClass('ativo');
    $('.sanfona .aba:eq(0)').addClass('show');
    $('.sanfona .show-content:eq(0) a.item').removeClass('open-aba');
    $('.sanfona .aba:eq(0)').slideDown();
    $('body').on('click', '.open-aba', function() {
        var thisAba = '.a-' + $(this).attr('rel');
        var thisBotao = '.b-' + $(this).attr('rel');
        $('.sanfona .show').slideUp(function() {
            $('.sanfona .show-content').removeClass('ativo');
            $(thisBotao).addClass('ativo');
            $('.sanfona .show-content a.item').addClass('open-aba');
            $('.sanfona .aba').removeClass('show');
            $(thisAba).addClass('show');
            $(thisBotao + ' a.item').removeClass('open-aba');
            $(thisAba).slideDown();
        });
    });
}
var voltar_topo = function() {
    $('body').on('click', '#voltartopo', function() {
        $('html, body').animate({
            scrollTop: 0
        }, 1000);
    });
    $('body').on('click', '.top-back a', function() {
        $('html, body').animate({
            scrollTop: 0
        }, 1000);
    });
};
var qtd_produto = function() {
    qtd_Estoque = $('a.buy-button').attr('style').toString();
    if (qtd_Estoque == 'display:block') {
        $('.m-qtd').show();
    } else {
        $('.m-qtd').remove();
        $('.b-frete').hide();
        $('.a-frete').hide();
    }
    $('.m-qtd').on('click', '.mais-qtd', function() {
        var qtd_Futura = parseInt($('.campo-qtd').val()) + 1;
        var url_Passada = $('a.buy-button').attr('href');
        var skuSelecionado = url_Passada.substr(0, 3);
        if (skuSelecionado == '/ch') {
            url_Atual = url_Passada.split('&qty=');
            url_Futura = url_Atual[0] + '&qty=' + qtd_Futura + '&seller=1&redirect=true&sc=1';
            $('a.buy-button').attr('href', url_Futura);
            $('.campo-qtd').val(qtd_Futura);
            $('#calculoFrete .quantity input').val(qtd_Futura);
        } else {
            alert('Por favor, selecione o modelo desejado');
        }
    });
    $('.m-qtd').on('click', '.menos-qtd', function() {
        var campoMin = parseInt($('.campo-qtd').val());
        if (campoMin > 1) {
            var qtd_Futura = parseInt($('.campo-qtd').val()) - 1;
            var url_Passada = $('a.buy-button').attr('href');
            var skuSelecionado = url_Passada.substr(0, 3);
            if (skuSelecionado == '/ch') {
                url_Atual = url_Passada.split('&qty=');
                url_Futura = url_Atual[0] + '&qty=' + qtd_Futura + '&seller=1&redirect=true&sc=1';
                $('a.buy-button').attr('href', url_Futura);
                $('.campo-qtd').val(qtd_Futura);
                $('#calculoFrete .quantity input').val(qtd_Futura);
            } else {
                alert('Por favor, selecione o modelo desejado');
            }
        }
    });
};
var nav_dicas = function() {
    $('.top-info').on('click', 'li a', function() {
        var goEscolha = $(this).attr('rel');
        $('html, body').animate({
            scrollTop: ($('#' + goEscolha).offset().top) - 120
        }, 800);
    });
}
var gt_login = function() {
    if ($('p.welcome a#login').length) {
        $('.drop-user').remove();
        $('.buttons li.user a.first').attr('id', 'login');
    } else {
        $('.drop-user').html('<li><a href="/account">Minha Conta</a></li><li><a href="/account/orders">Meus Pedidos</a></li><li><a href="/no-cache/user/logout">Sair</a></li>');
    }
    $('.buttons li.user').show();
}
var mini_Cart = function() {
    $('th.cartSkuQuantity').html('Qtd');
    $('.cartFooter a.cartCheckout').html('Finalizar pedido');
    var atualQtd = parseInt($('.minicart .amount-items-em:eq(0)').text());
    if (atualQtd == 0) {
        $('.vtexsc-cart').html('<p class="mini-cart-vazio">Você ainda não adicionou produtos em seu carrinho</p>');
    }
}
var config_produto = function() {
    $('.informacoes-produto .fn').removeClass('productName');
    $('.bread-crumb a:eq(0)').html('Home');
    if ($('.compre-junto table').length) {
        $('.junto').show();
    }
    if ($('.skuList').length < 1) {
        $('.sku-buy').show();
    }
    $('.box-product').on('click', 'h3.ini a', function() {
        var classOpen = $(this).attr('rel');
        $('.' + classOpen).slideToggle('fast');
        $(this).toggleClass('inativo');
    });
    $('.skuList').last().addClass('last');
    $('#lnkComprar').text('Compre Junto')
}
var imagem_marca = function() {
    var name_marca = $('.brandName a').text();
    var txt_marca = $('.brandName a').attr('class');
    var img_marca = txt_marca.replace('brand ', '');
    var src_marca = caminhoImg + 'titulo-' + img_marca + '.jpg';
    $('.img-marca .logo-marca').html('<img src="' + src_marca + '" alt="' + name_marca + '" />');
    $('.productDescription').prepend('<div class="marca-descricao"><img src="' + src_marca + '" id="imgmarca" alt="' + name_marca + '" /></div>');
    $('#imgmarca').error(function() {
        $('#imgmarca').remove();
        $('.img-marca .logo-marca').html(name_marca);
        $('.img-marca .logo-marca').addClass('txt-marca');
    });
}
var gt_carrossel_thumb = function() {
    if ($('.aviso-zoom').length < 1) {
        $('.apresentacao #include').append('<div class="aviso-zoom">ZOOM</div>');
    };
    var totalThumb = $('.thumbs li').length;
    var inicioThumb = 0;
    var finalThumb = totalThumb;
    var lastThumb = totalThumb + 2;
    var thumbExibe = 0;
    var thumbEsconde = 0;
    if (totalThumb > 3) {
        $('.thumbs li').hide();
        $('.thumbs li:eq(0)').show();
        $('.thumbs li:eq(1)').show();
        $('.thumbs li:eq(2)').show();
        $('.thumbs').prepend('<div class="gt_anterior" style="display:none">Anterior</div><div class="gt_proxima" style="display:block">Proxima</div>');
        $('.thumbs').addClass('thumb-slider');
    }
    $('body').on('click', '.gt_proxima', function() {
        inicioThumb = inicioThumb + 1;
        thumbEsconde = inicioThumb - 1;
        thumbExibe = inicioThumb + 2;
        $('.gt_anterior').show();
        if (inicioThumb < (totalThumb - 3)) {
            $('.gt_proxima').show();
        } else {
            $('.gt_proxima').hide();
        }
        $('.thumbs li:eq(' + thumbExibe + ')').show(300);
        $('.thumbs li:eq(' + thumbEsconde + ')').hide(300);
    });
    $('body').on('click', '.gt_anterior', function() {
        inicioThumb = inicioThumb - 1;
        thumbExibe = inicioThumb;
        thumbEsconde = inicioThumb + 3;
        $('.gt_proxima').show();
        if (inicioThumb <= 0) {
            $(this).hide();
        } else {
            $(this).show();
        }
        $('.thumbs li:eq(' + thumbExibe + ')').show(300);
        $('.thumbs li:eq(' + thumbEsconde + ')').hide(300);
    });
    $('.thumbs').show();
}
var gt_carrossel_thumb_click = function() {
    $('body').on('click', '.skuList label', function() {
        $('.thumbs').hide();
        $('.gt_anterior').remove();
        $('.gt_proxima').remove();
        setTimeout(gt_carrossel_thumb, 1000);
    });
}
var config_resenhas = function() {
    var vtex_comment = '';
    $('ul.resenhas ul.util').remove();
    $('.avaliacao ul.resenhas li').each(function() {
        console.log('teste')
        var nome_comment = $('p.dados strong', this).text();
        var txt_comment = $('.opt-texto', this).text();
        var rattin_comment = $('.rating-wrapper').html();
        if (nome_comment === 'Opinião de: ') {
            console.log('sem nome');
            nome_comment = 'Nome não publicado';
        }
        vtex_comment = vtex_comment + '<div class="box-comentario"><div class="info-comentario"><span class="nome">' + nome_comment + '</span>' + rattin_comment + '</div> <div class="texto-comentario">' + txt_comment + '</div></div>'
    });
    $('#caracteristicas td.Comentarios').prepend(vtex_comment);
    $('.box-product').removeClass('load');
}
var config_filtro = function() {
    $('.left-nav ul').hide();
    $('.left-nav fieldset div').hide();
    $('.left-nav fieldset:eq(0) div').show();
    $('.left-nav fieldset.refino-marca div').show();
    $('.left-nav fieldset:eq(0) h5').addClass('ativo');
    $('.left-nav fieldset.refino-marca h5').addClass('ativo');
    $('.left-nav h4').last().addClass('last');
    $('.left-nav fieldset:eq(0)').prepend('<p class="titulo-filtro">Filtre sua busca</p>');
    $('body').on('click', '.search-single-navigator h5', function() {
        $(this).toggleClass('ativo');
        $(this).next('ul').slideToggle('fast');
    });
    $('body').on('click', '.search-multiple-navigator h5', function() {
        $(this).toggleClass('ativo');
        $(this).next('div').slideToggle('fast');
    });
}
var num_top10 = function() {
    $('.top10 .content-top > div').each(function() {
        var numrank = 0;
        $(this).find('.rank').each(function() {
            var add_zero = '';
            numrank = numrank + 1;
            if (numrank < 10) {
                add_zero = '0'
            }
            $(this).text(add_zero + numrank);
        });
    });
}
$(document).ready(function() {
    console.log('V43');
    if ($('.resume-cart').length) {
        $('.resume-cart').minicart({
            showMinicart: true,
            showTotalizers: true
        });
    }
    $('.newsletter-button-ok').val('ENVIAR');
    voltar_topo();
    espiar();
    open_sub();
    if (window.location.href.indexOf("/?fq=H:247") > -1) {
        $(".main").prepend('<div class="fotoBuscaBanner"><img src="/arquivos/lovesale.jpg" /></div>');
        $(".left-nav").css('margin-top', '-1.8%').css('margin-bottom', '1%');
    }
    if (window.location.href.indexOf("/?fq=H:243") > -1) {
        $(".main").prepend('<div class="fotoBuscaBanner"><img src="/arquivos/banner-dia-dos-pais.jpg" /></div>');
        $(".left-nav").css('margin-top', '-1.8%').css('margin-bottom', '1%');
        $('#gcontent').css('background','url("/arquivos/bg-pag-pais.jpg") repeat');
    }
    if (window.location.href.indexOf("/?fq=H:296") > -1) {
        $(".main").prepend('<div class="fotoBuscaBanner"><img src="/arquivos/Banner-Topo-900x212-CupomDesconto-20160530_01.gif" /></div>');
        $(".left-nav").css('margin-top', '-1.8%').css('margin-bottom', '1%');
    }
    if ($('body.home').length) {
        $('.helperComplement').remove();
        $('#TRUSTEDCOMPANY_widget_101382').append('<script async src="https://d3643s33l1rfut.cloudfront.net/js/widget?w=BQYHDBEcXkJYQ1hWS1RSW0VbXUU"></script>');
        $(".carrossel .prateleira ul").each(function() {
            var t = $(this).find('>li').length;
            if (t > 4) {
                $(this).flexisel({
                    visibleItems: 4,
                    animationSpeed: 500,
                    autoPlay: false,
                    clone: false,
                    enableResponsiveBreakpoints: true,
                    responsiveBreakpoints: {
                        portrait: {
                            changePoint: 480,
                            visibleItems: 2
                        },
                        landscape: {
                            changePoint: 640,
                            visibleItems: 2
                        },
                        tablet: {
                            changePoint: 768,
                            visibleItems: 3
                        }
                    }
                });
            };
        });
        $('.video').on('click', function() {
            var rel = $(this).find('#rel').html();
            var htmlV = rel;
            gt_lightbox(htmlV, 'videoLB');
            return false;
        });
    }
    if ($('.busca-vazio').length > 0) {
        var termo = window.location.pathname;
        termo = termo.replace('/', '');
        termo = '<a title="' + termo + '" href="/' + termo + '">' + termo + '</a>';
        $('.bread-crumb ul li').append(termo);
        var conteudoBuscaVazia = '<p>Sua busca não encontrou nenhum resultado. Mas, temos diversos produtos que podem ser do seu interesse. Confira abaixo.</p>';
        $('.busca-vazio').html(conteudoBuscaVazia);
        $('.prateleiras_resultado_de_busca_nao_encontrado, .side-nao-encontrada').fadeIn();
        $('.left-nav').fadeOut();
        $('.lista-produtos').addClass('vazio');
    }
    if ($('body.top10').length) {
        num_top10();
        $('.menu-top li a').click(function() {
            var ref = $(this).attr('href');
            $('.menu-top li a').removeClass('ativo');
            $(this).addClass('ativo');
            $('.content-top > div').hide();
            $('.content-top > div' + ref).fadeIn();
            return false;
        });
    };
    if ($('body.como-escolher').length) {
        nav_dicas();
    }
    if ($('body.category').length) {
        config_departamento();
        busca_vazio();
        config_filtro();
    }
    if ($('body.como-escolher').length) {
        $('#olfativa .tags a').click(function() {
            var ref = $(this).attr('href');
            $('#olfativa .tags a').removeClass('ativo');
            $(this).addClass('ativo');
            $('#olfativa #conteudo-tabs > div').hide();
            $('#olfativa #conteudo-tabs > div' + ref).css('display', 'table-cell');
            return false;
        });
        $('#estacao .tags a').click(function() {
            var ref = $(this).attr('href');
            $('#estacao .tags a').removeClass('ativo');
            $(this).addClass('ativo');
            $('#estacao #conteudo-tabs2 > div').hide();
            $('#estacao #conteudo-tabs2 > div' + ref).css('display', 'table-cell');
            return false;
        });
    };
    if ($('body.produto').length) {
        config_produto();
        imagem_marca();
        gt_carrossel_thumb();
        gt_carrossel_thumb_click();
        $('body').on('click', '.mais-avaliacao', function() {
            $('.user-review').slideToggle('fast');
        });
        if ($('.brinde_controle ul > li').length < 1) {
            $('.brinde_controle').hide();
        };
        $('.trustedcompany-widget').append("<script>(function(){document.getElementById('trustedcompany-widget').src='//trustedcompany.com/embed/widget/v2?domain=aazperfumes.com.br&type=d&review=1&text=a';})();</script>")
        $('#TRUSTEDCOMPANY_widget_101381').append('<script async src="https://d3643s33l1rfut.cloudfront.net/js/widget?w=BQYHDBEcXkJYQ1hWS1RSW0VbXUY"></script>');
        var lk = $('.brandName a').attr('href');
        $('a.all-marcas').attr('href', lk);
    };
    if ($('body.produto.checkbox, body.produto.thumbcores').length) {
        qtd_produto();
    };
    if ($('body.conteudo').length) {
        $('.bread-crumb ul').append('<li class="last">' + $('.pinst h2:eq(0) span').text() + '</li>');
    };
    if ($('body.ajuda').length) {
        gt_sanfona();
    }
});
$(window).load(function() {});
$(document).ajaxStop(function() {
    mini_Cart();
    gt_login();
    if ($('body.produto').length) {
        config_resenhas();
    }
    if ($('body.home').length) {
        $('.helperComplement').remove();
    };
});
$(window).scroll(function(event) {
    if ($(this).scrollTop() > 100) {
        $('#voltartopo').fadeIn(200);
        $('body').addClass('top-fixed');
    } else {
        $('#voltartopo').fadeOut(200);
        $('body').removeClass('top-fixed');
        $('.gt-categoria').removeAttr('style');
        $('.burger').removeClass('ativo');
    }
});