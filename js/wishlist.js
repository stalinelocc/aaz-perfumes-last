// Document : lista de desejos
var wishilist_after_insert = function() {
	if ($('#TB_ajaxContent .save-success').length) {
		
		console.log('produto inserido na lista');
		$('.giftlist-insertsku').html('<div class="giftlist-insertsku-ok"><span class="giftlist-title-ok">Produto inserido com sucesso</span><a class="giftlist-go-ok" href="/Wishlist/verificar">Ver minha lista</a></div>');
		$('.giftlist-insertsku').show();
	}
	
	$('#TB_ajaxContent').removeClass('lista-load');
	
}

var add_wishlist = function() {
	
	$('a.glis-submit').removeAttr('href');
		
	var data = new Date();
	dia     = data.getDate();
	mes     = data.getMonth();
	ano2    = data.getYear();
	hora    = data.getHours();
	min     = data.getMinutes();
	seg     = data.getSeconds();
	mseg    = data.getMilliseconds();	
	var id_list = dia+''+mes+''+hora+''+min+''+seg+''+mseg; //nome da lista

	if ($('.glis-existing-title').length) {
		//ja tem lista
		$('.glis-submit-list').html('Confirmo');
		$('.glis-existing-title').html('Deseja incluir esse produto na sua lista de desejos?');
	} else {
		//nova lista
		$('input.glis-form-name').val(id_list);
		$('.glis-submit-new').html('Confirmo');
		$('.glis-new-title').html('Deseja incluir esse produto na sua lista de desejos?');		
	}
	
	$('body.produto #TB_ajaxContent').css('background','#fff');
	$('.giftlist-insertsku').show();
	
	if ($('.glis-save').length) {
		$('#TB_ajaxContent').addClass('lista-load');
		setTimeout(wishilist_after_insert, 1500);
	}

}

var redirecionar_lista = function() {
	
	if ($('.action-manage').length) {
		var redirectLista = $('li.action-manage:eq(0) a').attr('href');
		window.location.href = 'http://www.aazperfumes.com.br'+redirectLista;
	} else {
		$('#gcontent .int').html('<div class="lista-vazia"><h2>Você ainda não possui nenhum produto em sua Lista de Desejos</h2><a href="/">Clique aqui para conhecer nossos produto</a>');
		$('.all-list-null').show();
	}
}


var config_wishlist = function() {
	
	$('.giftlistsku table.giftlistproductsv2 th:eq(2)').html('Status');
	$('.giftlistsku table.giftlistproductsv2 th:eq(4)').html('Excluir');
	$('.giftlistsku table.giftlistproductsv2 th:eq(4)').html('Selecionar');
	$('.giftlistsku table.giftlistproductsv2 th:eq(3)').remove();	
	$('.giftlistsku table.giftlistproductsv2 td.wished').remove();
	
	$('.giftlistproductsv2 tr').each(function() {
    	var urlproduct_list = $('a.urlproduct', this).attr('href');
		$('td.purchased', this).html('<a href="'+urlproduct_list+'" class="ver-produto-lista">Adicionar ao carrinho</a>');    
    });
	
	$('.produtos-minha-lista').show();
}


$(document).ready(function() {
	console.log('lista');
	if ($('body.verificar-lista').length) {
		redirecionar_lista();
	}
	
});

$(document).ajaxStop(function(){	
	
	if ($('body.produto').length) {		
		add_wishlist();
	}
	
	if ($('body.wishlist').length) {
		config_wishlist();
	}
	
});	