// Documento JS / jQuery
// Cliente: AZ PERFUMES
// Autor: Profite
// Ultima data de alteração: 18/08/2014

// Banner
jQuery.easing.jswing = jQuery.easing.swing;
jQuery.extend( jQuery.easing, {
    def: "easeOutQuad",
    swing: function( e, t, n, r, i ) {
        return jQuery.easing[ jQuery.easing.def ]( e, t, n, r, i )
    },
    easeInQuad: function( e, t, n, r, i ) {
        return r * ( t /= i ) * t + n
    },
    easeOutQuad: function( e, t, n, r, i ) {
        return -r * ( t /= i ) * ( t - 2 ) + n
    },
    easeInOutQuad: function( e, t, n, r, i ) {
        if ( ( t /= i / 2 ) < 1 ) return r / 2 * t * t + n;
        return -r / 2 * ( --t * ( t - 2 ) - 1 ) + n
    },
    easeInCubic: function( e, t, n, r, i ) {
        return r * ( t /= i ) * t * t + n
    },
    easeOutCubic: function( e, t, n, r, i ) {
        return r * ( ( t = t / i - 1 ) * t * t + 1 ) + n
    },
    easeInOutCubic: function( e, t, n, r, i ) {
        if ( ( t /= i / 2 ) < 1 ) return r / 2 * t * t * t + n;
        return r / 2 * ( ( t -= 2 ) * t * t + 2 ) + n
    },
    easeInQuart: function( e, t, n, r, i ) {
        return r * ( t /= i ) * t * t * t + n
    },
    easeOutQuart: function( e, t, n, r, i ) {
        return -r * ( ( t = t / i - 1 ) * t * t * t - 1 ) + n
    },
    easeInOutQuart: function( e, t, n, r, i ) {
        if ( ( t /= i / 2 ) < 1 ) return r / 2 * t * t * t * t + n;
        return -r / 2 * ( ( t -= 2 ) * t * t * t - 2 ) + n
    },
    easeInQuint: function( e, t, n, r, i ) {
        return r * ( t /= i ) * t * t * t * t + n
    },
    easeOutQuint: function( e, t, n, r, i ) {
        return r * ( ( t = t / i - 1 ) * t * t * t * t + 1 ) + n
    },
    easeInOutQuint: function( e, t, n, r, i ) {
        if ( ( t /= i / 2 ) < 1 ) return r / 2 * t * t * t * t * t + n;
        return r / 2 * ( ( t -= 2 ) * t * t * t * t + 2 ) + n
    },
    easeInSine: function( e, t, n, r, i ) {
        return -r * Math.cos( t / i * ( Math.PI / 2 ) ) + r + n
    },
    easeOutSine: function( e, t, n, r, i ) {
        return r * Math.sin( t / i * ( Math.PI / 2 ) ) + n
    },
    easeInOutSine: function( e, t, n, r, i ) {
        return -r / 2 * ( Math.cos( Math.PI * t / i ) - 1 ) + n
    },
    easeInExpo: function( e, t, n, r, i ) {
        return t == 0 ? n : r * Math.pow( 2, 10 * ( t / i - 1 ) ) + n
    },
    easeOutExpo: function( e, t, n, r, i ) {
        return t == i ? n + r : r * ( -Math.pow( 2, -10 * t / i ) + 1 ) + n
    },
    easeInOutExpo: function( e, t, n, r, i ) {
        if ( t == 0 ) return n;
        if ( t == i ) return n + r;
        if ( ( t /= i / 2 ) < 1 ) return r / 2 * Math.pow( 2, 10 * ( t - 1 ) ) + n;
        return r / 2 * ( -Math.pow( 2, -10 * --t ) + 2 ) + n
    },
    easeInCirc: function( e, t, n, r, i ) {
        return -r * ( Math.sqrt( 1 - ( t /= i ) * t ) - 1 ) + n
    },
    easeOutCirc: function( e, t, n, r, i ) {
        return r * Math.sqrt( 1 - ( t = t / i - 1 ) * t ) + n
    },
    easeInOutCirc: function( e, t, n, r, i ) {
        if ( ( t /= i / 2 ) < 1 ) return -r / 2 * ( Math.sqrt( 1 - t * t ) - 1 ) + n;
        return r / 2 * ( Math.sqrt( 1 - ( t -= 2 ) * t ) + 1 ) + n
    },
    easeInElastic: function( e, t, n, r, i ) {
        e = 1.70158;
        var s = 0,
            o = r;
        if ( t == 0 ) return n;
        if ( ( t /= i ) == 1 ) return n + r;
        s || ( s = i * .3 );
        if ( o < Math.abs( r ) ) {
            o = r;
            e = s / 4
        } else
            e = s / ( 2 * Math.PI ) * Math.asin( r / o );
        return -( o * Math.pow( 2, 10 * ( t -= 1 ) ) * Math.sin( ( t * i - e ) * 2 * Math.PI / s ) ) + n
    },
    easeOutElastic: function( e, t, n, r, i ) {
        e = 1.70158;
        var s = 0,
            o = r;
        if ( t == 0 ) return n;
        if ( ( t /= i ) == 1 ) return n + r;
        s || ( s = i * .3 );
        if ( o < Math.abs( r ) ) {
            o = r;
            e = s / 4
        } else
            e = s / ( 2 * Math.PI ) * Math.asin( r / o );
        return o * Math.pow( 2, -10 * t ) * Math.sin( ( t * i - e ) * 2 * Math.PI / s ) + r + n
    },
    easeInOutElastic: function( e, t, n, r, i ) {
        e = 1.70158;
        var s = 0,
            o = r;
        if ( t == 0 ) return n;
        if ( ( t /= i / 2 ) == 2 ) return n + r;
        s || ( s = i * .3 * 1.5 );
        if ( o < Math.abs( r ) ) {
            o = r;
            e = s / 4
        } else
            e = s / ( 2 * Math.PI ) * Math.asin( r / o );
        if ( t < 1 ) return -.5 * o * Math.pow( 2, 10 * ( t -= 1 ) ) * Math.sin( ( t * i - e ) * 2 * Math.PI / s ) + n;
        return o * Math.pow( 2, -10 * ( t -= 1 ) ) * Math.sin( ( t * i - e ) * 2 * Math.PI / s ) * .5 + r + n
    },
    easeInBack: function( e, t, n, r, i, s ) {
        if ( s == undefined )
            s = 1.70158;
        return r * ( t /= i ) * t * ( ( s + 1 ) * t - s ) + n
    },
    easeOutBack: function( e, t, n, r, i, s ) {
        if ( s == undefined )
            s = 1.70158;
        return r * ( ( t = t / i - 1 ) * t * ( ( s + 1 ) * t + s ) + 1 ) + n
    },
    easeInOutBack: function( e, t, n, r, i, s ) {
        if ( s == undefined )
            s = 1.70158;
        if ( ( t /= i / 2 ) < 1 ) return r / 2 * t * t * ( ( ( s *= 1.525 ) + 1 ) * t - s ) + n;
        return r / 2 * ( ( t -= 2 ) * t * ( ( ( s *= 1.525 ) + 1 ) * t + s ) + 2 ) + n
    },
    easeInBounce: function( e, t, n, r, i ) {
        return r - jQuery.easing.easeOutBounce( e, i - t, 0, r, i ) + n
    },
    easeOutBounce: function( e, t, n, r, i ) {
        return ( t /= i ) < 1 / 2.75 ? r * 7.5625 * t * t + n : t < 2 / 2.75 ? r * ( 7.5625 * ( t -= 1.5 / 2.75 ) * t + .75 ) + n : t < 2.5 / 2.75 ? r * ( 7.5625 * ( t -= 2.25 / 2.75 ) * t + .9375 ) + n : r * ( 7.5625 * ( t -= 2.625 / 2.75 ) * t + .984375 ) + n
    },
    easeInOutBounce: function( e, t, n, r, i ) {
        if ( t < i / 2 ) return jQuery.easing.easeInBounce( e, t * 2, 0, r, i ) * .5 + n;
        return jQuery.easing.easeOutBounce( e, t * 2 - i, 0, r, i ) * .5 + r * .5 + n
    }
});
(function( e ) {
function t( t, n ) {
    this.slider = e( t );
    this._az = "";
    this._by = "";
    this._cx = "";
    var r = this;
    this.settings = e.extend({}, e.fn.royalSlider.defaults, n );
    this.isSlideshowRunning = false;
    this._dw = false;
    this._ev = this.slider.find( ".royalSlidesContainer" );
    this._fu = this._ev.wrap( '<div class="royalWrapper"/>' ).parent();
    this.slides = this._ev.find( ".royalSlide" );
    this._gt = "<p class='royalPreloader'></p>";
    this._hs = false;
    this._ir = false;
    if ( "ontouchstart" in window ) {
        if ( !this.settings.disableTranslate3d ) {
            if ( "WebKitCSSMatrix" in window && "m11" in new WebKitCSSMatrix ) {
                this._ev.css({
                    "-webkit-transform-origin": "0 0",
                    "-webkit-transform": "translateZ(0)"
                });
                this._ir = true
            }
        }
        this.hasTouch = true;
        this._az = "touchstart.rs";
        this._by = "touchmove.rs";
        this._cx = "touchend.rs"
    } else {
        this.hasTouch = false;
        if ( this.settings.dragUsingMouse ) {
            this._az = "mousedown.rs";
            this._by = "mousemove.rs";
            this._cx = "mouseup.rs"
        } else {
            this._ev.addClass( "auto-cursor" )
        }
    }
    if ( this.hasTouch ) {
        this.settings.directionNavAutoHide = false;
        this.settings.hideArrowOnLastSlide = true
    }
    if ( e.browser.msie && parseInt( e.browser.version, 10 ) <= 8 ) {
        this._jq = true
    } else {
        this._jq = false
    }
    this.slidesArr = [];
    var i, s, o, u;
    this.slides.each(function() {
        s = e( this );
        i = {};
        i.slide = s;
        if ( r.settings.blockLinksOnDrag ) {
            if ( !this.hasTouch ) {
                s.find( "a" ).bind( "click.rs", function( e ) {
                    if ( r._hs ) {
                        e.preventDefault();
                        return false
                    }
                })
            } else {
                var t = s.find( "a" );
                var n;
                t.each(function() {
                    n = e( this );
                    n.data( "royalhref", n.attr( "href" ) );
                    n.data( "royaltarget", n.attr( "target" ) );
                    n.attr( "href", "#" );
                    n.bind( "click", function( t ) {
                        t.preventDefault();
                        if ( r._hs ) {
                            return false
                        } else {
                            var n = e( this ).data( "royalhref" );
                            var i = e( this ).data( "royaltarget" );
                            if ( !i || i.toLowerCase() === "_kp" ) {
                                window.location.href = n
                            } else {
                                window.open( n )
                            }
                        }
                    })
                })
            }
        }
        if ( r.settings.nonDraggableClassEnabled ) {
            s.find( ".non-draggable" ).bind( r._az, function( e ) {
                r._hs = false;
                e.stopImmediatePropagation()
            })
        }
        o = s.attr( "data-src" );
        if ( o == undefined || o == "" || o == "none" ) {
            i.preload = false
        } else {
            i.preload = true;
            i.preloadURL = o
        }
        if ( r.settings.captionAnimationEnabled ) {
            i.caption = s.find( ".royalCaption" ).css( "display", "none" )
        }
        r.slidesArr.push( i )
    });
    this._lo = false;
    if ( this.settings.removeCaptionsOpacityInIE8 ) {
        if ( e.browser.msie && parseInt( e.browser.version, 10 ) <= 8 ) {
            this._lo = true
        }
    }
    if ( this.settings.autoScaleSlider ) {
        this.sliderScaleRatio = this.settings.autoScaleSliderHeight / this.settings.autoScaleSliderWidth
    }
    this.slider.css( "overflow", "visible" );
    this.slideWidth = 0;
    this.slideshowTimer = "";
    this.mn = false;
    this.numSlides = this.slides.length;
    this.currentSlideId = this.settings.startSlideIndex;
    this.lastSlideId = -1;
    this.isAnimating = true;
    this.wasSlideshowPlaying = false;
    this._az1 = 0;
    this._by1 = 0;
    this._cx1 = false;
    this._dw1 = [];
    this._ev1 = [];
    this._fu1 = false;
    this._gt1 = false;
    this._hs1 = 0;
    this._ir1 = 0;
    this._jq1 = 0;
    this._kp1 = 0;
    this._lo1 = 0;
    this._mn1 = 0;
    this._az2 = false;
    this._by2 = false;
    if ( this.settings.slideTransitionType === "fade" ) {
        if ( this._ir || "WebKitCSSMatrix" in window && "m11" in new WebKitCSSMatrix ) {
            this._cx2 = true
        } else {
            this._cx2 = false
        }
        this._dw2 = e( "<div class='fade-container'></div>" ).appendTo( this._fu )
    }
    if ( this.settings.slideshowEnabled && this.settings.slideshowDelay > 0 ) {
        if ( !this.hasTouch && this.settings.slideshowPauseOnHover ) {
            this.slider.hover(function() {
                r._by2 = true;
                r._ev2( true )
            }, function() {
                    r._by2 = false;
                    r._fu2( true )
                })
        }
        this.slideshowEnabled = true
    } else {
        this.slideshowEnabled = false
    }
    this._gt2();
    if ( this.settings.controlNavEnabled ) {
        var a;
        this._hs2Container = "";
        var f;
        if ( !r.settings.controlNavThumbs ) {
            this._hs2Container = e( '<div class="royalControlNavOverflow"><div class="royalControlNavContainer"><div class="royalControlNavCenterer"></div></div></div>' );
            a = this._hs2Container.find( ".royalControlNavCenterer" )
        } else {
            this.slider.addClass( "with-thumbs" );
            if ( r.settings.controlNavThumbsNavigation ) {
                f = e( '<div class="thumbsAndArrowsContainer"></div>' );
                this.thumbsArrowLeft = e( "<a href='#' class='thumbsArrow left'></a>" );
                this.thumbsArrowRight = e( "<a href='#' class='thumbsArrow right'></a>" );
                f.append( this.thumbsArrowLeft );
                f.append( this.thumbsArrowRight );
                var l = parseInt( this.thumbsArrowLeft.outerWidth(), 10 );
                this._hs2Container = e( '<div class="royalControlNavOverflow royalThumbs"><div class="royalControlNavThumbsContainer"></div></div>' );
                a = this._hs2Container.find( ".royalControlNavThumbsContainer" )
            } else {
                this._hs2Container = e( '<div class="royalControlNavOverflow royalThumbs"><div class="royalControlNavContainer"><div class="royalControlNavCenterer"></div></div></div>' );
                a = this._hs2Container.find( ".royalControlNavCenterer" )
            }
        }
        var c = 0;
        numero = 1;
        this.slides.each(function( t ) {
            if ( r.settings.controlNavThumbs ) {
                a.append( '<a href="#" class="royalThumb" style="background-image:url(' + e( this ).attr( "data-thumb" ) + ')">' + ( t + 1 ) + "</a>" )
            } else {
                a.append( '<a id="numero' + numero + '"  href="#">' + ( t + 1 ) + "</a>" )
            }
            c++;
            numero++
        });
        this.navItems = a.children();
        if ( f ) {
            f.append( this._hs2Container );
            this._fu.after( f )
        } else {
            this._fu.after( this._hs2Container )
        }
        if ( r.settings.controlNavThumbs && r.settings.controlNavThumbsNavigation ) {
            this._kp2 = true;
            this._lo2 = false;
            this._mn2 = a;
            if ( this._ir ) {
                this._mn2.css({
                    "-webkit-transition-duration": this.settings.controlNavThumbsSpeed + "ms",
                    "-webkit-transition-property": "-webkit-transform",
                    "-webkit-transition-timing-function": "ease-in-out"
                })
            }
            this._az3 = c;
            var h = this.navItems.eq( 0 );
            this._by3 = h.outerWidth( true );
            this._cx3 = this._by3 * this._az3;
            this._mn2.css( "width", this._cx3 );
            this._dw3 = parseInt( h.css( "marginRight" ), 10 );
            this._cx3 -= this._dw3;
            this._ev3 = 0;
            this._fu3();
            this.thumbsArrowLeft.click(function( e ) {
                e.preventDefault();
                if ( !r._kp2 ) {
                    r._gt3( r._ev3 + r._hs3 + r._dw3 )
                }
            });
            this.thumbsArrowRight.click(function( e ) {
                e.preventDefault();
                if ( !r._lo2 ) {
                    r._gt3( r._ev3 - r._hs3 - r._dw3 )
                }
            })
        }
        this._ir3()
    }
    if ( this.settings.directionNavEnabled ) {
        this._fu.after( "<a href='#' class='arrow left'/>" );
        this._fu.after( "<a href='#' class='arrow right'/>" );
        this.arrowLeft = this.slider.find( "a.arrow.left" );
        this.arrowRight = this.slider.find( "a.arrow.right" );
        if ( this.arrowLeft.length < 1 || this.arrowRight.length < 1 ) {
            this.settings.directionNavEnabled = false
        } else if ( this.settings.directionNavAutoHide ) {
            this.arrowLeft.hide();
            this.arrowRight.hide();
            this.slider.one( "mousemove.arrowshover", function() {
                r.arrowLeft.fadeIn( "fast" );
                r.arrowRight.fadeIn( "fast" )
            });
            this.slider.hover(function() {
                r.arrowLeft.fadeIn( "fast" );
                r.arrowRight.fadeIn( "fast" )
            }, function() {
                    r.arrowLeft.fadeOut( "fast" );
                    r.arrowRight.fadeOut( "fast" )
                })
        }
        this._jq3()
    }
    this.sliderWidth = 0;
    this.sliderHeight = 0;
    var p;
    this._kp3 = "onorientationchange" in window ? "orientationchange.royalslider" : "resize.royalslider";
    e( window ).bind( this._kp3, function() {
        if ( p ) {
            clearTimeout( p )
        }
        p = setTimeout(function() {
            r.updateSliderSize()
        }, 100 )
    });
    this.updateSliderSize();
    this.settings.beforeLoadStart.call( this );
    var d = this.slidesArr[ this.currentSlideId ];
    if ( this.currentSlideId != 0 ) {
        if ( !this._ir ) {
            this._ev.css({
                left: -this.currentSlideId * this.slideWidth
            })
        } else {
            this._ev.css({
                "-webkit-transition-duration": "0",
                "-webkit-transition-property": "none"
            });
            this._ev.css({
                "-webkit-transform": "translate3d(" + -this.currentSlideId * this.slideWidth + "px, 0, 0)"
            })
        }
    }
    if ( this.settings.welcomeScreenEnabled ) {
        function v( e ) {
            r.settings.loadingComplete.call( r );
            if ( e && r.settings.preloadNearbyImages ) {
                r._lo3( r.currentSlideId )
            }
            r.slider.find( ".royalLoadingScreen" ).fadeOut( r.settings.welcomeScreenShowSpeed );
            setTimeout(function() {
                r._mn3()
            }, r.settings.welcomeScreenShowSpeed + 100 )
        }
        if ( d.preload ) {
            this._lo3( this.currentSlideId, function() {
                v( false )
            })
        } else {
            u = d.slide.find( "img.royalImage" )[ 0 ];
            if ( u ) {
                if ( this._az4( u ) ) {
                    v( true );
                    e( u ).css( "opacity", 0 );
                    e( u ).animate({
                        opacity: 1
                    }, "fast" )
                } else {
                    e( u ).css( "opacity", 0 );
                    e( "<img />" ).load(function() {
                        v( true );
                        e( u ).animate({
                            opacity: 1
                        }, "fast" )
                    }).attr( "src", u.src )
                }
            } else {
                v( true )
            }
        }
    } else {
        if ( d.preload ) {
            this._by4( d, function() {
                r.settings.loadingComplete.call( r );
                if ( r.settings.preloadNearbyImages ) {
                    r._lo3( r.currentSlideId )
                }
            })
        } else {
            u = d.slide.find( "img.royalImage" )[ 0 ];
            if ( u ) {
                if ( this._az4( u ) ) {
                    e( u ).css( "opacity", 0 ).animate({
                        opacity: 1
                    }, "fast" )
                } else {
                    e( u ).css( "opacity", 0 );
                    e( "<img />" ).load(function() {
                        e( u ).animate({
                            opacity: 1
                        }, "fast" )
                    }).attr( "src", u.src )
                }
            }
            this.settings.loadingComplete.call( this )
        }
        setTimeout(function() {
            r._mn3()
        }, 100 )
    }
}
t.prototype = {
    goTo: function( e, t, n, r, i ) {
        if ( !this.isAnimating ) {
            this.isAnimating = true;
            var s = this;
            this.lastSlideId = this.currentSlideId;
            this.currentSlideId = e;
            this._gt1 = true;
            this._fu1 = true;
            if ( this.lastSlideId != e ) {
                this._ir3( n );
                this._lo3( e )
            }
            this._jq3();
            this.settings.beforeSlideChange.call( this );
            if ( this.slideshowEnabled && this.slideshowTimer ) {
                this.wasSlideshowPlaying = true;
                this._ev2()
            }
            var o = !t ? this.settings.slideTransitionSpeed : 0;
            if ( r || t || this.settings.slideTransitionType === "move" ) {
                var u;
                if ( i > 0 ) {
                    o = i
                } else {
                    u = this.settings.slideTransitionEasing
                }
                if ( !this._ir ) {
                    if ( parseInt( this._ev.css( "left" ), 10 ) !== -this.currentSlideId * this.slideWidth ) {
                        this._ev.animate({
                            left: -this.currentSlideId * this.slideWidth
                        }, o, i > 0 ? "easeOutSine" : this.settings.slideTransitionEasing, function() {
                                s._cx4()
                            })
                    } else {
                        this._cx4()
                    }
                } else {
                    if ( this._dw4() !== -this.currentSlideId * this.slideWidth ) {
                        this._ev.bind( "webkitTransitionEnd.rs", function( e ) {
                            if ( e.target == s._ev.get( 0 ) ) {
                                s._ev.unbind( "webkitTransitionEnd.rs" );
                                s._cx4()
                            }
                        });
                        this._ev.css({
                            "-webkit-transition-duration": o + "ms",
                            "-webkit-transition-property": "-webkit-transform",
                            "-webkit-transition-timing-function": i > 0 ? "ease-out" : "ease-in-out",
                            "-webkit-transform": "translate3d(" + -this.currentSlideId * this.slideWidth + "px, 0, 0)"
                        })
                    } else {
                        this._cx4()
                    }
                }
            } else {
                var a = this.slidesArr[ this.lastSlideId ].slide;
                var f = a.clone().appendTo( this._dw2 );
                if ( !this._cx2 ) {
                    this._ev.css({
                        left: -this.currentSlideId * this.slideWidth
                    });
                    f.animate({
                        opacity: 0
                    }, o, this.settings.slideTransitionEasing, function() {
                            f.remove();
                            s._cx4()
                        })
                } else {
                    if ( !this._ir ) {
                        this._ev.css({
                            left: -this.currentSlideId * this.slideWidth
                        })
                    } else {
                        this._ev.css({
                            "-webkit-transition-duration": "0",
                            "-webkit-transform": "translate3d(" + -this.currentSlideId * this.slideWidth + "px, 0, 0)",
                            opacity: "1"
                        })
                    }
                    setTimeout(function() {
                        f.bind( "webkitTransitionEnd.rs", function( e ) {
                            if ( e.target == f.get( 0 ) ) {
                                f.unbind( "webkitTransitionEnd.rs" );
                                f.remove();
                                s._cx4()
                            }
                        });
                        f.css({
                            "-webkit-transition-duration": o + "ms",
                            "-webkit-transition-property": "opacity",
                            "-webkit-transition-timing-function": "ease-in-out"
                        });
                        f.css( "opacity", 0 )
                    }, 100 )
                }
            }
        }
    },
    goToSilent: function( e ) {
        this.goTo( e, true )
    },
    prev: function() {
        if ( this.currentSlideId <= 0 ) {
            this.goTo( this.numSlides - 1 )
        } else {
            this._ev4()
        }
    },
    next: function() {
        if ( this.currentSlideId >= this.numSlides - 1 ) {
            this.goTo( 0 )
        } else {
            this._fu4()
        }
    },
    updateSliderSize: function() {
        var e = this;
        var t;
        var n;
        if ( this.settings.autoScaleSlider ) {
            t = this.slider.width();
            if ( t != this.sliderWidth ) {
                this.slider.css( "height", t * this.sliderScaleRatio )
            }
        }
        t = this.slider.width();
        n = this.slider.height();
        if ( t != this.sliderWidth || n != this.sliderHeight ) {
            this.sliderWidth = t;
            this.sliderHeight = n;
            this.slideWidth = this.sliderWidth + this.settings.slideSpacing;
            var r = this.slidesArr.length;
            var i, s;
            for ( var o = 0, u = r; o < u; ++o ) {
                i = this.slidesArr[ o ];
                s = i.slide.find( "img.royalImage" ).eq( 0 );
                if ( s && i.preload == false ) {
                    this._ir4( s, this.sliderWidth, this.sliderHeight )
                }
                if ( this.settings.slideSpacing > 0 && o < r - 1 ) {
                    i.slide.css( "cssText", "margin-right:" + this.settings.slideSpacing + "px !important;" )
                }
                i.slide.css({
                    height: e.sliderHeight,
                    width: e.sliderWidth
                })
            }
            if ( !this._ir ) {
                this._ev.css({
                    left: -this.currentSlideId * this.slideWidth,
                    width: this.slideWidth * this.numSlides
                })
            } else {
                if ( !this._gt1 ) {
                    this._ev.css({
                        "-webkit-transition-duration": "0",
                        "-webkit-transition-property": "none"
                    });
                    this._ev.css({
                        "-webkit-transform": "translate3d(" + -this.currentSlideId * this.slideWidth + "px, 0, 0)",
                        width: this.slideWidth * this.numSlides
                    })
                }
            }
            if ( this.settings.controlNavThumbs && this.settings.controlNavThumbsNavigation ) {
                this._fu3()
            }
        }
    },
    stopSlideshow: function() {
        this._ev2();
        this.slideshowEnabled = false;
        this.wasSlideshowPlaying = false
    },
    resumeSlideshow: function() {
        this.slideshowEnabled = true;
        if ( !this.wasSlideshowPlaying ) {
            this._fu2()
        }
    },
    destroy: function() {
        this._ev2();
        this._ev.unbind( this._az );
        e( document ).unbind( this._by ).unbind( this._cx );
        e( window ).unbind( this._kp3 );
        if ( this.settings.keyboardNavEnabled ) {
            e( document ).unbind( "keydown.rs" )
        }
        this.slider.remove();
        delete this.slider
    },
    _lo3: function( e, t ) {
        if ( this.settings.preloadNearbyImages ) {
            var n = this;
            this._by4( this.slidesArr[ e ], function() {
                if ( t ) {
                    t.call()
                }
                n._by4( n.slidesArr[ e + 1 ], function() {
                    n._by4( n.slidesArr[ e - 1 ] )
                })
            })
        } else {
            this._by4( this.slidesArr[ e ], t )
        }
    },
    _ir3: function( e ) {
        if ( this.settings.controlNavEnabled ) {
            this.navItems.eq( this.lastSlideId ).removeClass( "current" );
            this.navItems.eq( this.currentSlideId ).addClass( "current" );
            if ( this.settings.controlNavThumbs && this.settings.controlNavThumbsNavigation ) {
                var t = this.navItems.eq( this.currentSlideId ).position().left;
                var n = t - Math.abs( this._ev3 );
                if ( n > this._hs3 - this._by3 * 2 - 1 - this._dw3 ) {
                    if ( !e ) {
                        this._gt3( -t + this._by3 )
                    } else {
                        this._gt3( -t - this._by3 * 2 + this._hs3 + this._dw3 )
                    }
                } else if ( n < this._by3 * 2 - 1 ) {
                    if ( !e ) {
                        this._gt3( -t - this._by3 * 2 + this._hs3 + this._dw3 )
                    } else {
                        this._gt3( -t + this._by3 )
                    }
                }
            }
        }
    },
    _jq3: function() {
        if ( this.settings.directionNavEnabled ) {
            if ( this.settings.hideArrowOnLastSlide ) {
                if ( this.currentSlideId == 0 ) {
                    this._lo4 = true;
                    this.arrowLeft.addClass( "disabled" );
                    if ( this._mn4 ) {
                        this._mn4 = false;
                        this.arrowRight.removeClass( "disabled" )
                    }
                } else if ( this.currentSlideId == this.numSlides - 1 ) {
                    this._mn4 = true;
                    this.arrowRight.addClass( "disabled" );
                    if ( this._lo4 ) {
                        this._lo4 = false;
                        this.arrowLeft.removeClass( "disabled" )
                    }
                } else {
                    if ( this._lo4 ) {
                        this._lo4 = false;
                        this.arrowLeft.removeClass( "disabled" )
                    } else if ( this._mn4 ) {
                        this._mn4 = false;
                        this.arrowRight.removeClass( "disabled" )
                    }
                }
            }
        }
    },
    _fu2: function( e ) {
        if ( this.slideshowEnabled ) {
            var t = this;
            if ( !this.slideshowTimer ) {
                this.slideshowTimer = setInterval(function() {
                    t.next()
                }, this.settings.slideshowDelay )
            }
        }
    },
    _ev2: function( e ) {
        if ( this.slideshowTimer ) {
            clearInterval( this.slideshowTimer );
            this.slideshowTimer = ""
        }
    },
    _by4: function( t, n ) {
        if ( t ) {
            if ( t.preload ) {
                var r = this;
                var i = new Image;
                var s = e( i );
                s.css( "opacity", 0 );
                s.addClass( "royalImage" );
                t.slide.prepend( s );
                t.slide.prepend( this._gt );
                t.preload = false;
                s.load(function() {
                    r._ir4( s, r.sliderWidth, r.sliderHeight );
                    s.animate({
                        opacity: 1
                    }, 300, function() {
                            t.slide.find( ".royalPreloader" ).remove()
                        });
                    if ( n ) {
                        n.call()
                    }
                }).attr( "src", t.preloadURL )
            } else {
                if ( n ) {
                    n.call()
                }
            }
        } else {
            if ( n ) {
                n.call()
            }
        }
    },
    _fu3: function() {
        this._hs3 = parseInt( this._hs2Container.width(), 10 );
        this._az5 = -( this._cx3 - this._hs3 );
        if ( this._hs3 >= this._cx3 ) {
            this._lo2 = true;
            this._kp2 = true;
            this.thumbsArrowRight.addClass( "disabled" );
            this.thumbsArrowLeft.addClass( "disabled" );
            this._cx1 = true;
            this._by5( 0 )
        } else {
            this._cx1 = false;
            var e = this.navItems.eq( this.currentSlideId ).position().left;
            this._gt3( -e + this._by3 )
        }
    },
    _gt3: function( e ) {
        if ( !this._cx1 && e != this._ev3 ) {
            if ( e <= this._az5 ) {
                e = this._az5;
                this._kp2 = false;
                this._lo2 = true;
                this.thumbsArrowRight.addClass( "disabled" );
                this.thumbsArrowLeft.removeClass( "disabled" )
            } else if ( e >= 0 ) {
                e = 0;
                this._kp2 = true;
                this._lo2 = false;
                this.thumbsArrowLeft.addClass( "disabled" );
                this.thumbsArrowRight.removeClass( "disabled" )
            } else {
                if ( this._kp2 ) {
                    this._kp2 = false;
                    this.thumbsArrowLeft.removeClass( "disabled" )
                }
                if ( this._lo2 ) {
                    this._lo2 = false;
                    this.thumbsArrowRight.removeClass( "disabled" )
                }
            }
            this._by5( e );
            this._ev3 = e
        }
    },
    _by5: function( e ) {
        if ( !this._ir ) {
            this._mn2.animate({
                left: e
            }, this.settings.controlNavThumbsSpeed, this.settings.controlNavThumbsEasing )
        } else {
            this._mn2.css({
                "-webkit-transform": "translate3d(" + e + "px, 0, 0)"
            })
        }
    },
    _mn3: function() {
        var t = this;
        this.slider.find( ".royalLoadingScreen" ).remove();
        if ( this.settings.controlNavEnabled ) {
            this.navItems.bind( "click", function( e ) {
                e.preventDefault();
                if ( !t._fu1 ) {
                    t._cx5( e )
                }
            })
        }
        if ( this.settings.directionNavEnabled ) {
            this.arrowRight.click(function( e ) {
                e.preventDefault();
                if ( !t._mn4 && !t._fu1 ) {
                    t.next()
                }
            });
            this.arrowLeft.click(function( e ) {
                e.preventDefault();
                if ( !t._lo4 && !t._fu1 ) {
                    t.prev()
                }
            })
        }
        if ( this.settings.keyboardNavEnabled ) {
            e( document ).bind( "keydown.rs", function( e ) {
                if ( !t._fu1 ) {
                    if ( e.keyCode === 37 ) {
                        t.prev()
                    } else if ( e.keyCode === 39 ) {
                        t.next()
                    }
                }
            })
        }
        this.wasSlideshowPlaying = true;
        this._cx4();
        this._ev.bind( this._az, function( e ) {
            if ( !t._gt1 ) {
                t._dw5( e )
            } else if ( !t.hasTouch ) {
                e.preventDefault()
            }
        });
        if ( this.slideshowEnabled && !this.settings.slideshowAutoStart ) {
            this._ev2()
        }
        this.settings.allComplete.call( this )
    },
    _gt2: function() {
        this._ev.removeClass( "grabbing-cursor" );
        this._ev.addClass( "grab-cursor" )
    },
    _ev5: function() {
        this._ev.removeClass( "grab-cursor" );
        this._ev.addClass( "grabbing-cursor" )
    },
    _fu4: function( e, t ) {
        if ( this.currentSlideId < this.numSlides - 1 ) {
            this.goTo( this.currentSlideId + 1, false, false, e, t )
        } else {
            this.goTo( this.currentSlideId, false, false, e, t )
        }
    },
    _ev4: function( e, t ) {
        if ( this.currentSlideId > 0 ) {
            this.goTo( this.currentSlideId - 1, false, false, e, t )
        } else {
            this.goTo( this.currentSlideId, false, false, e, t )
        }
    },
    _cx5: function( t ) {
        this.goTo( e( t.currentTarget ).index(), false, true )
    },
    _dw4: function() {
        var e = window.getComputedStyle( this._ev.get( 0 ), null ).getPropertyValue( "-webkit-transform" );
        var t = e.replace( /^matrix\(/i, "" ).split( /, |\)$/g );
        return parseInt( t[ 4 ], 10 )
    },
    _dw5: function( t ) {
        if ( !this._az2 ) {
            var n;
            if ( this.hasTouch ) {
                this._fu5 = false;
                var r = t.originalEvent.touches;
                if ( r && r.length > 0 ) {
                    n = r[ 0 ]
                } else {
                    return false
                }
            } else {
                n = t;
                t.preventDefault()
            }
            if ( this.slideshowEnabled ) {
                if ( this.slideshowTimer ) {
                    this.wasSlideshowPlaying = true;
                    this._ev2()
                } else {
                    this.wasSlideshowPlaying = false
                }
            }
            this._ev5();
            this._az2 = true;
            var i = this;
            if ( this._ir ) {
                i._ev.css({
                    "-webkit-transition-duration": "0",
                    "-webkit-transition-property": "none"
                })
            }
            e( document ).bind( this._by, function( e ) {
                i._gt5( e )
            });
            e( document ).bind( this._cx, function( e ) {
                i._hs5( e )
            });
            if ( !this._ir ) {
                this._mn1 = this._jq1 = parseInt( this._ev.css( "left" ), 10 )
            } else {
                this._mn1 = this._jq1 = this._dw4()
            }
            this._hs = false;
            this._ir1 = this._jq1;
            this._hs1 = t.timeStamp || ( new Date ).getTime();
            this._kp1 = n.clientX;
            this._lo1 = n.clientY
        }
        return false
    },
    _gt5: function( e ) {
        var t;
        if ( this.hasTouch ) {
            if ( this._fu5 ) {
                return false
            }
            var n = e.originalEvent.touches;
            if ( n.length > 1 ) {
                return false
            }
            t = n[ 0 ];
            if ( Math.abs( t.clientY - this._lo1 ) > Math.abs( t.clientX - this._kp1 ) + 3 ) {
                if ( this.settings.lockAxis ) {
                    this._fu5 = true
                }
                return false
            }
            e.preventDefault()
        } else {
            t = e;
            e.preventDefault()
        }
        this._by1 = this._az1;
        var r = t.clientX - this._kp1;
        if ( this._by1 != r ) {
            this._az1 = r
        }
        if ( r != 0 ) {
            if ( this.currentSlideId == 0 ) {
                if ( r > 0 ) {
                    r = Math.sqrt( r ) * 5
                }
            } else if ( this.currentSlideId == this.numSlides - 1 ) {
                if ( r < 0 ) {
                    r = -Math.sqrt( -r ) * 5
                }
            }
            if ( !this._ir ) {
                this._ev.css( "left", this._jq1 + r )
            } else {
                this._ev.css({
                    "-webkit-transform": "translate3d(" + ( this._jq1 + r ) + "px, 0, 0)"
                })
            }
        }
        var i = e.timeStamp || ( new Date ).getTime();
        if ( i - this._hs1 > 350 ) {
            this._hs1 = i;
            this._ir1 = this._jq1 + r
        }
        return false
    },
    _hs5: function( t ) {
        if ( this._az2 ) {
            var n = this;
            this._az2 = false;
            this._gt2();
            if ( !this._ir ) {
                this.endPos = parseInt( this._ev.css( "left" ), 10 )
            } else {
                this.endPos = this._dw4()
            }
            this.isdrag = false;
            e( document ).unbind( this._by ).unbind( this._cx );
            if ( this.slideshowEnabled ) {
                if ( this.wasSlideshowPlaying ) {
                    if ( !this._by2 ) {
                        this._fu2()
                    }
                    this.wasSlideshowPlaying = false
                }
            }
            if ( this.endPos == this._mn1 ) {
                this._hs = false;
                return
            } else {
                this._hs = true
            }
            var r = this._ir1 - this.endPos;
            var i = Math.max( 40, ( t.timeStamp || ( new Date ).getTime() ) - this._hs1 );
            var s = Math.abs( r ) / i;
            var o = this.slideWidth - Math.abs( this._mn1 - this.endPos );
            var u = Math.max( o * 1.08 / s, 200 );
            u = Math.min( u, 600 );

            function a() {
                o = Math.abs( n._mn1 - n.endPos );
                u = Math.max( o * 1.08 / s, 200 );
                u = Math.min( u, 500 );
                n.goTo( n.currentSlideId, false, false, true, u )
            }
            if ( this._mn1 - this.settings.minSlideOffset > this.endPos ) {
                if ( this._by1 < this._az1 ) {
                    a();
                    return false
                }
                this._fu4( true, u )
            } else if ( this._mn1 + this.settings.minSlideOffset < this.endPos ) {
                if ( this._by1 > this._az1 ) {
                    a();
                    return false
                }
                this._ev4( true, u )
            } else {
                a()
            }
        }
        return false
    },
    _cx4: function() {
        var e = this;
        if ( this.slideshowEnabled ) {
            if ( this.wasSlideshowPlaying ) {
                if ( !this._by2 ) {
                    this._fu2()
                }
                this.wasSlideshowPlaying = false
            }
        }
        this._fu1 = false;
        this._gt1 = false;
        if ( this.settings.captionAnimationEnabled && this.lastSlideId != this.currentSlideId ) {
            if ( this.lastSlideId != -1 ) {
                this.slidesArr[ this.lastSlideId ].caption.css( "display", "none" )
            }
            e._ir5( e.currentSlideId )
        }
        this.isAnimating = false;
        this.settings.afterSlideChange.call( this )
    },
    _ir5: function( t ) {
        var n = this.slidesArr[ t ].caption;
        if ( n && n.length > 0 ) {
            n.css( "display", "block" );
            var r = this;
            var i, s, o, u, a, f, l, c, h, p, d, v, m;
            var g = n.children();
            if ( this._dw1.length > 0 ) {
                for ( var y = this._dw1.length - 1; y > -1; y-- ) {
                    clearTimeout( this._dw1.splice( y, 1 ) )
                }
            }
            if ( this._ev1.length > 0 ) {
                var b;
                for ( var w = this._ev1.length - 1; w > -1; w-- ) {
                    b = this._ev1[ w ];
                    if ( b ) {
                        if ( !this._ir ) {
                            if ( b.running ) {
                                b.captionItem.stop( true, true )
                            } else {
                                b.captionItem.css( b.css )
                            }
                        }
                    }
                    this._ev1.splice( w, 1 )
                }
            }
            for ( var E = 0; E < g.length; E++ ) {
                i = e( g[ E ] );
                a = {};
                s = false;
                o = false;
                f = "";
                if ( i.attr( "data-show-effect" ) == undefined ) {
                    l = this.settings.captionShowEffects
                } else {
                    l = i.attr( "data-show-effect" ).split( " " )
                }
                for ( var S = 0; S < l.length; S++ ) {
                    if ( s && o ) {
                        break
                    }
                    u = l[ S ].toLowerCase();
                    if ( !s && u == "fade" ) {
                        s = true;
                        a[ "opacity" ] = 1
                    } else if ( o ) {
                        break
                    } else if ( u == "movetop" ) {
                        f = "margin-top"
                    } else if ( u == "moveleft" ) {
                        f = "margin-left"
                    } else if ( u == "movebottom" ) {
                        f = "margin-bottom"
                    } else if ( u == "moveright" ) {
                        f = "margin-right"
                    }
                    if ( f != "" ) {
                        a[ "moveProp" ] = f;
                        if ( !r._ir ) {
                            a[ "moveStartPos" ] = parseInt( i.css( f ), 10 )
                        } else {
                            a[ "moveStartPos" ] = 0
                        }
                        o = true
                    }
                }
                h = parseInt( i.attr( "data-move-offset" ), 10 );
                if ( isNaN( h ) ) {
                    h = this.settings.captionMoveOffset
                }
                p = parseInt( i.attr( "data-delay" ), 10 );
                if ( isNaN( p ) ) {
                    p = r.settings.captionShowDelay * E
                }
                d = parseInt( i.attr( "data-speed" ), 10 );
                if ( isNaN( d ) ) {
                    d = r.settings.captionShowSpeed
                }
                v = i.attr( "data-easing" );
                if ( !v ) {
                    v = r.settings.captionShowEasing
                }
                c = {};
                if ( o ) {
                    m = a.moveProp;
                    if ( m == "margin-right" ) {
                        m = "margin-left";
                        c[ m ] = a.moveStartPos + h
                    } else if ( m == "margin-bottom" ) {
                        m = "margin-top";
                        c[ m ] = a.moveStartPos + h
                    } else {
                        c[ m ] = a.moveStartPos - h
                    }
                }
                if ( !r._lo && s ) {
                    i.css( "opacity", 0 )
                }
                if ( !r._ir ) {
                    i.css( "visibility", "hidden" );
                    i.css( c );
                    if ( o ) {
                        c[ m ] = a.moveStartPos
                    }
                    if ( !r._lo && s ) {
                        c.opacity = 1
                    }
                } else {
                    var x = {};
                    if ( o ) {
                        x[ "-webkit-transition-duration" ] = "0";
                        x[ "-webkit-transition-property" ] = "none";
                        x[ "-webkit-transform" ] = "translate3d(" + ( isNaN( c[ "margin-left" ] ) ? 0 : c[ "margin-left" ] + "px" ) + ", " + ( isNaN( c[ "margin-top" ] ) ? 0 : c[ "margin-top" ] + "px" ) + ",0)";
                        delete c[ "margin-left" ];
                        delete c[ "margin-top" ];
                        c[ "-webkit-transform" ] = "translate3d(0,0,0)"
                    }
                    c.visibility = "visible";
                    c.opacity = 1;
                    if ( !r._lo && s ) {
                        x[ "opacity" ] = 0
                    }
                    x[ "visibility" ] = "hidden";
                    i.css( x )
                }
                this._ev1.push({
                    captionItem: i,
                    css: c,
                    running: false
                });
                this._dw1.push( setTimeout( function( e, t, n, i, s, o, u ) {
                    return function() {
                        r._ev1[ s ].running = true;
                        if ( !r._ir ) {
                            e.css( "visibility", "visible" ).animate( t, n, i, function() {
                                if ( r._jq && o ) {
                                    e.get( 0 ).style.removeAttribute( "filter" )
                                }
                                delete r._ev1[ s ]
                            })
                        } else {
                            e.css({
                                "-webkit-transition-duration": n + "ms",
                                "-webkit-transition-property": "opacity" + ( u ? ", -webkit-transform" : "" ),
                                "-webkit-transition-timing-function": "ease-out"
                            });
                            e.css( t )
                        }
                    }
                }( i, c, d, v, E, s, o ), p ) )
            }
        }
    },
    _ir4: function( t, n, r ) {
        var i = this.settings.imageScaleMode;
        var s = this.settings.imageAlignCenter;
        if ( s || i == "fill" || i == "fit" ) {
            var o = false;

            function u() {
                var e, o, u, a, l;
                var c = new Image;
                c.onload = function() {
                    var c = this.width;
                    var p = this.height;
                    var v = parseInt( t.css( "borderWidth" ), 10 );
                    v = isNaN( v ) ? 0 : v;
                    if ( i == "fill" || i == "fit" ) {
                        e = n / c;
                        o = r / p;
                        if ( i == "fill" ) {
                            u = e > o ? e : o
                        } else if ( i == "fit" ) {
                            u = e < o ? e : o
                        } else {
                            u = 1
                        }
                        a = parseInt( c * u, 10 ) - v;
                        l = parseInt( p * u, 10 ) - v;
                        t.attr({
                            width: a,
                            height: l
                        }).css({
                            width: a,
                            height: l
                        })
                    } else {
                        a = c - v;
                        l = p - v;
                        t.attr( "width", a ).attr( "height", l )
                    }
                    if ( s ) {
                        t.css({
                            "margin-left": Math.floor( ( n - a ) / 2 ),
                            "margin-top": Math.floor( ( r - l ) / 2 )
                        })
                    }
                };
                c.src = t.attr( "src" )
            }
            t.removeAttr( "height" ).removeAttr( "width" );
            if ( !this._az4( t.get( 0 ) ) ) {
                e( "<img />" ).load(function() {
                    u()
                }).attr( "src", t.attr( "src" ) )
            } else {
                u()
            }
        }
    },
    _az4: function( e ) {
        if ( e ) {
            if ( !e.complete ) {
                return false
            }
            if ( typeof e.naturalWidth != "undefined" && e.naturalWidth == 0 ) {
                return false
            }
        } else {
            return false
        }
        return true
    }
};
e.fn.royalSlider = function( n ) {
    return this.each(function() {
        var r = new t( e( this ), n );
        e( this ).data( "royalSlider", r )
    })
};
e.fn.royalSlider.defaults = {
    lockAxis: true,
    preloadNearbyImages: true,
    imageScaleMode: "none",
    imageAlignCenter: false,
    keyboardNavEnabled: false,
    directionNavEnabled: true,
    directionNavAutoHide: false,
    hideArrowOnLastSlide: true,
    slideTransitionType: "move",
    slideTransitionSpeed: 400,
    slideTransitionEasing: "easeInOutSine",
    captionAnimationEnabled: true,
    captionShowEffects: [ "fade", "moveleft" ],
    captionMoveOffset: 20,
    captionShowSpeed: 400,
    captionShowEasing: "easeOutCubic",
    captionShowDelay: 200,
    controlNavEnabled: true,
    controlNavThumbs: false,
    controlNavThumbsNavigation: true,
    controlNavThumbsSpeed: 400,
    controlNavThumbsEasing: "easeInOutSine",
    slideshowEnabled: false,
    slideshowDelay: 5e3,
    slideshowPauseOnHover: true,
    slideshowAutoStart: true,
    welcomeScreenEnabled: false,
    welcomeScreenShowSpeed: 500,
    minSlideOffset: 20,
    disableTranslate3d: false,
    removeCaptionsOpacityInIE8: false,
    startSlideIndex: 0,
    slideSpacing: 0,
    blockLinksOnDrag: true,
    nonDraggableClassEnabled: true,
    dragUsingMouse: true,
    autoScaleSlider: false,
    autoScaleSliderWidth: 960,
    autoScaleSliderHeight: 400,
    beforeSlideChange: function() {},
    afterSlideChange: function() {},
    beforeLoadStart: function() {},
    loadingComplete: function() {},
    allComplete: function() {}
};
e.fn.royalSlider.settings = {}
})( jQuery )

$( document ).ready(function() {

    // Geral
    $( '.helperComplement' ).remove();
    // Verificando se o navegador é o firefox
    if ( $.browser.mozilla ) {
        $( 'body' ).addClass( 'firefox' );
    }
    // Verificando se o navegador é o IE
    if ( $.browser.msie ) {
        $( "body" ).addClass( "eie" );
    }
    if ( $.browser.msie && $.browser.version == "7.0" ) {
        $( "body" ).addClass( "ie7" );
    }
    if ( $.browser.msie && $.browser.version == "8.0" ) {
        $( "body" ).addClass( "ie8" );
    }
    if ( $.browser.msie && $.browser.version == "9.0" ) {
        $( "body" ).addClass( "ie9" );
    }
    if ( $.browser.msie && $.browser.version == "10.0" ) {
        $( "body" ).addClass( "ie10" );
    }

    $( '#footer_az .atendimento-area' ).click(function() {
        $( '.zopim' ).first().hide();
        $( '.zopim' ).last().show();
    });

    // Bread Crumb
    // Definindo variavel que sera adicionada no codigo
    var setinhaBread = '<span title="Divisao Seta" id="setinha_bread"></span>';
    // A todas as li´s adiciona o span
    $( '.bread-crumb li' ).each(function() {
        $( this ).append( setinhaBread );
    })

    //header flutuante
    $( window ).scroll(function() {
        posScroll = $( document ).scrollTop();
        if ( posScroll >= 140 )
            $( '.floatingTopBar' ).fadeIn( 300 );
        else
            $( '.floatingTopBar' ).fadeOut( 300 );
    });
    //oculta header flutuante
    $( '.floatingTopBar .ocultar' ).click(function() {
        $( '.floatingTopBar' ).animate({
            top: "-93px"
        }, 1000 );
        $( '.floatingTopBar .ocultar' ).hide();
        $( '.floatingTopBar .exibir' ).show();
    })
    //exibe header flutuante
    $( '.floatingTopBar .exibir' ).click(function() {
        $( '.floatingTopBar' ).animate({
            top: "0px"
        }, 800 );
        $( '.floatingTopBar .exibir' ).hide();
        $( '.floatingTopBar .ocultar' ).show();
    })


    // Home
    if ( $( 'body.home' ).length > 0 ) {

        $( '.lista-abas li' ).mouseenter(function() {
            if ( $( this ).attr( 'class' ).indexOf( 'active' ) == -1 ) {
                $( this ).parents( '.abas' ).find( '.lista-abas li' ).removeClass( "active" );
                $( this ).addClass( "active" );
                $( this ).parents( '.abas' ).find( '.content-abas > div' ).hide();
                $( this ).parents( '.abas' ).find( '.content-abas > div' ).eq( $( this ).index() ).fadeIn();
            } else {
                return false;
            }
        })

        $( '.abas' ).mouseleave(function( argument ) {
            $( this ).find( '.lista-abas li' ).removeClass( "active" );
            $( this ).find( '.content-abas > div' ).hide();
        });

        // Inicializando Banner
        $( '#banner-rotator' ).royalSlider({
            slideshowEnabled: true,
            slideshowDelay: 6000,
            slideTransitionSpeed: 1000,
            imageScaleMode: "fill",
            hideArrowOnLastSlide: true,
            slideSpacing: 20
        });

        // Inicializando Carrossel de Marcas
        $( "#carrossel_marcas" ).jcarousel({
            wrap: 'circular'
        });
        setInterval(function() {
            $( '#carrossel_marcas .jcarousel-next' ).trigger( 'click' );
        }, 5000 );

        // Funcionamento das Abas
        $( '.conteudo_abas div.conteudo' ).hide();
        // Evento de Click
        $( '#abas_home ul li' ).click(function() {
            // Removendo configuracoes defaults
            $( '#abas_home ul li' ).removeClass( 'active' );
            $( '.conteudo_abas div.conteudo' ).hide();
            // Adicionando conteudo ativo
            var classe = $( this ).attr( 'class' );
            var seletor = '.conteudo_abas .' + classe;
            $( this ).addClass( 'active' );
            $( seletor ).fadeIn( 'fast' );
            return false;
        });

        // Definindo Feminino como padrao
        $( '#abas_home ul li.feminino' ).addClass( 'active' );
        $( '.conteudo_abas div.feminino' ).show( 'fast' );


    }

    if ( $( 'body.produto' ).length > 0 ) {
        $( 'body#produto.moreSku #sku_produto .skuList .portal-notify-me-ref .notifyme' ).each(function() {
            if ( $( this ).css( 'display' ) == 'block' ) {
                $( this ).hide();
                $( this ).parent().prepend( '<div class="btnAvise">Avise-me</div>' );
                $( this ).prepend( '<div class="close">x</div>' );
            }
        });
        $( 'body#produto.moreSku #sku_produto .skuList .portal-notify-me-ref .btnAvise' ).live( 'click', function() {
            $( 'body#produto.moreSku #sku_produto .skuList .portal-notify-me-ref .notifyme' ).fadeOut();
            $( this ).next().fadeIn();
        });
        $( 'body#produto.moreSku #sku_produto .skuList .portal-notify-me-ref .notifyme .close' ).live( 'click', function() {
            $( this ).parent().fadeOut();
        });

        // Produto Sku
        var profiteFixSku = ProfiteFixSku( "Tamanho", true );

        // Trocando nome do avalie
        var textoCorretoAvalie = 'Já avaliou este produto?';
        $( '#botoes_compartilhar_indique_avaliar #span_avaliacao strong' ).html( textoCorretoAvalie );



        /*$(document).ajaxStop(function(){
            if($('a.a_video_car').length == 0){
            // Add Classe aos spans da resenha
                $('.voteRatingBar').next('span').addClass('span_voto');

                // Adicionando video aos thumbs e criando evento de clique para que ele apareça no lugar da imagem do produto
                var liVideo = '<li><a class="a_video_car" href="#"><img src="/arquivos/video_produto.png"></a></li>';
                $('.produto ul.thumbs').append(liVideo);

                var varVideo = $('.produto .value-field').html();
                $('.produto #video_carrossel').html(varVideo);

                var open = false;
                $('a.a_video_car').click(function(){
                    if (open == true){
                        $('.produto #video_carrossel').fadeOut();
                        $('.produto #video_carrossel').html(varVideo);
                        open = false;
                    }
                    else if(open == false){
                        $('.produto #video_carrossel').fadeIn();
                        open = true;
                    }
                    return false;
                });
            }
        });*/

        if ( $( '.comprarFlutuante' ).find( 'a' ).length == 0 ) {
            $( '.comprarFlutuante' ).html( '<div class="escolher">Escolher produto</div>' );
        }
        $( '.conversorFlutuante .conteudoFlutuante .escolher' ).live( 'click', function() {
            $( "html, body" ).animate({
                scrollTop: 200
            }, "slow" );
        });

    }

    if ( $( 'body.productQuickView' ).length > 0 ) {

        // Produto Sku
        var profiteFixSku = ProfiteFixSku( "Tamanho", true );

    }

    if ( $( 'body.categoria' ).length > 0 ) {

        // Classes para auxilixar o CSS
        contadorSub = 1;
        $( '.sub' ).each(function() {
            $( this ).addClass( 'sub' + contadorSub );
            contadorSub++;
        });

        contadorResults = 1;
        $( '.searchResultsTime' ).each(function() {
            $( this ).addClass( 'p' + contadorResults );
            contadorResults++;
        });


        // Retirando liÃ‚Â´s vazias
        $( '.helperComplement' ).each(function() {
            $( this ).remove();
        });

        // Adicionando Carrossel

        if ( $( 'body#categoria' ).length > 0 ) {
            // Carrossel
            $( ".carrossel" ).jcarousel({
                scroll: 1
            });
        }



        // Adicionando classes a paginacao e em divs, para diferenciar a paginação do topo e a do footer
        var contli = 0;
        $( 'body.categoria #conteudo_produtos div.main .sub' ).each(function() {
            contli++;
            var classeLi = 'sub' + contli;
            $( this ).addClass( classeLi );
        });
        var contp = 0;
        $( 'body.categoria #conteudo_produtos div.main p.searchResultsTime' ).each(function() {
            contp++;
            var classeP = 'p' + contp;
            $( this ).addClass( classeP );
        });

    }

    if ( $( 'body.resultado-busca' ).length > 0 ) {
        if ( $( '.busca-vazio' ).length > 0 ) {

            var termo = window.location.pathname;
            termo = termo.replace( '/', '' );
            termo = '<a title="' + termo + '" href="/' + termo + '">' + termo + '</a>';
            $( '.bread-crumb ul li' ).append( termo );

            var conteudoBuscaVazia = '<p>Sua busca não encontrou nenhum resultado. Mas, temos diversos produtos que podem ser do seu interesse. Confira abaixo.</p>';
            $( '.busca-vazio' ).html( conteudoBuscaVazia );
            $( '.prateleiras_resultado_de_busca_nao_encontrado, .side-nao-encontrada' ).fadeIn();
            $( '#sidebar_navigation .navigation' ).fadeOut();
        }
    }

    if ( $( 'body.central' ).length > 0 ) {
        $( '.central_interna a.abre-chat' ).click(function() {
            $( '.zopim' ).first().hide();
            $( '.zopim' ).last().show();
        });
    }

    if ( $( 'body.account' ).length > 0 ) {

        $( document ).ajaxStop(function() {
            var statusNew = false;
            $( '.new-address-link a.address-update' ).click(function() {
                if ( statusNew == false ) {
                    $( '.modal #address-edit' ).fadeIn( 'fast' );
                    statusNew = true;
                } else if ( statusNew == true ) {
                    $( '.modal #address-edit' ).fadeOut( 'fast' );
                    statusNew = false;
                }
                return false;
            });
        });

        //Legado
        // Minha Conta - Modais
        var statusAlterarPerfil = false;
        $( 'body.account #content_abas a#edit-data-link' ).click(function() {
            if ( statusAlterarPerfil == false ) {
                $( '#editar-perfil' ).fadeIn( 'fast' );
                statusAlterarPerfil = true;
            } else if ( statusAlterarPerfil == true ) {
                $( '#editar-perfil' ).fadeOut( 'fast' );
                statusAlterarPerfil = false;
            }
        });
        $( '#editar-perfil .close' ).click(function() {
            $( '#editar-perfil' ).fadeOut( 'fast' );
            statusAlterarPerfil = false;
        });

        var statusEndRemove = false;
        $( 'body.account p.edit-address-link a.delete' ).click(function() {
            if ( statusEndRemove == false ) {
                $( '#address-remove' ).fadeIn( 'fast' );
                statusEndRemove = true;
            } else if ( statusEndRemove == true ) {
                $( '#address-remove' ).fadeOut( 'fast' );
                statusEndRemove = false;
            }
        });
        $( '#address-remove .close' ).click(function() {
            $( '#address-remove' ).fadeOut( 'fast' );
            statusEndRemove = false;
        });

        var statusEndUpdate = false;
        $( 'body.account p.new-address-link a.address-update' ).click(function() {
            if ( statusEndUpdate == false ) {
                $( '#address-edit' ).fadeIn( 'fast' );
                statusEndUpdate = true;
            } else if ( statusEndUpdate == true ) {
                $( '#address-edit' ).fadeOut( 'fast' );
                statusEndUpdate = false;
            }
        });
        $( '#address-edit .close' ).click(function() {
            $( '#address-edit' ).fadeOut( 'fast' );
            statusEndUpdate = false;
        });
        //fim legado

        // var statusEdit = false;
        // $('.edit-profile-link a#edit-data-link').click(function(){
        //  if (statusEdit == false) {
        //      $('.modal #address-edit').fadeIn('fast');
        //      statusEdit = true;
        //  }
        //  else if(statusEdit == true){
        //      $('.modal #address-edit').fadeOut('fast');
        //      statusEdit = false;
        //  }
        // });
    }
    

    // REVELAR E ESCONDER O CONVERSOR FLUTUANTE NA PAGINA DE PRODUTO
    $( window ).scroll(function() {
        posScroll = $( document ).scrollTop();
        if ( posScroll >= 625 )
            $( '.conversorFlutuante' ).fadeIn( 300 );
        else
            $( '.conversorFlutuante' ).fadeOut( 300 );
    });


    // VER COMPLETO NO QUICK VIEW
    $( 'body.productQuickView .productName' ).append( '<a href="" target="_parent" class="visCompleta">Ver visualização completa</a>' );
    var urlquickview = location.search.split( "/" );
    $( '.visCompleta' ).attr( 'href', '/produto-completo/' + urlquickview + '' );



    // ESCONDER ITENS DA SIDEBAR
    $( '.categoria #sidebar_navigation h5' ).each(function() {
        var quantLabel = $( this ).next( 'ul' ).children( 'li' ).length;
        //console.log(quantLabel);
        if ( quantLabel > 6 ) {
            $( this ).append( '<span class="mais">+</span>' );
            $( this ).next( 'ul' ).css( 'height', '160px' ).css( 'overflow', 'hidden' );
        } else {
            $( this ).next( 'ul' ).css( 'height', 'auto' );
        }
    });

    $( '.categoria #sidebar_navigation h5' ).click(function() {
        var valorSpan = $( this ).find( 'span' ).html();
        if ( valorSpan === '+' ) {
            $( this ).next( 'ul' ).css( 'height', 'auto' );
            $( this ).find( 'span' ).removeClass().addClass( 'menos' ).html( '-' );
        } else {
            $( this ).next().css( 'height', '160px' );
            $( this ).find( 'span' ).removeClass().addClass( 'mais' ).html( '+' );
        }
    });

    if ( $( '#produto.moreSku #info_produto #info_sku #sku_produto .skuList' ).length > 0 ) {
        $( '#produto.moreSku #info_produto #info_sku #info_gray' ).hide();
    }

});

$( window ).load(function() {
    // AJUSTES NO CONVERSOR DA PAGINA DE PRODUTO
    if ( $( 'body' ).hasClass( 'productQuickView' ) ) {
        var pegaValorDivido = $( 'body.productQuickView .valor-dividido.price-installments > span, body.produto .comprarFlutuante .valor-dividido.price-installments > span' ).html().replace( 'ou', '<span class="ouAte">ou até </span>' );
        $( 'body.productQuickView .valor-dividido.price-installments span, body.produto .comprarFlutuante .valor-dividido.price-installments span' ).html( pegaValorDivido );
    }
});

$( 'document' ).ready(function() {

    $( '.bread-crumb ul li' ).eq( 0 ).find( 'a' ).html( 'Home' );

    $( '.menu-departamento h4 a, .menu-departamento ul li a' ).each(function() {
        var qtd = '';
        var nome = '';
        qtd = $( this ).html();
        if ( /\(/.test( qtd ) ) {
            qtd = qtd.split( '(' );
            nome = qtd[ 0 ];
            qtd = qtd[ 1 ];
            qtd = qtd.split( ')' );
            qtd = qtd[ 0 ];
            $( this ).html( nome );
        }
    });

    $( 'body#departamento #sidebar_navigation h5' ).each(function() {
        if ( $( this ).text() == 'Marca+' ) {
            $( this ).next( 'ul' ).css( 'overflow', 'auto' );
        }
    });

    $( document ).ajaxStart(function() {

        $( '.v2-vtexsc-cart.vtexsc-cart.mouseActivated.preLoaded' ).hide();

    });

    $( document ).ajaxStop(function() {

        $( '.v2-vtexsc-cart.vtexsc-cart.mouseActivated.preLoaded' ).hide();

    });

    /*function  alteraWelcome() {

        var welcome = $('.welcome').text(); //    Ola nome-do-usuario /
        var usuario = welcome.split('Olá')[1]; //   nome-do-usuario


        //Reconstroi a estrutura
        $('.welcome').html( 'Olá, ' + '<span>' + usuario + '</span>'  + '<a href="/no-cache/user/logout">Não é você?</a>' );

        //estiliza os elementos
        $('.welcome span').css('font-weight', 'bold');
        $('.welcome a').css({'color': 'gray', 'font-style': 'normal'})

    }

    alteraWelcome();*/

    if ( $( 'body' ).hasClass( 'home' ) ) {

        $( '.conteudo_abas .prateleira' ).each(function() {
            if ( $( this ).find( 'li' ).length >= 4 ) {
                $( this ).jcarousel({
                    wrap: 'circular',
                    scroll: 1,
                });
            }
        });
    }
});

$( document ).ajaxStop(function() {

    if ($( '.welcome' ).find( '#login' ).length === 0) {
        $( '.welcome' ).find( 'a' ).html( ' / Sair' ).show();
    }

    if ( $( 'body' ).hasClass( 'thumb-color' ) ) {
        if ( $( '#sku_produto ul li label' ).eq( 0 ).hasClass( 'item_unavailable' ) ) {
            $( '.botao, #qtd_produto' ).hide();
        }
    } else {
        if ( $( '.notifyme' ).css( 'display' ) == 'block' ) {
            $( '#info_gray>div' ).show();
            $( '#sku_preco, #botao_comprar, #qtd_produto' ).hide();
        }
    }
});
//Ajustes QA 18-08-2014 Up Menu
$( document ).ready(function() {
    if ( $( 'body.departamento' ).length > 0 ) {

        if ( $( '.bread-crumb li' ).eq( 1 ).find( 'a' ).html() === "Beleza" ) {
            $( ".even:last-child" ).prependTo( ".menu-departamento" );
            $( "h5" ).prependTo( ".menu-departamento" );
        }
    }
    if ( $( 'body.categoria' ).length > 0 ) {

        if ( $( '.bread-crumb li' ).eq( 1 ).find( 'a' ).html() === "Beleza" ) {
            $( '.Hide.HideMarca + ul' ).prependTo( ".menu-departamento" );
            $( '.Hide.HideMarca' ).prependTo( ".menu-departamento" );
        }
    }
    if($('body.produto').length > 0) {
        if($('#product-gift-wrapper ul li').length > 0){
            $('#product-gift-wrapper').show();
        }
        else{
            $('#product-gift-wrapper').addClass('gift-no-product');   
            $('#product-gift-wrapper').hide();   
        }
    }
    //sidebar scroll marca
    if($('#sidebar_navigation h5').length > 0){
        $('#sidebar_navigation h5').each(function() {
            if($(this).text() === 'Marcas' || $(this).text() === 'Marca'){
                $(this).next('ul').addClass('marca-scroll');
                return true;
            }
        });
    } 
    if($('#sidebar_navigation .side-nao-encontrada h3').length > 0){
        $('#sidebar_navigation .side-nao-encontrada h3').each(function() {
            if($(this).text() === 'Marcas' || $(this).text() === 'Marca'){
                $(this).next('ul').addClass('marca-scroll');
                return true;
            }
        });
    }

    $(document).ajaxComplete(function() {
        $(".vitrine li").each(function() {
            if ($(this).find(".flags p.brinde-de-produto").length > 0) {
                if (!$(this).hasClass('brinde')) {
                    $(this).addClass("brinde");
                    $(this).find(".price").hide();
                    $(this).find(".buy-button-normal").hide();
                }
            }
        })
    });
});

