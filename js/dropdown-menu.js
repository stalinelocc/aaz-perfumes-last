$.get('https://www.aazperfumes.com.br/no-cache/profileSystem/getProfile').then(function(response){
	if(response.IsUserDefined == false){
		$('li.col-left.box.desk.user').append('<div class="conteudo-drop-az-login" style="display:none;position: absolute;right:calc(0px - 40px);background: #fff;width: 100%;text-align: center;padding: 15px 12px 0px 12px;min-width: 180px;box-shadow: 0px 2px 3px #00000069;z-index: 9;"><h3><span>Você ainda não esta logado</span></h3><nav class="login-acesso"><ul style="padding: 16px 0px;"><li style="padding-bottom: 5px;text-transform: uppercase;"><a style="color: #555;" href="/login">Entrar</a></li><li style="padding-bottom: 5px;text-transform: uppercase;"><a style="color: #555;" href="/account">Minha conta</a></li><li style="padding-bottom: 5px;text-transform: uppercase;"><a style="color: #555;" href="/account/orders">Meus Pedidos</a></li></ul></nav></div>');
	}else{
        var email = response.Email;
        $('li.col-left.box.desk.user').append('<div class="conteudo-drop-az-login" style="display:none;position: absolute;right:calc(0px - 40px);background: #fff;width: 100%;text-align: center;padding: 15px 12px 0px 12px;min-width: 180px;box-shadow: 0px 2px 3px #00000069;z-index: 9;"><h3>Olá, <span>'+email+'</span></h3><nav class="login-acesso"><ul style="padding: 16px 0px;"><li style="padding-bottom: 5px;text-transform: uppercase;"><a style="color: #555;" href="/logout">Sair</a></li><li style="padding-bottom: 5px;text-transform: uppercase;"><a style="color: #555;" href="/account">Minha conta</a></li><li style="padding-bottom: 5px;text-transform: uppercase;"><a style="color: #555;" href="/account/orders">Meus Pedidos</a></li></ul></nav></div>');
	}
}).fail(function(error){
	console.log(error);
});