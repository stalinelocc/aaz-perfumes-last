//VARIAVEIS DE  INTERAÃ‡ÃƒO
var indice = 1;
var a = 1;
var b = 2;
var c = 3;
var pontos = 0;
var sexo = "";


//FAZ ANIMAÃ‡ÃƒO DE INÃCIO
var apresentacao = function(){
    setTimeout(function(){
       $('.iniciar').addClass('active');
    },1000);
};

//APARECE APRESENTAÃ‡ÃƒO
var showApres = function(){
	$('.quiz').fadeOut();
	$('.abertura, .iniciar').addClass('active');
	$('body').removeClass('bg').addClass('bg');
};

//DESAPARECE APRESENTAÃ‡ÃƒO
var hideApres = function(){
	$('.abertura, .iniciar').removeClass('active');
	$('body').removeClass('bg');
	$('.quiz').fadeIn();
};

//BOTÃƒO INICIAR
var iniciar = function(){
	$('.iniciar').click(function(){
		hideApres();
	});
};

//PROXIMA PERGUNTA
var nextQuest = function(){
	$('.box-resp').hide();
	if(indice == 1){
		$('.box-resp[idx="idx-1"]').fadeIn();
	}else{
		if(sexo == "m"){
			//condicoes de proximo e resultado para homem
			if(indice > 1 && indice < 10){
				$('.box-resp[idx="idx-'+indice+'"]').fadeIn();
			};
			if(indice > 1 && indice > 9){
				if(pontos < 7){
					$('.resultado-fem.res1').fadeIn();
					$('.questionario').hide();
					$('.quiz').addClass('new-bg');
				};
				if(pontos > 6 && pontos < 11){
					$('.resultado-fem.res2').fadeIn();
					$('.questionario').hide();
					$('.quiz').addClass('new-bg');
				};
				if(pontos > 10){
					$('.resultado-fem.res3').fadeIn();
					$('.questionario').hide();
					$('.quiz').addClass('new-bg');
				};
			};

		};
	
		if(sexo == "f"){
			//condicoes de proximo e resultado para mulher
			if(indice > 1 && indice < 6){
				$('.box-resp[idx="idx-'+indice+'"]').fadeIn();
			};
			if(indice > 1 && indice > 5){
				if(pontos < 7){
					$('.resultado-masc.res1').fadeIn();
					$('.questionario').hide();
					$('.quiz').addClass('new-bg');
				};
				if(pontos > 6 && pontos < 11){
					$('.resultado-masc.res2').fadeIn();
					$('.questionario').hide();
					$('.quiz').addClass('new-bg');
				};
				if(pontos > 10){
					$('.resultado-masc.res3').fadeIn();
					$('.questionario').hide();
					$('.quiz').addClass('new-bg');
				};

				
			};
		};
	};
};

//INTERAÃ‡ÃƒO DE PERGUNTAS
var perguntas = function(){
	nextQuest();
	$('.box-resp ul li').click(function(){
		var classe = $(this).attr('class');
		if(classe == "mascu"){
			indice = 5;
			sexo = "m";
		};
		if(classe == "femi"){
			indice = 1;
			sexo = "f";
		};
		if(classe == "a"){
			pontos = pontos+a;
			$(this).parents('.box-resp').find('.oculto').html(a);
		};
		if(classe == "b"){
			pontos = pontos+b;
			$(this).parents('.box-resp').find('.oculto').html(b);
		};
		if(classe == "c"){
			pontos = pontos+c;
			$(this).parents('.box-resp').find('.oculto').html(c);
		};
		indice++;
		nextQuest();
	});
};

//VOLTAR
var voltar = function(){
	$('.back').click(function(){

		var anterior = $('.box-resp[idx="idx-'+(indice-1)+'"]');
		$('.box-resp').hide();

		if(indice == 6 && sexo == "m"){
			indice = 1;
			pontos = 0;
			$('.box-resp[idx="idx-1"]').fadeIn();
		}else{
			if(indice == 2){
				indice = 1;
				pontos = 0;
				$('.box-resp[idx="idx-1"]').fadeIn();
			}else{
				var PontoAnterior = anterior.find('.oculto').html();
				pontos = (pontos-PontoAnterior);		
				anterior.fadeIn();
				indice = indice-1;
			};
		};
	});
};

$(document).ready(function(){
	var end = window.location.href;
	$('.fb-like').attr('data-href',end);

	console.log('testessss');
});

$(window).load(function(){
	apresentacao();
	iniciar();
	perguntas();
	voltar();
});