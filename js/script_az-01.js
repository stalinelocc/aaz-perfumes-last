// Documento JS / jQuery
// Cliente: AZ PERFUMES
// Autor: Profite
// Ultima data de alteraçãoo: 20/05/2015 - (One Sky)

var gt_login = function(gRedirect,gDiv) {
    var gPage = location.href;
    if ($('p.welcome a#login').length) {
        
        if (gDiv == '.l-login') { gRedirect = gPage; }
        $('body').on('click', gDiv, function () {           
            vtexid.start({
                returnUrl: gRedirect,
                userEmail: '',
                locale: 'pt-BR',
                forceReload: true
            });
            $('html body').animate({scrollTop: 0}, 1000);
        });
    } else {
        $('.open-login').removeClass('l-login');
        $('.l-conta').attr("href","/account");
        $('.l-pedidos').attr("href","/account/orders");
    }
};

var abrePopupNewz = function abrePopup(el){
    $('.news-cookie.'+el+', .bgOverlay').fadeIn(600);
    $('.news-cookie.'+el+' .fechar').click(function(){
        $('.news-cookie.'+el+', .bgOverlay').fadeOut(600);       
    });
};



// Banner
jQuery.easing.jswing = jQuery.easing.swing;
jQuery.extend( jQuery.easing, {
    def: "easeOutQuad",
    swing: function( e, t, n, r, i ) {
        return jQuery.easing[ jQuery.easing.def ]( e, t, n, r, i )
    },
    easeInQuad: function( e, t, n, r, i ) {
        return r * ( t /= i ) * t + n
    },
    easeOutQuad: function( e, t, n, r, i ) {
        return -r * ( t /= i ) * ( t - 2 ) + n
    },
    easeInOutQuad: function( e, t, n, r, i ) {
        if ( ( t /= i / 2 ) < 1 ) return r / 2 * t * t + n;
        return -r / 2 * ( --t * ( t - 2 ) - 1 ) + n
    },
    easeInCubic: function( e, t, n, r, i ) {
        return r * ( t /= i ) * t * t + n
    },
    easeOutCubic: function( e, t, n, r, i ) {
        return r * ( ( t = t / i - 1 ) * t * t + 1 ) + n
    },
    easeInOutCubic: function( e, t, n, r, i ) {
        if ( ( t /= i / 2 ) < 1 ) return r / 2 * t * t * t + n;
        return r / 2 * ( ( t -= 2 ) * t * t + 2 ) + n
    },
    easeInQuart: function( e, t, n, r, i ) {
        return r * ( t /= i ) * t * t * t + n
    },
    easeOutQuart: function( e, t, n, r, i ) {
        return -r * ( ( t = t / i - 1 ) * t * t * t - 1 ) + n
    },
    easeInOutQuart: function( e, t, n, r, i ) {
        if ( ( t /= i / 2 ) < 1 ) return r / 2 * t * t * t * t + n;
        return -r / 2 * ( ( t -= 2 ) * t * t * t - 2 ) + n
    },
    easeInQuint: function( e, t, n, r, i ) {
        return r * ( t /= i ) * t * t * t * t + n
    },
    easeOutQuint: function( e, t, n, r, i ) {
        return r * ( ( t = t / i - 1 ) * t * t * t * t + 1 ) + n
    },
    easeInOutQuint: function( e, t, n, r, i ) {
        if ( ( t /= i / 2 ) < 1 ) return r / 2 * t * t * t * t * t + n;
        return r / 2 * ( ( t -= 2 ) * t * t * t * t + 2 ) + n
    },
    easeInSine: function( e, t, n, r, i ) {
        return -r * Math.cos( t / i * ( Math.PI / 2 ) ) + r + n
    },
    easeOutSine: function( e, t, n, r, i ) {
        return r * Math.sin( t / i * ( Math.PI / 2 ) ) + n
    },
    easeInOutSine: function( e, t, n, r, i ) {
        return -r / 2 * ( Math.cos( Math.PI * t / i ) - 1 ) + n
    },
    easeInExpo: function( e, t, n, r, i ) {
        return t == 0 ? n : r * Math.pow( 2, 10 * ( t / i - 1 ) ) + n
    },
    easeOutExpo: function( e, t, n, r, i ) {
        return t == i ? n + r : r * ( -Math.pow( 2, -10 * t / i ) + 1 ) + n
    },
    easeInOutExpo: function( e, t, n, r, i ) {
        if ( t == 0 ) return n;
        if ( t == i ) return n + r;
        if ( ( t /= i / 2 ) < 1 ) return r / 2 * Math.pow( 2, 10 * ( t - 1 ) ) + n;
        return r / 2 * ( -Math.pow( 2, -10 * --t ) + 2 ) + n
    },
    easeInCirc: function( e, t, n, r, i ) {
        return -r * ( Math.sqrt( 1 - ( t /= i ) * t ) - 1 ) + n
    },
    easeOutCirc: function( e, t, n, r, i ) {
        return r * Math.sqrt( 1 - ( t = t / i - 1 ) * t ) + n
    },
    easeInOutCirc: function( e, t, n, r, i ) {
        if ( ( t /= i / 2 ) < 1 ) return -r / 2 * ( Math.sqrt( 1 - t * t ) - 1 ) + n;
        return r / 2 * ( Math.sqrt( 1 - ( t -= 2 ) * t ) + 1 ) + n
    },
    easeInElastic: function( e, t, n, r, i ) {
        e = 1.70158;
        var s = 0,
            o = r;
        if ( t == 0 ) return n;
        if ( ( t /= i ) == 1 ) return n + r;
        s || ( s = i * .3 );
        if ( o < Math.abs( r ) ) {
            o = r;
            e = s / 4
        } else
            e = s / ( 2 * Math.PI ) * Math.asin( r / o );
        return -( o * Math.pow( 2, 10 * ( t -= 1 ) ) * Math.sin( ( t * i - e ) * 2 * Math.PI / s ) ) + n
    },
    easeOutElastic: function( e, t, n, r, i ) {
        e = 1.70158;
        var s = 0,
            o = r;
        if ( t == 0 ) return n;
        if ( ( t /= i ) == 1 ) return n + r;
        s || ( s = i * .3 );
        if ( o < Math.abs( r ) ) {
            o = r;
            e = s / 4
        } else
            e = s / ( 2 * Math.PI ) * Math.asin( r / o );
        return o * Math.pow( 2, -10 * t ) * Math.sin( ( t * i - e ) * 2 * Math.PI / s ) + r + n
    },
    easeInOutElastic: function( e, t, n, r, i ) {
        e = 1.70158;
        var s = 0,
            o = r;
        if ( t == 0 ) return n;
        if ( ( t /= i / 2 ) == 2 ) return n + r;
        s || ( s = i * .3 * 1.5 );
        if ( o < Math.abs( r ) ) {
            o = r;
            e = s / 4
        } else
            e = s / ( 2 * Math.PI ) * Math.asin( r / o );
        if ( t < 1 ) return -.5 * o * Math.pow( 2, 10 * ( t -= 1 ) ) * Math.sin( ( t * i - e ) * 2 * Math.PI / s ) + n;
        return o * Math.pow( 2, -10 * ( t -= 1 ) ) * Math.sin( ( t * i - e ) * 2 * Math.PI / s ) * .5 + r + n
    },
    easeInBack: function( e, t, n, r, i, s ) {
        if ( s == undefined )
            s = 1.70158;
        return r * ( t /= i ) * t * ( ( s + 1 ) * t - s ) + n
    },
    easeOutBack: function( e, t, n, r, i, s ) {
        if ( s == undefined )
            s = 1.70158;
        return r * ( ( t = t / i - 1 ) * t * ( ( s + 1 ) * t + s ) + 1 ) + n
    },
    easeInOutBack: function( e, t, n, r, i, s ) {
        if ( s == undefined )
            s = 1.70158;
        if ( ( t /= i / 2 ) < 1 ) return r / 2 * t * t * ( ( ( s *= 1.525 ) + 1 ) * t - s ) + n;
        return r / 2 * ( ( t -= 2 ) * t * ( ( ( s *= 1.525 ) + 1 ) * t + s ) + 2 ) + n
    },
    easeInBounce: function( e, t, n, r, i ) {
        return r - jQuery.easing.easeOutBounce( e, i - t, 0, r, i ) + n
    },
    easeOutBounce: function( e, t, n, r, i ) {
        return ( t /= i ) < 1 / 2.75 ? r * 7.5625 * t * t + n : t < 2 / 2.75 ? r * ( 7.5625 * ( t -= 1.5 / 2.75 ) * t + .75 ) + n : t < 2.5 / 2.75 ? r * ( 7.5625 * ( t -= 2.25 / 2.75 ) * t + .9375 ) + n : r * ( 7.5625 * ( t -= 2.625 / 2.75 ) * t + .984375 ) + n
    },
    easeInOutBounce: function( e, t, n, r, i ) {
        if ( t < i / 2 ) return jQuery.easing.easeInBounce( e, t * 2, 0, r, i ) * .5 + n;
        return jQuery.easing.easeOutBounce( e, t * 2 - i, 0, r, i ) * .5 + r * .5 + n
    }
});
(function( e ) {
function t( t, n ) {
    this.slider = e( t );
    this._az = "";
    this._by = "";
    this._cx = "";
    var r = this;
    this.settings = e.extend({}, e.fn.royalSlider.defaults, n );
    this.isSlideshowRunning = false;
    this._dw = false;
    this._ev = this.slider.find( ".royalSlidesContainer" );
    this._fu = this._ev.wrap( '<div class="royalWrapper"/>' ).parent();
    this.slides = this._ev.find( ".royalSlide" );
    this._gt = "<p class='royalPreloader'></p>";
    this._hs = false;
    this._ir = false;
    if ( "ontouchstart" in window ) {
        if ( !this.settings.disableTranslate3d ) {
            if ( "WebKitCSSMatrix" in window && "m11" in new WebKitCSSMatrix ) {
                this._ev.css({
                    "-webkit-transform-origin": "0 0",
                    "-webkit-transform": "translateZ(0)"
                });
                this._ir = true
            }
        }
        this.hasTouch = true;
        this._az = "touchstart.rs";
        this._by = "touchmove.rs";
        this._cx = "touchend.rs"
    } else {
        this.hasTouch = false;
        if ( this.settings.dragUsingMouse ) {
            this._az = "mousedown.rs";
            this._by = "mousemove.rs";
            this._cx = "mouseup.rs"
        } else {
            this._ev.addClass( "auto-cursor" )
        }
    }
    if ( this.hasTouch ) {
        this.settings.directionNavAutoHide = false;
        this.settings.hideArrowOnLastSlide = true
    }
    if ( e.browser.msie && parseInt( e.browser.version, 10 ) <= 8 ) {
        this._jq = true
    } else {
        this._jq = false
    }
    this.slidesArr = [];
    var i, s, o, u;
    this.slides.each(function() {
        s = e( this );
        i = {};
        i.slide = s;
        if ( r.settings.blockLinksOnDrag ) {
            if ( !this.hasTouch ) {
                s.find( "a" ).bind( "click.rs", function( e ) {
                    if ( r._hs ) {
                        e.preventDefault();
                        return false
                    }
                })
            } else {
                var t = s.find( "a" );
                var n;
                t.each(function() {
                    n = e( this );
                    n.data( "royalhref", n.attr( "href" ) );
                    n.data( "royaltarget", n.attr( "target" ) );
                    n.attr( "href", "#" );
                    n.bind( "click", function( t ) {
                        t.preventDefault();
                        if ( r._hs ) {
                            return false
                        } else {
                            var n = e( this ).data( "royalhref" );
                            var i = e( this ).data( "royaltarget" );
                            if ( !i || i.toLowerCase() === "_kp" ) {
                                window.location.href = n
                            } else {
                                window.open( n )
                            }
                        }
                    })
                })
            }
        }
        if ( r.settings.nonDraggableClassEnabled ) {
            s.find( ".non-draggable" ).bind( r._az, function( e ) {
                r._hs = false;
                e.stopImmediatePropagation()
            })
        }
        o = s.attr( "data-src" );
        if ( o == undefined || o == "" || o == "none" ) {
            i.preload = false
        } else {
            i.preload = true;
            i.preloadURL = o
        }
        if ( r.settings.captionAnimationEnabled ) {
            i.caption = s.find( ".royalCaption" ).css( "display", "none" )
        }
        r.slidesArr.push( i )
    });
    this._lo = false;
    if ( this.settings.removeCaptionsOpacityInIE8 ) {
        if ( e.browser.msie && parseInt( e.browser.version, 10 ) <= 8 ) {
            this._lo = true
        }
    }
    if ( this.settings.autoScaleSlider ) {
        this.sliderScaleRatio = this.settings.autoScaleSliderHeight / this.settings.autoScaleSliderWidth
    }
    this.slider.css( "overflow", "visible" );
    this.slideWidth = 0;
    this.slideshowTimer = "";
    this.mn = false;
    this.numSlides = this.slides.length;
    this.currentSlideId = this.settings.startSlideIndex;
    this.lastSlideId = -1;
    this.isAnimating = true;
    this.wasSlideshowPlaying = false;
    this._az1 = 0;
    this._by1 = 0;
    this._cx1 = false;
    this._dw1 = [];
    this._ev1 = [];
    this._fu1 = false;
    this._gt1 = false;
    this._hs1 = 0;
    this._ir1 = 0;
    this._jq1 = 0;
    this._kp1 = 0;
    this._lo1 = 0;
    this._mn1 = 0;
    this._az2 = false;
    this._by2 = false;
    if ( this.settings.slideTransitionType === "fade" ) {
        if ( this._ir || "WebKitCSSMatrix" in window && "m11" in new WebKitCSSMatrix ) {
            this._cx2 = true
        } else {
            this._cx2 = false
        }
        this._dw2 = e( "<div class='fade-container'></div>" ).appendTo( this._fu )
    }
    if ( this.settings.slideshowEnabled && this.settings.slideshowDelay > 0 ) {
        if ( !this.hasTouch && this.settings.slideshowPauseOnHover ) {
            this.slider.hover(function() {
                r._by2 = true;
                r._ev2( true )
            }, function() {
                    r._by2 = false;
                    r._fu2( true )
                })
        }
        this.slideshowEnabled = true
    } else {
        this.slideshowEnabled = false
    }
    this._gt2();
    if ( this.settings.controlNavEnabled ) {
        var a;
        this._hs2Container = "";
        var f;
        if ( !r.settings.controlNavThumbs ) {
            this._hs2Container = e( '<div class="royalControlNavOverflow"><div class="royalControlNavContainer"><div class="royalControlNavCenterer"></div></div></div>' );
            a = this._hs2Container.find( ".royalControlNavCenterer" )
        } else {
            this.slider.addClass( "with-thumbs" );
            if ( r.settings.controlNavThumbsNavigation ) {
                f = e( '<div class="thumbsAndArrowsContainer"></div>' );
                this.thumbsArrowLeft = e( "<a href='#' class='thumbsArrow left'></a>" );
                this.thumbsArrowRight = e( "<a href='#' class='thumbsArrow right'></a>" );
                f.append( this.thumbsArrowLeft );
                f.append( this.thumbsArrowRight );
                var l = parseInt( this.thumbsArrowLeft.outerWidth(), 10 );
                this._hs2Container = e( '<div class="royalControlNavOverflow royalThumbs"><div class="royalControlNavThumbsContainer"></div></div>' );
                a = this._hs2Container.find( ".royalControlNavThumbsContainer" )
            } else {
                this._hs2Container = e( '<div class="royalControlNavOverflow royalThumbs"><div class="royalControlNavContainer"><div class="royalControlNavCenterer"></div></div></div>' );
                a = this._hs2Container.find( ".royalControlNavCenterer" )
            }
        }
        var c = 0;
        numero = 1;
        this.slides.each(function( t ) {
            if ( r.settings.controlNavThumbs ) {
                a.append( '<a href="#" class="royalThumb" style="background-image:url(' + e( this ).attr( "data-thumb" ) + ')">' + ( t + 1 ) + "</a>" )
            } else {
                a.append( '<a id="numero' + numero + '"  href="#">' + ( t + 1 ) + "</a>" )
            }
            c++;
            numero++
        });
        this.navItems = a.children();
        if ( f ) {
            f.append( this._hs2Container );
            this._fu.after( f )
        } else {
            this._fu.after( this._hs2Container )
        }
        if ( r.settings.controlNavThumbs && r.settings.controlNavThumbsNavigation ) {
            this._kp2 = true;
            this._lo2 = false;
            this._mn2 = a;
            if ( this._ir ) {
                this._mn2.css({
                    "-webkit-transition-duration": this.settings.controlNavThumbsSpeed + "ms",
                    "-webkit-transition-property": "-webkit-transform",
                    "-webkit-transition-timing-function": "ease-in-out"
                })
            }
            this._az3 = c;
            var h = this.navItems.eq( 0 );
            this._by3 = h.outerWidth( true );
            this._cx3 = this._by3 * this._az3;
            this._mn2.css( "width", this._cx3 );
            this._dw3 = parseInt( h.css( "marginRight" ), 10 );
            this._cx3 -= this._dw3;
            this._ev3 = 0;
            this._fu3();
            this.thumbsArrowLeft.click(function( e ) {
                e.preventDefault();
                if ( !r._kp2 ) {
                    r._gt3( r._ev3 + r._hs3 + r._dw3 )
                }
            });
            this.thumbsArrowRight.click(function( e ) {
                e.preventDefault();
                if ( !r._lo2 ) {
                    r._gt3( r._ev3 - r._hs3 - r._dw3 )
                }
            })
        }
        this._ir3()
    }
    if ( this.settings.directionNavEnabled ) {
        this._fu.after( "<a style='height: 142px;width: 45px;display: table;margin-top: -240px;z-index: 50;position: relative;background: url(http://www.aazperfumes.com.br/arquivos/arrow_left.png)' href='#' class='arrow setas left'/>" );
        this._fu.after( "<a style='height: 142px;width: 45px;display: table;margin-top: -240px;z-index: 50;position: relative;background: url(http://www.aazperfumes.com.br/arquivos/arrow_right.png)' href='#' class='arrow setas right'/>" );
        this.arrowLeft = this.slider.find( "a.arrow.left" );
        this.arrowRight = this.slider.find( "a.arrow.right" );
        if ( this.arrowLeft.length < 1 || this.arrowRight.length < 1 ) {
            this.settings.directionNavEnabled = false
        } else if ( this.settings.directionNavAutoHide ) {
            this.arrowLeft.hide();
            this.arrowRight.hide();
            this.slider.one( "mousemove.arrowshover", function() {
                r.arrowLeft.fadeIn( "fast" );
                r.arrowRight.fadeIn( "fast" )
            });
            this.slider.hover(function() {
                r.arrowLeft.fadeIn( "fast" );
                r.arrowRight.fadeIn( "fast" )
            }, function() {
                    r.arrowLeft.fadeOut( "fast" );
                    r.arrowRight.fadeOut( "fast" )
                })
        }
        this._jq3()
    }
    this.sliderWidth = 0;
    this.sliderHeight = 0;
    var p;
    this._kp3 = "onorientationchange" in window ? "orientationchange.royalslider" : "resize.royalslider";
    e( window ).bind( this._kp3, function() {
        if ( p ) {
            clearTimeout( p )
        }
        p = setTimeout(function() {
            r.updateSliderSize()
        }, 100 )
    });
    this.updateSliderSize();
    this.settings.beforeLoadStart.call( this );
    var d = this.slidesArr[ this.currentSlideId ];
    if ( this.currentSlideId != 0 ) {
        if ( !this._ir ) {
            this._ev.css({
                left: -this.currentSlideId * this.slideWidth
            })
        } else {
            this._ev.css({
                "-webkit-transition-duration": "0",
                "-webkit-transition-property": "none"
            });
            this._ev.css({
                "-webkit-transform": "translate3d(" + -this.currentSlideId * this.slideWidth + "px, 0, 0)"
            })
        }
    }
    if ( this.settings.welcomeScreenEnabled ) {
        function v( e ) {
            r.settings.loadingComplete.call( r );
            if ( e && r.settings.preloadNearbyImages ) {
                r._lo3( r.currentSlideId )
            }
            r.slider.find( ".royalLoadingScreen" ).fadeOut( r.settings.welcomeScreenShowSpeed );
            setTimeout(function() {
                r._mn3()
            }, r.settings.welcomeScreenShowSpeed + 100 )
        }
        if ( d.preload ) {
            this._lo3( this.currentSlideId, function() {
                v( false )
            })
        } else {
            u = d.slide.find( "img.royalImage" )[ 0 ];
            if ( u ) {
                if ( this._az4( u ) ) {
                    v( true );
                    e( u ).css( "opacity", 0 );
                    e( u ).animate({
                        opacity: 1
                    }, "fast" )
                } else {
                    e( u ).css( "opacity", 0 );
                    e( "<img />" ).load(function() {
                        v( true );
                        e( u ).animate({
                            opacity: 1
                        }, "fast" )
                    }).attr( "src", u.src )
                }
            } else {
                v( true )
            }
        }
    } else {
        if ( d.preload ) {
            this._by4( d, function() {
                r.settings.loadingComplete.call( r );
                if ( r.settings.preloadNearbyImages ) {
                    r._lo3( r.currentSlideId )
                }
            })
        } else {
            u = d.slide.find( "img.royalImage" )[ 0 ];
            if ( u ) {
                if ( this._az4( u ) ) {
                    e( u ).css( "opacity", 0 ).animate({
                        opacity: 1
                    }, "fast" )
                } else {
                    e( u ).css( "opacity", 0 );
                    e( "<img />" ).load(function() {
                        e( u ).animate({
                            opacity: 1
                        }, "fast" )
                    }).attr( "src", u.src )
                }
            }
            this.settings.loadingComplete.call( this )
        }
        setTimeout(function() {
            r._mn3()
        }, 100 )
    }
}
t.prototype = {
    goTo: function( e, t, n, r, i ) {
        if ( !this.isAnimating ) {
            this.isAnimating = true;
            var s = this;
            this.lastSlideId = this.currentSlideId;
            this.currentSlideId = e;
            this._gt1 = true;
            this._fu1 = true;
            if ( this.lastSlideId != e ) {
                this._ir3( n );
                this._lo3( e )
            }
            this._jq3();
            this.settings.beforeSlideChange.call( this );
            if ( this.slideshowEnabled && this.slideshowTimer ) {
                this.wasSlideshowPlaying = true;
                this._ev2()
            }
            var o = !t ? this.settings.slideTransitionSpeed : 0;
            if ( r || t || this.settings.slideTransitionType === "move" ) {
                var u;
                if ( i > 0 ) {
                    o = i
                } else {
                    u = this.settings.slideTransitionEasing
                }
                if ( !this._ir ) {
                    if ( parseInt( this._ev.css( "left" ), 10 ) !== -this.currentSlideId * this.slideWidth ) {
                        this._ev.animate({
                            left: -this.currentSlideId * this.slideWidth
                        }, o, i > 0 ? "easeOutSine" : this.settings.slideTransitionEasing, function() {
                                s._cx4()
                            })
                    } else {
                        this._cx4()
                    }
                } else {
                    if ( this._dw4() !== -this.currentSlideId * this.slideWidth ) {
                        this._ev.bind( "webkitTransitionEnd.rs", function( e ) {
                            if ( e.target == s._ev.get( 0 ) ) {
                                s._ev.unbind( "webkitTransitionEnd.rs" );
                                s._cx4()
                            }
                        });
                        this._ev.css({
                            "-webkit-transition-duration": o + "ms",
                            "-webkit-transition-property": "-webkit-transform",
                            "-webkit-transition-timing-function": i > 0 ? "ease-out" : "ease-in-out",
                            "-webkit-transform": "translate3d(" + -this.currentSlideId * this.slideWidth + "px, 0, 0)"
                        })
                    } else {
                        this._cx4()
                    }
                }
            } else {
                var a = this.slidesArr[ this.lastSlideId ].slide;
                var f = a.clone().appendTo( this._dw2 );
                if ( !this._cx2 ) {
                    this._ev.css({
                        left: -this.currentSlideId * this.slideWidth
                    });
                    f.animate({
                        opacity: 0
                    }, o, this.settings.slideTransitionEasing, function() {
                            f.remove();
                            s._cx4()
                        })
                } else {
                    if ( !this._ir ) {
                        this._ev.css({
                            left: -this.currentSlideId * this.slideWidth
                        })
                    } else {
                        this._ev.css({
                            "-webkit-transition-duration": "0",
                            "-webkit-transform": "translate3d(" + -this.currentSlideId * this.slideWidth + "px, 0, 0)",
                            opacity: "1"
                        })
                    }
                    setTimeout(function() {
                        f.bind( "webkitTransitionEnd.rs", function( e ) {
                            if ( e.target == f.get( 0 ) ) {
                                f.unbind( "webkitTransitionEnd.rs" );
                                f.remove();
                                s._cx4()
                            }
                        });
                        f.css({
                            "-webkit-transition-duration": o + "ms",
                            "-webkit-transition-property": "opacity",
                            "-webkit-transition-timing-function": "ease-in-out"
                        });
                        f.css( "opacity", 0 )
                    }, 100 )
                }
            }
        }
    },
    goToSilent: function( e ) {
        this.goTo( e, true )
    },
    prev: function() {
        if ( this.currentSlideId <= 0 ) {
            this.goTo( this.numSlides - 1 )
        } else {
            this._ev4()
        }
    },
    next: function() {
        if ( this.currentSlideId >= this.numSlides - 1 ) {
            this.goTo( 0 )
        } else {
            this._fu4()
        }
    },
    updateSliderSize: function() {
        var e = this;
        var t;
        var n;
        if ( this.settings.autoScaleSlider ) {
            t = this.slider.width();
            if ( t != this.sliderWidth ) {
                this.slider.css( "height", t * this.sliderScaleRatio )
            }
        }
        t = this.slider.width();
        n = this.slider.height();
        if ( t != this.sliderWidth || n != this.sliderHeight ) {
            this.sliderWidth = t;
            this.sliderHeight = n;
            this.slideWidth = this.sliderWidth + this.settings.slideSpacing;
            var r = this.slidesArr.length;
            var i, s;
            for ( var o = 0, u = r; o < u; ++o ) {
                i = this.slidesArr[ o ];
                s = i.slide.find( "img.royalImage" ).eq( 0 );
                if ( s && i.preload == false ) {
                    this._ir4( s, this.sliderWidth, this.sliderHeight )
                }
                if ( this.settings.slideSpacing > 0 && o < r - 1 ) {
                    i.slide.css( "cssText", "margin-right:" + this.settings.slideSpacing + "px !important;" )
                }
                i.slide.css({
                    height: e.sliderHeight,
                    width: e.sliderWidth
                })
            }
            if ( !this._ir ) {
                this._ev.css({
                    left: -this.currentSlideId * this.slideWidth,
                    width: this.slideWidth * this.numSlides
                })
            } else {
                if ( !this._gt1 ) {
                    this._ev.css({
                        "-webkit-transition-duration": "0",
                        "-webkit-transition-property": "none"
                    });
                    this._ev.css({
                        "-webkit-transform": "translate3d(" + -this.currentSlideId * this.slideWidth + "px, 0, 0)",
                        width: this.slideWidth * this.numSlides
                    })
                }
            }
            if ( this.settings.controlNavThumbs && this.settings.controlNavThumbsNavigation ) {
                this._fu3()
            }
        }
    },
    stopSlideshow: function() {
        this._ev2();
        this.slideshowEnabled = false;
        this.wasSlideshowPlaying = false
    },
    resumeSlideshow: function() {
        this.slideshowEnabled = true;
        if ( !this.wasSlideshowPlaying ) {
            this._fu2()
        }
    },
    destroy: function() {
        this._ev2();
        this._ev.unbind( this._az );
        e( document ).unbind( this._by ).unbind( this._cx );
        e( window ).unbind( this._kp3 );
        if ( this.settings.keyboardNavEnabled ) {
            e( document ).unbind( "keydown.rs" )
        }
        this.slider.remove();
        delete this.slider
    },
    _lo3: function( e, t ) {
        if ( this.settings.preloadNearbyImages ) {
            var n = this;
            this._by4( this.slidesArr[ e ], function() {
                if ( t ) {
                    t.call()
                }
                n._by4( n.slidesArr[ e + 1 ], function() {
                    n._by4( n.slidesArr[ e - 1 ] )
                })
            })
        } else {
            this._by4( this.slidesArr[ e ], t )
        }
    },
    _ir3: function( e ) {
        if ( this.settings.controlNavEnabled ) {
            this.navItems.eq( this.lastSlideId ).removeClass( "current" );
            this.navItems.eq( this.currentSlideId ).addClass( "current" );
            if ( this.settings.controlNavThumbs && this.settings.controlNavThumbsNavigation ) {
                var t = this.navItems.eq( this.currentSlideId ).position().left;
                var n = t - Math.abs( this._ev3 );
                if ( n > this._hs3 - this._by3 * 2 - 1 - this._dw3 ) {
                    if ( !e ) {
                        this._gt3( -t + this._by3 )
                    } else {
                        this._gt3( -t - this._by3 * 2 + this._hs3 + this._dw3 )
                    }
                } else if ( n < this._by3 * 2 - 1 ) {
                    if ( !e ) {
                        this._gt3( -t - this._by3 * 2 + this._hs3 + this._dw3 )
                    } else {
                        this._gt3( -t + this._by3 )
                    }
                }
            }
        }
    },
    _jq3: function() {
        if ( this.settings.directionNavEnabled ) {
            if ( this.settings.hideArrowOnLastSlide ) {
                if ( this.currentSlideId == 0 ) {
                    this._lo4 = true;
                    this.arrowLeft.addClass( "disabled" );
                    if ( this._mn4 ) {
                        this._mn4 = false;
                        this.arrowRight.removeClass( "disabled" )
                    }
                } else if ( this.currentSlideId == this.numSlides - 1 ) {
                    this._mn4 = true;
                    this.arrowRight.addClass( "disabled" );
                    if ( this._lo4 ) {
                        this._lo4 = false;
                        this.arrowLeft.removeClass( "disabled" )
                    }
                } else {
                    if ( this._lo4 ) {
                        this._lo4 = false;
                        this.arrowLeft.removeClass( "disabled" )
                    } else if ( this._mn4 ) {
                        this._mn4 = false;
                        this.arrowRight.removeClass( "disabled" )
                    }
                }
            }
        }
    },
    _fu2: function( e ) {
        if ( this.slideshowEnabled ) {
            var t = this;
            if ( !this.slideshowTimer ) {
                this.slideshowTimer = setInterval(function() {
                    t.next()
                }, this.settings.slideshowDelay )
            }
        }
    },
    _ev2: function( e ) {
        if ( this.slideshowTimer ) {
            clearInterval( this.slideshowTimer );
            this.slideshowTimer = ""
        }
    },
    _by4: function( t, n ) {
        if ( t ) {
            if ( t.preload ) {
                var r = this;
                var i = new Image;
                var s = e( i );
                s.css( "opacity", 0 );
                s.addClass( "royalImage" );
                t.slide.prepend( s );
                t.slide.prepend( this._gt );
                t.preload = false;
                s.load(function() {
                    r._ir4( s, r.sliderWidth, r.sliderHeight );
                    s.animate({
                        opacity: 1
                    }, 300, function() {
                            t.slide.find( ".royalPreloader" ).remove()
                        });
                    if ( n ) {
                        n.call()
                    }
                }).attr( "src", t.preloadURL )
            } else {
                if ( n ) {
                    n.call()
                }
            }
        } else {
            if ( n ) {
                n.call()
            }
        }
    },
    _fu3: function() {
        this._hs3 = parseInt( this._hs2Container.width(), 10 );
        this._az5 = -( this._cx3 - this._hs3 );
        if ( this._hs3 >= this._cx3 ) {
            this._lo2 = true;
            this._kp2 = true;
            this.thumbsArrowRight.addClass( "disabled" );
            this.thumbsArrowLeft.addClass( "disabled" );
            this._cx1 = true;
            this._by5( 0 )
        } else {
            this._cx1 = false;
            var e = this.navItems.eq( this.currentSlideId ).position().left;
            this._gt3( -e + this._by3 )
        }
    },
    _gt3: function( e ) {
        if ( !this._cx1 && e != this._ev3 ) {
            if ( e <= this._az5 ) {
                e = this._az5;
                this._kp2 = false;
                this._lo2 = true;
                this.thumbsArrowRight.addClass( "disabled" );
                this.thumbsArrowLeft.removeClass( "disabled" )
            } else if ( e >= 0 ) {
                e = 0;
                this._kp2 = true;
                this._lo2 = false;
                this.thumbsArrowLeft.addClass( "disabled" );
                this.thumbsArrowRight.removeClass( "disabled" )
            } else {
                if ( this._kp2 ) {
                    this._kp2 = false;
                    this.thumbsArrowLeft.removeClass( "disabled" )
                }
                if ( this._lo2 ) {
                    this._lo2 = false;
                    this.thumbsArrowRight.removeClass( "disabled" )
                }
            }
            this._by5( e );
            this._ev3 = e
        }
    },
    _by5: function( e ) {
        if ( !this._ir ) {
            this._mn2.animate({
                left: e
            }, this.settings.controlNavThumbsSpeed, this.settings.controlNavThumbsEasing )
        } else {
            this._mn2.css({
                "-webkit-transform": "translate3d(" + e + "px, 0, 0)"
            })
        }
    },
    _mn3: function() {
        var t = this;
        this.slider.find( ".royalLoadingScreen" ).remove();
        if ( this.settings.controlNavEnabled ) {
            this.navItems.bind( "click", function( e ) {
                e.preventDefault();
                if ( !t._fu1 ) {
                    t._cx5( e )
                }
            })
        }
        if ( this.settings.directionNavEnabled ) {
            this.arrowRight.click(function( e ) {
                e.preventDefault();
                if ( !t._mn4 && !t._fu1 ) {
                    t.next()
                }
            });
            this.arrowLeft.click(function( e ) {
                e.preventDefault();
                if ( !t._lo4 && !t._fu1 ) {
                    t.prev()
                }
            })
        }
        if ( this.settings.keyboardNavEnabled ) {
            e( document ).bind( "keydown.rs", function( e ) {
                if ( !t._fu1 ) {
                    if ( e.keyCode === 37 ) {
                        t.prev()
                    } else if ( e.keyCode === 39 ) {
                        t.next()
                    }
                }
            })
        }
        this.wasSlideshowPlaying = true;
        this._cx4();
        this._ev.bind( this._az, function( e ) {
            if ( !t._gt1 ) {
                t._dw5( e )
            } else if ( !t.hasTouch ) {
                e.preventDefault()
            }
        });
        if ( this.slideshowEnabled && !this.settings.slideshowAutoStart ) {
            this._ev2()
        }
        this.settings.allComplete.call( this )
    },
    _gt2: function() {
        this._ev.removeClass( "grabbing-cursor" );
        this._ev.addClass( "grab-cursor" )
    },
    _ev5: function() {
        this._ev.removeClass( "grab-cursor" );
        this._ev.addClass( "grabbing-cursor" )
    },
    _fu4: function( e, t ) {
        if ( this.currentSlideId < this.numSlides - 1 ) {
            this.goTo( this.currentSlideId + 1, false, false, e, t )
        } else {
            this.goTo( this.currentSlideId, false, false, e, t )
        }
    },
    _ev4: function( e, t ) {
        if ( this.currentSlideId > 0 ) {
            this.goTo( this.currentSlideId - 1, false, false, e, t )
        } else {
            this.goTo( this.currentSlideId, false, false, e, t )
        }
    },
    _cx5: function( t ) {
        this.goTo( e( t.currentTarget ).index(), false, true )
    },
    _dw4: function() {
        var e = window.getComputedStyle( this._ev.get( 0 ), null ).getPropertyValue( "-webkit-transform" );
        var t = e.replace( /^matrix\(/i, "" ).split( /, |\)$/g );
        return parseInt( t[ 4 ], 10 )
    },
    _dw5: function( t ) {
        if ( !this._az2 ) {
            var n;
            if ( this.hasTouch ) {
                this._fu5 = false;
                var r = t.originalEvent.touches;
                if ( r && r.length > 0 ) {
                    n = r[ 0 ]
                } else {
                    return false
                }
            } else {
                n = t;
                t.preventDefault()
            }
            if ( this.slideshowEnabled ) {
                if ( this.slideshowTimer ) {
                    this.wasSlideshowPlaying = true;
                    this._ev2()
                } else {
                    this.wasSlideshowPlaying = false
                }
            }
            this._ev5();
            this._az2 = true;
            var i = this;
            if ( this._ir ) {
                i._ev.css({
                    "-webkit-transition-duration": "0",
                    "-webkit-transition-property": "none"
                })
            }
            e( document ).bind( this._by, function( e ) {
                i._gt5( e )
            });
            e( document ).bind( this._cx, function( e ) {
                i._hs5( e )
            });
            if ( !this._ir ) {
                this._mn1 = this._jq1 = parseInt( this._ev.css( "left" ), 10 )
            } else {
                this._mn1 = this._jq1 = this._dw4()
            }
            this._hs = false;
            this._ir1 = this._jq1;
            this._hs1 = t.timeStamp || ( new Date ).getTime();
            this._kp1 = n.clientX;
            this._lo1 = n.clientY
        }
        return false
    },
    _gt5: function( e ) {
        var t;
        if ( this.hasTouch ) {
            if ( this._fu5 ) {
                return false
            }
            var n = e.originalEvent.touches;
            if ( n.length > 1 ) {
                return false
            }
            t = n[ 0 ];
            if ( Math.abs( t.clientY - this._lo1 ) > Math.abs( t.clientX - this._kp1 ) + 3 ) {
                if ( this.settings.lockAxis ) {
                    this._fu5 = true
                }
                return false
            }
            e.preventDefault()
        } else {
            t = e;
            e.preventDefault()
        }
        this._by1 = this._az1;
        var r = t.clientX - this._kp1;
        if ( this._by1 != r ) {
            this._az1 = r
        }
        if ( r != 0 ) {
            if ( this.currentSlideId == 0 ) {
                if ( r > 0 ) {
                    r = Math.sqrt( r ) * 5
                }
            } else if ( this.currentSlideId == this.numSlides - 1 ) {
                if ( r < 0 ) {
                    r = -Math.sqrt( -r ) * 5
                }
            }
            if ( !this._ir ) {
                this._ev.css( "left", this._jq1 + r )
            } else {
                this._ev.css({
                    "-webkit-transform": "translate3d(" + ( this._jq1 + r ) + "px, 0, 0)"
                })
            }
        }
        var i = e.timeStamp || ( new Date ).getTime();
        if ( i - this._hs1 > 350 ) {
            this._hs1 = i;
            this._ir1 = this._jq1 + r
        }
        return false
    },
    _hs5: function( t ) {
        if ( this._az2 ) {
            var n = this;
            this._az2 = false;
            this._gt2();
            if ( !this._ir ) {
                this.endPos = parseInt( this._ev.css( "left" ), 10 )
            } else {
                this.endPos = this._dw4()
            }
            this.isdrag = false;
            e( document ).unbind( this._by ).unbind( this._cx );
            if ( this.slideshowEnabled ) {
                if ( this.wasSlideshowPlaying ) {
                    if ( !this._by2 ) {
                        this._fu2()
                    }
                    this.wasSlideshowPlaying = false
                }
            }
            if ( this.endPos == this._mn1 ) {
                this._hs = false;
                return
            } else {
                this._hs = true
            }
            var r = this._ir1 - this.endPos;
            var i = Math.max( 40, ( t.timeStamp || ( new Date ).getTime() ) - this._hs1 );
            var s = Math.abs( r ) / i;
            var o = this.slideWidth - Math.abs( this._mn1 - this.endPos );
            var u = Math.max( o * 1.08 / s, 200 );
            u = Math.min( u, 600 );

            function a() {
                o = Math.abs( n._mn1 - n.endPos );
                u = Math.max( o * 1.08 / s, 200 );
                u = Math.min( u, 500 );
                n.goTo( n.currentSlideId, false, false, true, u )
            }
            if ( this._mn1 - this.settings.minSlideOffset > this.endPos ) {
                if ( this._by1 < this._az1 ) {
                    a();
                    return false
                }
                this._fu4( true, u )
            } else if ( this._mn1 + this.settings.minSlideOffset < this.endPos ) {
                if ( this._by1 > this._az1 ) {
                    a();
                    return false
                }
                this._ev4( true, u )
            } else {
                a()
            }
        }
        return false
    },
    _cx4: function() {
        var e = this;
        if ( this.slideshowEnabled ) {
            if ( this.wasSlideshowPlaying ) {
                if ( !this._by2 ) {
                    this._fu2()
                }
                this.wasSlideshowPlaying = false
            }
        }
        this._fu1 = false;
        this._gt1 = false;
        if ( this.settings.captionAnimationEnabled && this.lastSlideId != this.currentSlideId ) {
            if ( this.lastSlideId != -1 ) {
                this.slidesArr[ this.lastSlideId ].caption.css( "display", "none" )
            }
            e._ir5( e.currentSlideId )
        }
        this.isAnimating = false;
        this.settings.afterSlideChange.call( this )
    },
    _ir5: function( t ) {
        var n = this.slidesArr[ t ].caption;
        if ( n && n.length > 0 ) {
            n.css( "display", "block" );
            var r = this;
            var i, s, o, u, a, f, l, c, h, p, d, v, m;
            var g = n.children();
            if ( this._dw1.length > 0 ) {
                for ( var y = this._dw1.length - 1; y > -1; y-- ) {
                    clearTimeout( this._dw1.splice( y, 1 ) )
                }
            }
            if ( this._ev1.length > 0 ) {
                var b;
                for ( var w = this._ev1.length - 1; w > -1; w-- ) {
                    b = this._ev1[ w ];
                    if ( b ) {
                        if ( !this._ir ) {
                            if ( b.running ) {
                                b.captionItem.stop( true, true )
                            } else {
                                b.captionItem.css( b.css )
                            }
                        }
                    }
                    this._ev1.splice( w, 1 )
                }
            }
            for ( var E = 0; E < g.length; E++ ) {
                i = e( g[ E ] );
                a = {};
                s = false;
                o = false;
                f = "";
                if ( i.attr( "data-show-effect" ) == undefined ) {
                    l = this.settings.captionShowEffects
                } else {
                    l = i.attr( "data-show-effect" ).split( " " )
                }
                for ( var S = 0; S < l.length; S++ ) {
                    if ( s && o ) {
                        break
                    }
                    u = l[ S ].toLowerCase();
                    if ( !s && u == "fade" ) {
                        s = true;
                        a[ "opacity" ] = 1
                    } else if ( o ) {
                        break
                    } else if ( u == "movetop" ) {
                        f = "margin-top"
                    } else if ( u == "moveleft" ) {
                        f = "margin-left"
                    } else if ( u == "movebottom" ) {
                        f = "margin-bottom"
                    } else if ( u == "moveright" ) {
                        f = "margin-right"
                    }
                    if ( f != "" ) {
                        a[ "moveProp" ] = f;
                        if ( !r._ir ) {
                            a[ "moveStartPos" ] = parseInt( i.css( f ), 10 )
                        } else {
                            a[ "moveStartPos" ] = 0
                        }
                        o = true
                    }
                }
                h = parseInt( i.attr( "data-move-offset" ), 10 );
                if ( isNaN( h ) ) {
                    h = this.settings.captionMoveOffset
                }
                p = parseInt( i.attr( "data-delay" ), 10 );
                if ( isNaN( p ) ) {
                    p = r.settings.captionShowDelay * E
                }
                d = parseInt( i.attr( "data-speed" ), 10 );
                if ( isNaN( d ) ) {
                    d = r.settings.captionShowSpeed
                }
                v = i.attr( "data-easing" );
                if ( !v ) {
                    v = r.settings.captionShowEasing
                }
                c = {};
                if ( o ) {
                    m = a.moveProp;
                    if ( m == "margin-right" ) {
                        m = "margin-left";
                        c[ m ] = a.moveStartPos + h
                    } else if ( m == "margin-bottom" ) {
                        m = "margin-top";
                        c[ m ] = a.moveStartPos + h
                    } else {
                        c[ m ] = a.moveStartPos - h
                    }
                }
                if ( !r._lo && s ) {
                    i.css( "opacity", 0 )
                }
                if ( !r._ir ) {
                    i.css( "visibility", "hidden" );
                    i.css( c );
                    if ( o ) {
                        c[ m ] = a.moveStartPos
                    }
                    if ( !r._lo && s ) {
                        c.opacity = 1
                    }
                } else {
                    var x = {};
                    if ( o ) {
                        x[ "-webkit-transition-duration" ] = "0";
                        x[ "-webkit-transition-property" ] = "none";
                        x[ "-webkit-transform" ] = "translate3d(" + ( isNaN( c[ "margin-left" ] ) ? 0 : c[ "margin-left" ] + "px" ) + ", " + ( isNaN( c[ "margin-top" ] ) ? 0 : c[ "margin-top" ] + "px" ) + ",0)";
                        delete c[ "margin-left" ];
                        delete c[ "margin-top" ];
                        c[ "-webkit-transform" ] = "translate3d(0,0,0)"
                    }
                    c.visibility = "visible";
                    c.opacity = 1;
                    if ( !r._lo && s ) {
                        x[ "opacity" ] = 0
                    }
                    x[ "visibility" ] = "hidden";
                    i.css( x )
                }
                this._ev1.push({
                    captionItem: i,
                    css: c,
                    running: false
                });
                this._dw1.push( setTimeout( function( e, t, n, i, s, o, u ) {
                    return function() {
                        r._ev1[ s ].running = true;
                        if ( !r._ir ) {
                            e.css( "visibility", "visible" ).animate( t, n, i, function() {
                                if ( r._jq && o ) {
                                    e.get( 0 ).style.removeAttribute( "filter" )
                                }
                                delete r._ev1[ s ]
                            })
                        } else {
                            e.css({
                                "-webkit-transition-duration": n + "ms",
                                "-webkit-transition-property": "opacity" + ( u ? ", -webkit-transform" : "" ),
                                "-webkit-transition-timing-function": "ease-out"
                            });
                            e.css( t )
                        }
                    }
                }( i, c, d, v, E, s, o ), p ) )
            }
        }
    },
    _ir4: function( t, n, r ) {
        var i = this.settings.imageScaleMode;
        var s = this.settings.imageAlignCenter;
        if ( s || i == "fill" || i == "fit" ) {
            var o = false;

            function u() {
                var e, o, u, a, l;
                var c = new Image;
                c.onload = function() {
                    var c = this.width;
                    var p = this.height;
                    var v = parseInt( t.css( "borderWidth" ), 10 );
                    v = isNaN( v ) ? 0 : v;
                    if ( i == "fill" || i == "fit" ) {
                        e = n / c;
                        o = r / p;
                        if ( i == "fill" ) {
                            u = e > o ? e : o
                        } else if ( i == "fit" ) {
                            u = e < o ? e : o
                        } else {
                            u = 1
                        }
                        a = parseInt( c * u, 10 ) - v;
                        l = parseInt( p * u, 10 ) - v;
                        t.attr({
                            width: a,
                            height: l
                        }).css({
                            width: a,
                            height: l
                        })
                    } else {
                        a = c - v;
                        l = p - v;
                        t.attr( "width", a ).attr( "height", l )
                    }
                    if ( s ) {
                        t.css({
                            "margin-left": Math.floor( ( n - a ) / 2 ),
                            "margin-top": Math.floor( ( r - l ) / 2 )
                        })
                    }
                };
                c.src = t.attr( "src" )
            }
            t.removeAttr( "height" ).removeAttr( "width" );
            if ( !this._az4( t.get( 0 ) ) ) {
                e( "<img />" ).load(function() {
                    u()
                }).attr( "src", t.attr( "src" ) )
            } else {
                u()
            }
        }
    },
    _az4: function( e ) {
        if ( e ) {
            if ( !e.complete ) {
                return false
            }
            if ( typeof e.naturalWidth != "undefined" && e.naturalWidth == 0 ) {
                return false
            }
        } else {
            return false
        }
        return true
    }
};
e.fn.royalSlider = function( n ) {
    return this.each(function() {
        var r = new t( e( this ), n );
        e( this ).data( "royalSlider", r )
    })
};
e.fn.royalSlider.defaults = {
    lockAxis: true,
    loop: false,
    preloadNearbyImages: true,
    imageScaleMode: "none",
    imageAlignCenter: false,
    keyboardNavEnabled: false,
    directionNavEnabled: true,
    directionNavAutoHide: false,
    hideArrowOnLastSlide: true,
    slideTransitionType: "move",
    slideTransitionSpeed: 400,
    slideTransitionEasing: "easeInOutSine",
    captionAnimationEnabled: true,
    captionShowEffects: [ "fade", "moveleft" ],
    captionMoveOffset: 20,
    captionShowSpeed: 400,
    captionShowEasing: "easeOutCubic",
    captionShowDelay: 200,
    controlNavEnabled: true,
    controlNavThumbs: false,
    controlNavThumbsNavigation: true,
    controlNavThumbsSpeed: 400,
    controlNavThumbsEasing: "easeInOutSine",
    slideshowEnabled: false,
    slideshowDelay: 7000,
    slideshowPauseOnHover: true,
    slideshowAutoStart: true,
    welcomeScreenEnabled: false,
    welcomeScreenShowSpeed: 500,
    minSlideOffset: 20,
    disableTranslate3d: false,
    removeCaptionsOpacityInIE8: false,
    startSlideIndex: 0,
    slideSpacing: 0,
    blockLinksOnDrag: true,
    nonDraggableClassEnabled: true,
    dragUsingMouse: true,
    autoScaleSlider: false,
    autoScaleSliderWidth: 960,
    autoScaleSliderHeight: 400,
    beforeSlideChange: function() {},
    afterSlideChange: function() {},
    beforeLoadStart: function() {},
    loadingComplete: function() {},
    allComplete: function() {}
};
e.fn.royalSlider.settings = {}
})( jQuery )

$( document ).ready(function() {

    if($('.produto').length){

        var brand = $("#bread a.brand");
        if(brand.length){
            brand.html('<img src="/arquivos/brand-'+ brand.attr("class").split(" ").pop() +'.jpg" alt="'+brand.text()+'" />');
        };

    };

    // Geral
    $( '.helperComplement' ).remove();
    // Verificando se o navegador ÃƒÂ© o firefox
    if ( $.browser.mozilla ) {
        $( 'body' ).addClass( 'firefox' );
    }
    // Verificando se o navegador ÃƒÂ© o IE
    if ( $.browser.msie ) {
        $( "body" ).addClass( "eie" );
    }
    if ( $.browser.msie && $.browser.version == "7.0" ) {
        $( "body" ).addClass( "ie7" );
    }
    if ( $.browser.msie && $.browser.version == "8.0" ) {
        $( "body" ).addClass( "ie8" );
    }
    if ( $.browser.msie && $.browser.version == "9.0" ) {
        $( "body" ).addClass( "ie9" );
    }
    if ( $.browser.msie && $.browser.version == "10.0" ) {
        $( "body" ).addClass( "ie10" );
    }

    $( '#footer_az .atendimento-area' ).click(function() {
        $( '.zopim' ).first().hide();
        $( '.zopim' ).last().show();
    });

    // Bread Crumb
    // Definindo variavel que sera adicionada no codigo
    var setinhaBread = '<span title="Divisao Seta" id="setinha_bread"></span>';
    // A todas as liÃ‚Â´s adiciona o span
    $( '.bread-crumb li' ).each(function() {
        $( this ).append( setinhaBread );
    })

    //header flutuante
    $( window ).scroll(function() {
        posScroll = $( document ).scrollTop();
        if ( posScroll >= 140 )
            $( '.floatingTopBar' ).fadeIn( 300 );
        else
            $( '.floatingTopBar' ).fadeOut( 300 );
    });
    //oculta header flutuante
    $( '.floatingTopBar .ocultar' ).click(function() {
        $( '.floatingTopBar' ).animate({
            top: "-93px"
        }, 1000 );
        $( '.floatingTopBar .ocultar' ).hide();
        $( '.floatingTopBar .exibir' ).show();
    })
    //exibe header flutuante
    $( '.floatingTopBar .exibir' ).click(function() {
        $( '.floatingTopBar' ).animate({
            top: "0px"
        }, 800 );
        $( '.floatingTopBar .exibir' ).hide();
        $( '.floatingTopBar .ocultar' ).show();
    })


    // Home
    if ( $( 'body.home' ).length > 0 ) {

        $( '.lista-abas li' ).mouseenter(function() {
            if ( $( this ).attr( 'class' ).indexOf( 'active' ) == -1 ) {
                $( this ).parents( '.abas' ).find( '.lista-abas li' ).removeClass( "active" );
                $( this ).addClass( "active" );
                $( this ).parents( '.abas' ).find( '.content-abas > div' ).hide();
                $( this ).parents( '.abas' ).find( '.content-abas > div' ).eq( $( this ).index() ).fadeIn();
            } else {
                return false;
            }
        })

        $( '.abas' ).mouseleave(function( argument ) {
            $( this ).find( '.lista-abas li' ).removeClass( "active" );
            $( this ).find( '.lista-abas li.faixa-de-preco' ).addClass( "active" );
            $( this ).find( '.content-abas > div' ).hide();
            $( this ).find( '.content-abas > div.faixa-de-preco' ).show();
        });

        // Inicializando Banner
        // $( '#banner-rotator' ).royalSlider({
        //     slideshowEnabled: true,
        //     slideshowDelay: 3000,
        //     slideTransitionSpeed: 1000,
        //     imageScaleMode: "fill",
        //     hideArrowOnLastSlide: true,
        //     slideSpacing: 20,
        //     arrowsNav: true,
        //     transitionType: 'fade'
        // });

            $('#banner-rotator').append('<a class="arrow setas right" href="#" style="height: 142px;width: 45px;display: table;margin-top: -255px;z-index: 50;position: relative;background: url(http://www.aazperfumes.com.br/arquivos/arrow_right.png)"></a><a class="arrow setas left disabled" href="#" style="height: 142px;width: 45px;display: table;margin-top: -255px;z-index: 50;position: relative;background: url(http://www.aazperfumes.com.br/arquivos/arrow_left.png)"></a>');
            $('#banner-rotator ul').cycle({
                fx: 'fade',
                speed: 1000,
                timeout: 3000,
                prev: '.setas.left',
                next: '.setas.right'
            });

        // Inicializando Carrossel de Marcas
        $( "#carrossel_marcas" ).jcarousel({
            wrap: 'circular'
        });
        setInterval(function() {
            $( '#carrossel_marcas .jcarousel-next' ).trigger( 'click' );
        }, 5000 );

        // Funcionamento das Abas
        $( '.conteudo_abas div.conteudo' ).hide();
        // Evento de Click
        $( '#abas_home ul li' ).click(function() {
            // Removendo configuracoes defaults
            $( '#abas_home ul li' ).removeClass( 'active' );
            $( '.conteudo_abas div.conteudo' ).hide();
            // Adicionando conteudo ativo
            var classe = $( this ).attr( 'class' );
            var seletor = '.conteudo_abas .' + classe;
            $( this ).addClass( 'active' );
            $( seletor ).fadeIn( 'fast' );
            return false;
        });

        // Definindo Feminino como padrao
        $( '#abas_home ul li.feminino' ).addClass( 'active' );
        $( '.conteudo_abas div.feminino' ).show( 'fast' );


    }

    if ( $( 'body.produto' ).length > 0 ) {
        $( 'body#produto.moreSku #sku_produto .skuList .portal-notify-me-ref .notifyme' ).each(function() {
            if ( $( this ).css( 'display' ) == 'block' ) {
                $( this ).hide();
                $( this ).parent().prepend( '<div class="btnAvise">Avise-me</div>' );
                $( this ).prepend( '<div class="close">x</div>' );
            }
        });
        $( 'body#produto.moreSku #sku_produto .skuList .portal-notify-me-ref .btnAvise' ).live( 'click', function() {
            $( 'body#produto.moreSku #sku_produto .skuList .portal-notify-me-ref .notifyme' ).fadeOut();
            $( this ).next().fadeIn();
        });
        $( 'body#produto.moreSku #sku_produto .skuList .portal-notify-me-ref .notifyme .close' ).live( 'click', function() {
            $( this ).parent().fadeOut();
        });

        // Produto Sku
        var profiteFixSku = ProfiteFixSku( "Tamanho", true );

        // Trocando nome do avalie
        $('#botoes_compartilhar_indique_avaliar #span_avaliacao strong').text('Já avaliou este produto?');



        /*$(document).ajaxStop(function(){
            if($('a.a_video_car').length == 0){
            // Add Classe aos spans da resenha
                $('.voteRatingBar').next('span').addClass('span_voto');

                // Adicionando video aos thumbs e criando evento de clique para que ele apareÃƒÂ§a no lugar da imagem do produto
                var liVideo = '<li><a class="a_video_car" href="#"><img src="/arquivos/video_produto.png"></a></li>';
                $('.produto ul.thumbs').append(liVideo);

                var varVideo = $('.produto .value-field').html();
                $('.produto #video_carrossel').html(varVideo);

                var open = false;
                $('a.a_video_car').click(function(){
                    if (open == true){
                        $('.produto #video_carrossel').fadeOut();
                        $('.produto #video_carrossel').html(varVideo);
                        open = false;
                    }
                    else if(open == false){
                        $('.produto #video_carrossel').fadeIn();
                        open = true;
                    }
                    return false;
                });
            }
        });*/

        if ( $( '.comprarFlutuante' ).find( 'a' ).length == 0 ) {
            $( '.comprarFlutuante' ).html( '<div class="escolher">Escolher produto</div>' );
        }
        $( '.conversorFlutuante .conteudoFlutuante .escolher' ).live( 'click', function() {
            $( "html, body" ).animate({
                scrollTop: 200
            }, "slow" );
        });

    }

    if ( $( 'body.productQuickView' ).length > 0 ) {

        // Produto Sku
        var profiteFixSku = ProfiteFixSku( "Tamanho", true );

    }

    if ( $( 'body.categoria' ).length > 0 ) {

        // Classes para auxilixar o CSS
        contadorSub = 1;
        $( '.sub' ).each(function() {
            $( this ).addClass( 'sub' + contadorSub );
            contadorSub++;
        });

        contadorResults = 1;
        $( '.searchResultsTime' ).each(function() {
            $( this ).addClass( 'p' + contadorResults );
            contadorResults++;
        });


        // Retirando lis vazias
        $( '.helperComplement' ).each(function() {
            $( this ).remove();
        });

        // Adicionando Carrossel

        if ( $( 'body#categoria' ).length > 0 ) {
            // Carrossel
            $( ".carrossel" ).jcarousel({
                scroll: 1
            });
        }



        // Adicionando classes a paginacao e em divs, para diferenciar a paginaÃƒÂ§ÃƒÂ£o do topo e a do footer
        var contli = 0;
        $( 'body.categoria #conteudo_produtos div.main .sub' ).each(function() {
            contli++;
            var classeLi = 'sub' + contli;
            $( this ).addClass( classeLi );
        });
        var contp = 0;
        $( 'body.categoria #conteudo_produtos div.main p.searchResultsTime' ).each(function() {
            contp++;
            var classeP = 'p' + contp;
            $( this ).addClass( classeP );
        });

    }

    if ( $( 'body.resultado-busca' ).length > 0 ) {

        var ref = window.location.search;

        var tem2 = ref.indexOf('fq=H:247');
        if(tem2 > 0){
            $('.main').prepend('<img src="/arquivos/BannerColecaoSuperSale.jpg" />');  
        };

        var tem2 = ref.indexOf('fq=H:238');
        if(tem2 > 0){
            $('.main').prepend('<img src="/arquivos/BannerColecaoDesconto.jpg" />');  
        };

        var tem3 = ref.indexOf('fq=H:237');
        if(tem3 > 0){
            $('.main').prepend('<img src="/arquivos/banner-colection237.jpg" />');  
        };

        var tem4 = ref.indexOf('fq=H:247');
        if(tem3 > 0){
            $('.main').prepend('<img src="/arquivos/Colecao_AZ_497_2.jpg" />');  
        };
        
        if ( $( '.busca-vazio' ).length > 0 ) {

            var termo = window.location.pathname;
            termo = termo.replace( '/', '' );
            termo = '<a title="' + termo + '" href="/' + termo + '">' + termo + '</a>';
            $( '.bread-crumb ul li' ).append( termo );

            var conteudoBuscaVazia = '<p>Sua busca não encontrou nenhum resultado. Mas, temos diversos produtos que podem ser do seu interesse. Confira abaixo.</p>';
            $( '.busca-vazio' ).html( conteudoBuscaVazia );
            $( '.prateleiras_resultado_de_busca_nao_encontrado, .side-nao-encontrada' ).fadeIn();
            $( '#sidebar_navigation .navigation' ).fadeOut();
        }
    }

    if ( $( 'body.central' ).length > 0 ) {
        $( '.central_interna a.abre-chat' ).click(function() {
            $( '.zopim' ).first().hide();
            $( '.zopim' ).last().show();
        });
    }

    if ( $( 'body.account' ).length > 0 ) {

        $( document ).ajaxStop(function() {

            gt_login('/login?ReturnUrl=%2faccount%2f','.l-conta');
            gt_login('/login?ReturnUrl=%2faccount%2forders','.l-pedidos');

            var statusNew = false;
            $( '.new-address-link a.address-update' ).click(function() {
                if ( statusNew == false ) {
                    $( '.modal #address-edit' ).fadeIn( 'fast' );
                    statusNew = true;
                } else if ( statusNew == true ) {
                    $( '.modal #address-edit' ).fadeOut( 'fast' );
                    statusNew = false;
                }
                return false;
            });
        });

        //Legado
        // Minha Conta - Modais
        var statusAlterarPerfil = false;
        $( 'body.account #content_abas a#edit-data-link' ).click(function() {
            if ( statusAlterarPerfil == false ) {
                $( '#editar-perfil' ).fadeIn( 'fast' );
                statusAlterarPerfil = true;
            } else if ( statusAlterarPerfil == true ) {
                $( '#editar-perfil' ).fadeOut( 'fast' );
                statusAlterarPerfil = false;
            }
        });
        $( '#editar-perfil .close' ).click(function() {
            $( '#editar-perfil' ).fadeOut( 'fast' );
            statusAlterarPerfil = false;
        });

        var statusEndRemove = false;
        $( 'body.account p.edit-address-link a.delete' ).click(function() {
            if ( statusEndRemove == false ) {
                $( '#address-remove' ).fadeIn( 'fast' );
                statusEndRemove = true;
            } else if ( statusEndRemove == true ) {
                $( '#address-remove' ).fadeOut( 'fast' );
                statusEndRemove = false;
            }
        });
        $( '#address-remove .close' ).click(function() {
            $( '#address-remove' ).fadeOut( 'fast' );
            statusEndRemove = false;
        });

        var statusEndUpdate = false;
        $( 'body.account p.new-address-link a.address-update' ).click(function() {
            if ( statusEndUpdate == false ) {
                $( '#address-edit' ).fadeIn( 'fast' );
                statusEndUpdate = true;
            } else if ( statusEndUpdate == true ) {
                $( '#address-edit' ).fadeOut( 'fast' );
                statusEndUpdate = false;
            }
        });
        $( '#address-edit .close' ).click(function() {
            $( '#address-edit' ).fadeOut( 'fast' );
            statusEndUpdate = false;
        });
        //fim legado

        // var statusEdit = false;
        // $('.edit-profile-link a#edit-data-link').click(function(){
        //  if (statusEdit == false) {
        //      $('.modal #address-edit').fadeIn('fast');
        //      statusEdit = true;
        //  }
        //  else if(statusEdit == true){
        //      $('.modal #address-edit').fadeOut('fast');
        //      statusEdit = false;
        //  }
        // });
    }
    

    // REVELAR E ESCONDER O CONVERSOR FLUTUANTE NA PAGINA DE PRODUTO
    $( window ).scroll(function() {
        posScroll = $( document ).scrollTop();
        if ( posScroll >= 625 )
            $( '.conversorFlutuante' ).fadeIn( 300 );
        else
            $( '.conversorFlutuante' ).fadeOut( 300 );
    });


    // VER COMPLETO NO QUICK VIEW
    $( 'body.productQuickView .productName' ).append( '<a href="" target="_parent" class="visCompleta">Ver visualização completa</a>' );
    var urlquickview = location.search.split( "/" );
    $( '.visCompleta' ).attr( 'href', '/produto-completo/' + urlquickview + '' );



    // ESCONDER ITENS DA SIDEBAR
    $( '.categoria #sidebar_navigation h5' ).each(function() {
        var quantLabel = $( this ).next( 'ul' ).children( 'li' ).length;
        //console.log(quantLabel);
        if ( quantLabel > 6 ) {
            $( this ).append( '<span class="mais">+</span>' );
            $( this ).next( 'ul' ).css( 'height', '160px' ).css( 'overflow', 'hidden' );
        } else {
            $( this ).next( 'ul' ).css( 'height', 'auto' );
        }
    });

    $( '.categoria #sidebar_navigation h5' ).click(function() {
        var valorSpan = $( this ).find( 'span' ).html();
        if ( valorSpan === '+' ) {
            $( this ).next( 'ul' ).css( 'height', 'auto' );
            $( this ).find( 'span' ).removeClass().addClass( 'menos' ).html( '-' );
        } else {
            $( this ).next().css( 'height', '160px' );
            $( this ).find( 'span' ).removeClass().addClass( 'mais' ).html( '+' );
        }
    });

    if ( $( '#produto.moreSku #info_produto #info_sku #sku_produto .skuList' ).length > 0 ) {
        $( '#produto.moreSku #info_produto #info_sku #info_gray' ).hide();
    }

});

$( window ).load(function() {

    gt_login('/login?ReturnUrl=%2faccount%2f','.l-conta');
    gt_login('/login?ReturnUrl=%2faccount%2forders','.l-pedidos');

    if($('body.home').length){

        //abre o popup de paypal
        var refEnd = window.location.search;
        var tem = refEnd.indexOf('paypal');

        if(tem>1){
            abrePopupNewz('paypal'); 
        }else{
            //seta o cookie para fazer o modal de newsletter
            var REcookie = docCookies.getItem(name="jaCadastrou");
            if(REcookie != 1){
                setTimeout(function(){
                   abrePopupNewz('normal'); 
                },15000);
            };
        };

    };


    // AJUSTES NO CONVERSOR DA PAGINA DE PRODUTO
    if ( $( 'body' ).hasClass( 'productQuickView' ) ) {
        var pegaValorDivido = $( 'body.productQuickView .valor-dividido.price-installments > span, body.produto .comprarFlutuante .valor-dividido.price-installments > span' ).html().replace( 'ou', '<span class="ouAte">ou atÃƒÂ© </span>' );
        $( 'body.productQuickView .valor-dividido.price-installments span, body.produto .comprarFlutuante .valor-dividido.price-installments span' ).html( pegaValorDivido );
    }
});

$( 'document' ).ready(function() {

    $( '.bread-crumb ul li' ).eq( 0 ).find( 'a' ).html( 'Home' );

    $( '.menu-departamento h4 a, .menu-departamento ul li a' ).each(function() {
        var qtd = '';
        var nome = '';
        qtd = $( this ).html();
        if ( /\(/.test( qtd ) ) {
            qtd = qtd.split( '(' );
            nome = qtd[ 0 ];
            qtd = qtd[ 1 ];
            qtd = qtd.split( ')' );
            qtd = qtd[ 0 ];
            $( this ).html( nome );
        }
    });

    //CHAT ONLINE
    $('body').append('<div id="livezilla_tracking" style="display:none"><script src="http://atendimento.sistemadechat.com.br/server.php?ws=YXpwZXJmdW1lcy5jb20uYnI_&rqst=track&output=jcrpt&ovlp=MjI_&ovlc=IzAwODAwMA__&ovlct=I2ZmZmZmZg__&ovlt=RmFsZSBjb20gQVogUGVyZnVtZXM_&ovlto=RGVpeGUgc3VhIG1lbnNhZ2VtIQ__&eca=MQ__&ecw=Mjg1&ech=OTU_&ecmb=Mjk_&echt=RmFsZSBjb20gYSBBWiBQZXJmdW1lcw__&echst=QXRlbmRpbWVudG8gZW0gdGVtcG8gcmVhbA__&ecoht=RmFsZSBjb20gYSBBWiBQZXJmdW1lcw__&ecohst=RGVpeGUgdW1hIG1lbnNhZ2Vt&ecfs=IzAwODQ0Mg__&ecfe=IzAwQTQwMA__&echc=I0Q3RkZENw__&ecslw=Mg__&ecsgs=IzY1OUYyQQ__&ecsge=IzY1OUYyQQ__&nse=546" type="text/javascript" async></script></div><noscript><img src="http://atendimento.sistemadechat.com.br/server.php?ws=YXpwZXJmdW1lcy5jb20uYnI_&amp;rqst=track&amp;output=nojcrpt&ovlp=MjI_&ovlc=IzAwODAwMA__&ovlct=I2ZmZmZmZg__&ovlt=RmFsZSBjb20gQVogUGVyZnVtZXM_&ovlto=RGVpeGUgc3VhIG1lbnNhZ2VtIQ__&eca=MQ__&ecw=Mjg1&ech=OTU_&ecmb=Mjk_&echt=RmFsZSBjb20gYSBBWiBQZXJmdW1lcw__&echst=QXRlbmRpbWVudG8gZW0gdGVtcG8gcmVhbA__&ecoht=RmFsZSBjb20gYSBBWiBQZXJmdW1lcw__&ecohst=RGVpeGUgdW1hIG1lbnNhZ2Vt&ecfs=IzAwODQ0Mg__&ecfe=IzAwQTQwMA__&echc=I0Q3RkZENw__&ecslw=Mg__&ecsgs=IzY1OUYyQQ__&ecsge=IzY1OUYyQQ__" width="0" height="0" style="visibility:hidden;" alt="" /></noscript>');

    $( 'body#departamento #sidebar_navigation h5' ).each(function() {
        if ( $( this ).text() == 'Marca+' ) {
            $( this ).next( 'ul' ).css( 'overflow', 'auto' );
        }
    });

    $( document ).ajaxStart(function() {

        $( '.v2-vtexsc-cart.vtexsc-cart.mouseActivated.preLoaded' ).hide();

    });

    $( document ).ajaxStop(function() {

        if($('body.home').length){

            if($('fieldset.success').length > 0){
                docCookies.setItem("jaCadastrou", 1);
            };

        };

        $( '.v2-vtexsc-cart.vtexsc-cart.mouseActivated.preLoaded' ).hide();

    });

    /*function  alteraWelcome() {

        var welcome = $('.welcome').text(); //    Ola nome-do-usuario /
        var usuario = welcome.split('OlÃƒÂ¡')[1]; //   nome-do-usuario


        //Reconstroi a estrutura
        $('.welcome').html( 'OlÃƒÂ¡, ' + '<span>' + usuario + '</span>'  + '<a href="/no-cache/user/logout">NÃƒÂ£o ÃƒÂ© vocÃƒÂª?</a>' );

        //estiliza os elementos
        $('.welcome span').css('font-weight', 'bold');
        $('.welcome a').css({'color': 'gray', 'font-style': 'normal'})

    }

    alteraWelcome();*/

    if ( $( 'body' ).hasClass( 'home' ) ) {

        $( '.conteudo_abas .prateleira' ).each(function() {
            if ( $( this ).find( 'li' ).length >= 4 ) {
                $( this ).jcarousel({
                    wrap: 'circular',
                    scroll: 1,
                });
            }
        });
    }
});

$( document ).ajaxStop(function() {

    if ($( '.welcome' ).find( '#login' ).length === 0) {
        $( '.welcome' ).find( 'a' ).html( ' / Sair' ).show();
    }

    if ( $( 'body' ).hasClass( 'thumb-color' ) ) {
        if ( $( '#sku_produto ul li label' ).eq( 0 ).hasClass( 'item_unavailable' ) ) {
            $( '.botao, #qtd_produto' ).hide();
        }
    } else {
        if ( $( '.notifyme' ).css( 'display' ) == 'block' ) {
            $( '#info_gray>div' ).show();
            $( '#sku_preco, #botao_comprar, #qtd_produto' ).hide();
        }
    }
});
//Ajustes QA 18-08-2014 Up Menu
$( document ).ready(function() {

    if($('body.home').length){

        $('.news-cookie .fechar').click(function(){
            docCookies.setItem("jaCadastrou", 1);
        });

        //insere o cookie quando clica no submit e retira se entrar na pagina de erro
        $('#btCadastrar').click(function(){
            docCookies.setItem("jaCadastrou", 1);
        });
        
    };

    var site = window.location.pathname;
        if(site.indexOf('error') > 0){
            docCookies.removeItem("jacadastrou");
        };

    if ( $( 'body.departamento' ).length > 0 ) {

        if ( $( '.bread-crumb li' ).eq( 1 ).find( 'a' ).html() === "Beleza" ) {
            $( ".even:last-child" ).prependTo( ".menu-departamento" );
            $( "h5" ).prependTo( ".menu-departamento" );
        }
    }
    if ( $( 'body.categoria' ).length > 0 ) {

        if ( $( '.bread-crumb li' ).eq( 1 ).find( 'a' ).html() === "Beleza" ) {
            $( '.Hide.HideMarca + ul' ).prependTo( ".menu-departamento" );
            $( '.Hide.HideMarca' ).prependTo( ".menu-departamento" );
        }
    }
    if($('body.produto').length > 0) {

        var alt = $('.brandName img').attr('alt');
        var lk = $('.brandName a').attr('href');
        $('.brandName a').attr('href','/'+alt);

        if($('#product-gift-wrapper ul li').length > 0){
            $('#product-gift-wrapper').show();
        }
        else{
            $('#product-gift-wrapper').addClass('gift-no-product');   
            $('#product-gift-wrapper').hide();   
        }
    }
    //sidebar scroll marca
    if($('#sidebar_navigation h5').length > 0){
        $('#sidebar_navigation h5').each(function() {
            if($(this).text() === 'Marcas' || $(this).text() === 'Marca'){
                $(this).next('ul').addClass('marca-scroll');
                return true;
            }
        });
    } 
    if($('#sidebar_navigation .side-nao-encontrada h3').length > 0){
        $('#sidebar_navigation .side-nao-encontrada h3').each(function() {
            if($(this).text() === 'Marcas' || $(this).text() === 'Marca'){
                $(this).next('ul').addClass('marca-scroll');
                return true;
            }
        });
    }

    $(document).ajaxComplete(function() {
        $(".vitrine li").each(function() {
            if ($(this).find(".flags p.brinde-de-produto").length > 0) {
                if (!$(this).hasClass('brinde')) {
                    $(this).addClass("brinde");
                    $(this).find(".price").hide();
                    $(this).find(".buy-button-normal").hide();
                }
            }
        })
    });
});



/*!
 * jQuery Cycle Plugin (with Transition Definitions)
 * Examples and documentation at: http://jquery.malsup.com/cycle/
 * Copyright (c) 2007-2010 M. Alsup
 * Version: 2.9999 (13-NOV-2011)
 * Dual licensed under the MIT and GPL licenses.
 * http://jquery.malsup.com/license.html
 * Requires: jQuery v1.3.2 or later
 * Compressed by Carlos (twitter.com/vs_Carlos)
 */
(function(e,c){function h(b){e.fn.cycle.debug&&a(b)}function a(){window.console&&console.log&&console.log("[cycle] "+Array.prototype.join.call(arguments," "))}function j(b,g,a){var d=e(b).data("cycle.opts"),c=!!b.cyclePause;c&&d.paused?d.paused(b,d,g,a):!c&&d.resumed&&d.resumed(b,d,g,a)}function i(b,g,k){function d(b,g,k){if(!b&&!0===g){b=e(k).data("cycle.opts");if(!b)return a("options not found, can not resume"),!1;if(k.cycleTimeout)clearTimeout(k.cycleTimeout),k.cycleTimeout=0;t(b.elements,b,1,
!b.backwards)}}if(b.cycleStop==c)b.cycleStop=0;if(g===c||null===g)g={};if(g.constructor==String)switch(g){case "destroy":case "stop":k=e(b).data("cycle.opts");if(!k)return!1;b.cycleStop++;b.cycleTimeout&&clearTimeout(b.cycleTimeout);b.cycleTimeout=0;k.elements&&e(k.elements).stop();e(b).removeData("cycle.opts");"destroy"==g&&l(k);return!1;case "toggle":return b.cyclePause=1===b.cyclePause?0:1,d(b.cyclePause,k,b),j(b),!1;case "pause":return b.cyclePause=1,j(b),!1;case "resume":return b.cyclePause=
0,d(!1,k,b),j(b),!1;case "prev":case "next":k=e(b).data("cycle.opts");if(!k)return a('options not found, "prev/next" ignored'),!1;e.fn.cycle[g](k);return!1;default:g={fx:g}}else if(g.constructor==Number){var i=g,g=e(b).data("cycle.opts");if(!g)return a("options not found, can not advance slide"),!1;if(0>i||i>=g.elements.length)return a("invalid slide index: "+i),!1;g.nextSlide=i;if(b.cycleTimeout)clearTimeout(b.cycleTimeout),b.cycleTimeout=0;if("string"==typeof k)g.oneTimeFx=k;t(g.elements,g,1,i>=
g.currSlide);return!1}return g}function d(b,g){if(!e.support.opacity&&g.cleartype&&b.style.filter)try{b.style.removeAttribute("filter")}catch(a){}}function l(b){b.next&&e(b.next).unbind(b.prevNextEvent);b.prev&&e(b.prev).unbind(b.prevNextEvent);if(b.pager||b.pagerAnchorBuilder)e.each(b.pagerAnchors||[],function(){this.unbind().remove()});b.pagerAnchors=null;b.destroy&&b.destroy(b)}function n(b,g,k,i,z){var q,f=e.extend({},e.fn.cycle.defaults,i||{},e.metadata?b.metadata():e.meta?b.data():{}),h=e.isFunction(b.data)?
b.data(f.metaAttr):null;h&&(f=e.extend(f,h));if(f.autostop)f.countdown=f.autostopCount||k.length;var l=b[0];b.data("cycle.opts",f);f.$cont=b;f.stopCount=l.cycleStop;f.elements=k;f.before=f.before?[f.before]:[];f.after=f.after?[f.after]:[];!e.support.opacity&&f.cleartype&&f.after.push(function(){d(this,f)});f.continuous&&f.after.push(function(){t(k,f,0,!f.backwards)});m(f);!e.support.opacity&&f.cleartype&&!f.cleartypeNoBg&&r(g);"static"==b.css("position")&&b.css("position","relative");f.width&&b.width(f.width);
f.height&&"auto"!=f.height&&b.height(f.height);f.startingSlide!=c?(f.startingSlide=parseInt(f.startingSlide,10),f.startingSlide>=k.length||0>f.startSlide?f.startingSlide=0:q=!0):f.startingSlide=f.backwards?k.length-1:0;if(f.random){f.randomMap=[];for(h=0;h<k.length;h++)f.randomMap.push(h);f.randomMap.sort(function(){return Math.random()-0.5});if(q)for(q=0;q<k.length;q++){if(f.startingSlide==f.randomMap[q])f.randomIndex=q}else f.randomIndex=1,f.startingSlide=f.randomMap[1]}else if(f.startingSlide>=
k.length)f.startingSlide=0;f.currSlide=f.startingSlide||0;var o=f.startingSlide;g.css({position:"absolute",top:0,left:0}).hide().each(function(b){b=f.backwards?o?b<=o?k.length+(b-o):o-b:k.length-b:o?b>=o?k.length-(b-o):o-b:k.length-b;e(this).css("z-index",b)});e(k[o]).css("opacity",1).show();d(k[o],f);f.fit&&(f.aspect?g.each(function(){var b=e(this),g=!0===f.aspect?b.width()/b.height():f.aspect;f.width&&b.width()!=f.width&&(b.width(f.width),b.height(f.width/g));f.height&&b.height()<f.height&&(b.height(f.height),
b.width(f.height*g))}):(f.width&&g.width(f.width),f.height&&"auto"!=f.height&&g.height(f.height)));f.center&&(!f.fit||f.aspect)&&g.each(function(){var b=e(this);b.css({"margin-left":f.width?(f.width-b.width())/2+"px":0,"margin-top":f.height?(f.height-b.height())/2+"px":0})});f.center&&!f.fit&&!f.slideResize&&g.each(function(){var b=e(this);b.css({"margin-left":f.width?(f.width-b.width())/2+"px":0,"margin-top":f.height?(f.height-b.height())/2+"px":0})});if(f.containerResize&&!b.innerHeight()){for(var n=
h=q=0;n<k.length;n++){var v=e(k[n]),w=v[0],u=v.outerWidth(),x=v.outerHeight();u||(u=w.offsetWidth||w.width||v.attr("width"));x||(x=w.offsetHeight||w.height||v.attr("height"));q=u>q?u:q;h=x>h?x:h}0<q&&0<h&&b.css({width:q+"px",height:h+"px"})}var B=!1;f.pause&&b.hover(function(){B=!0;this.cyclePause++;j(l,!0)},function(){B&&this.cyclePause--;j(l,!0)});if(!1===s(f))return!1;var C=!1;i.requeueAttempts=i.requeueAttempts||0;g.each(function(){var b=e(this);this.cycleH=f.fit&&f.height?f.height:b.height()||
this.offsetHeight||this.height||b.attr("height")||0;this.cycleW=f.fit&&f.width?f.width:b.width()||this.offsetWidth||this.width||b.attr("width")||0;if(b.is("img")){var b=e.browser.mozilla&&34==this.cycleW&&19==this.cycleH&&!this.complete,g=e.browser.opera&&(42==this.cycleW&&19==this.cycleH||37==this.cycleW&&17==this.cycleH)&&!this.complete,k=0==this.cycleH&&0==this.cycleW&&!this.complete;if(e.browser.msie&&28==this.cycleW&&30==this.cycleH&&!this.complete||b||g||k){if(z.s&&f.requeueOnImageNotLoaded&&
100>++i.requeueAttempts)return a(i.requeueAttempts," - img slide not loaded, requeuing slideshow: ",this.src,this.cycleW,this.cycleH),setTimeout(function(){e(z.s,z.c).cycle(i)},f.requeueTimeout),C=!0,!1;a("could not determine size of image: "+this.src,this.cycleW,this.cycleH)}}return!0});if(C)return!1;f.cssBefore=f.cssBefore||{};f.cssAfter=f.cssAfter||{};f.cssFirst=f.cssFirst||{};f.animIn=f.animIn||{};f.animOut=f.animOut||{};g.not(":eq("+o+")").css(f.cssBefore);e(g[o]).css(f.cssFirst);if(f.timeout){f.timeout=
parseInt(f.timeout,10);if(f.speed.constructor==String)f.speed=e.fx.speeds[f.speed]||parseInt(f.speed,10);f.sync||(f.speed/=2);for(q="none"==f.fx?0:"shuffle"==f.fx?500:250;f.timeout-f.speed<q;)f.timeout+=f.speed}if(f.easing)f.easeIn=f.easeOut=f.easing;if(!f.speedIn)f.speedIn=f.speed;if(!f.speedOut)f.speedOut=f.speed;f.slideCount=k.length;f.currSlide=f.lastSlide=o;if(f.random){if(++f.randomIndex==k.length)f.randomIndex=0;f.nextSlide=f.randomMap[f.randomIndex]}else f.nextSlide=f.backwards?0==f.startingSlide?
k.length-1:f.startingSlide-1:f.startingSlide>=k.length-1?0:f.startingSlide+1;if(!f.multiFx)if(q=e.fn.cycle.transitions[f.fx],e.isFunction(q))q(b,g,f);else if("custom"!=f.fx&&!f.multiFx)return a("unknown transition: "+f.fx,"; slideshow terminating"),!1;b=g[o];f.skipInitializationCallbacks||(f.before.length&&f.before[0].apply(b,[b,b,f,!0]),f.after.length&&f.after[0].apply(b,[b,b,f,!0]));f.next&&e(f.next).bind(f.prevNextEvent,function(){return y(f,1)});f.prev&&e(f.prev).bind(f.prevNextEvent,function(){return y(f,
0)});(f.pager||f.pagerAnchorBuilder)&&A(k,f);D(f,k);return f}function m(b){b.original={before:[],after:[]};b.original.cssBefore=e.extend({},b.cssBefore);b.original.cssAfter=e.extend({},b.cssAfter);b.original.animIn=e.extend({},b.animIn);b.original.animOut=e.extend({},b.animOut);e.each(b.before,function(){b.original.before.push(this)});e.each(b.after,function(){b.original.after.push(this)})}function s(b){var g,k,d=e.fn.cycle.transitions;if(0<b.fx.indexOf(",")){b.multiFx=!0;b.fxs=b.fx.replace(/\s*/g,
"").split(",");for(g=0;g<b.fxs.length;g++){var i=b.fxs[g];k=d[i];if(!k||!d.hasOwnProperty(i)||!e.isFunction(k))a("discarding unknown transition: ",i),b.fxs.splice(g,1),g--}if(!b.fxs.length)return a("No valid transitions named; slideshow terminating."),!1}else if("all"==b.fx)for(p in b.multiFx=!0,b.fxs=[],d)k=d[p],d.hasOwnProperty(p)&&e.isFunction(k)&&b.fxs.push(p);if(b.multiFx&&b.randomizeEffects){k=Math.floor(20*Math.random())+30;for(g=0;g<k;g++)d=Math.floor(Math.random()*b.fxs.length),b.fxs.push(b.fxs.splice(d,
1)[0]);h("randomized fx sequence: ",b.fxs)}return!0}function D(b,g){b.addSlide=function(a,d){var i=e(a),c=i[0];b.autostopCount||b.countdown++;g[d?"unshift":"push"](c);if(b.els)b.els[d?"unshift":"push"](c);b.slideCount=g.length;b.random&&(b.randomMap.push(b.slideCount-1),b.randomMap.sort(function(){return Math.random()-0.5}));i.css("position","absolute");i[d?"prependTo":"appendTo"](b.$cont);d&&(b.currSlide++,b.nextSlide++);!e.support.opacity&&b.cleartype&&!b.cleartypeNoBg&&r(i);b.fit&&b.width&&i.width(b.width);
b.fit&&b.height&&"auto"!=b.height&&i.height(b.height);c.cycleH=b.fit&&b.height?b.height:i.height();c.cycleW=b.fit&&b.width?b.width:i.width();i.css(b.cssBefore);(b.pager||b.pagerAnchorBuilder)&&e.fn.cycle.createPagerAnchor(g.length-1,c,e(b.pager),g,b);if(e.isFunction(b.onAddSlide))b.onAddSlide(i);else i.hide()}}function t(b,g,a,d){function i(){var a=0;g.timeout&&!g.continuous?(a=u(b[g.currSlide],b[g.nextSlide],g,d),"shuffle"==g.fx&&(a-=g.speedOut)):g.continuous&&j.cyclePause&&(a=10);if(0<a)j.cycleTimeout=
setTimeout(function(){t(b,g,0,!g.backwards)},a)}if(a&&g.busy&&g.manualTrump)h("manualTrump in go(), stopping active transition"),e(b).stop(!0,!0),g.busy=0;if(g.busy)h("transition active, ignoring new tx request");else{var j=g.$cont[0],f=b[g.currSlide],l=b[g.nextSlide];if(!(j.cycleStop!=g.stopCount||0===j.cycleTimeout&&!a))if(!a&&!j.cyclePause&&!g.bounce&&(g.autostop&&0>=--g.countdown||g.nowrap&&!g.random&&g.nextSlide<g.currSlide))g.end&&g.end(g);else{var n=!1;if((a||!j.cyclePause)&&g.nextSlide!=g.currSlide){var n=
!0,o=g.fx;f.cycleH=f.cycleH||e(f).height();f.cycleW=f.cycleW||e(f).width();l.cycleH=l.cycleH||e(l).height();l.cycleW=l.cycleW||e(l).width();if(g.multiFx){if(d&&(g.lastFx==c||++g.lastFx>=g.fxs.length))g.lastFx=0;else if(!d&&(g.lastFx==c||0>--g.lastFx))g.lastFx=g.fxs.length-1;o=g.fxs[g.lastFx]}if(g.oneTimeFx)o=g.oneTimeFx,g.oneTimeFx=null;e.fn.cycle.resetState(g,o);g.before.length&&e.each(g.before,function(b,a){j.cycleStop==g.stopCount&&a.apply(l,[f,l,g,d])});var m=function(){g.busy=0;e.each(g.after,
function(b,a){j.cycleStop==g.stopCount&&a.apply(l,[f,l,g,d])});j.cycleStop||i()};h("tx firing("+o+"); currSlide: "+g.currSlide+"; nextSlide: "+g.nextSlide);g.busy=1;if(g.fxFn)g.fxFn(f,l,g,m,d,a&&g.fastOnEvent);else if(e.isFunction(e.fn.cycle[g.fx]))e.fn.cycle[g.fx](f,l,g,m,d,a&&g.fastOnEvent);else e.fn.cycle.custom(f,l,g,m,d,a&&g.fastOnEvent)}else i();if(n||g.nextSlide==g.currSlide)if(g.lastSlide=g.currSlide,g.random){g.currSlide=g.nextSlide;if(++g.randomIndex==b.length)g.randomIndex=0,g.randomMap.sort(function(){return Math.random()-
0.5});g.nextSlide=g.randomMap[g.randomIndex];if(g.nextSlide==g.currSlide)g.nextSlide=g.currSlide==g.slideCount-1?0:g.currSlide+1}else g.backwards?(a=0>g.nextSlide-1)&&g.bounce?(g.backwards=!g.backwards,g.nextSlide=1,g.currSlide=0):(g.nextSlide=a?b.length-1:g.nextSlide-1,g.currSlide=a?0:g.nextSlide+1):(a=g.nextSlide+1==b.length)&&g.bounce?(g.backwards=!g.backwards,g.nextSlide=b.length-2,g.currSlide=b.length-1):(g.nextSlide=a?0:g.nextSlide+1,g.currSlide=a?b.length-1:g.nextSlide-1);n&&g.pager&&g.updateActivePagerLink(g.pager,
g.currSlide,g.activePagerClass)}}}function u(b,a,e,d){if(e.timeoutFn){for(b=e.timeoutFn.call(b,b,a,e,d);"none"!=e.fx&&250>b-e.speed;)b+=e.speed;h("calculated timeout: "+b+"; speed: "+e.speed);if(!1!==b)return b}return e.timeout}function y(b,a){var d=a?1:-1,i=b.elements,c=b.$cont[0],j=c.cycleTimeout;if(j)clearTimeout(j),c.cycleTimeout=0;if(b.random&&0>d){b.randomIndex--;if(-2==--b.randomIndex)b.randomIndex=i.length-2;else if(-1==b.randomIndex)b.randomIndex=i.length-1;b.nextSlide=b.randomMap[b.randomIndex]}else if(b.random)b.nextSlide=
b.randomMap[b.randomIndex];else if(b.nextSlide=b.currSlide+d,0>b.nextSlide){if(b.nowrap)return!1;b.nextSlide=i.length-1}else if(b.nextSlide>=i.length){if(b.nowrap)return!1;b.nextSlide=0}c=b.onPrevNextEvent||b.prevNextClick;e.isFunction(c)&&c(0<d,b.nextSlide,i[b.nextSlide]);t(i,b,1,a);return!1}function A(b,a){var d=e(a.pager);e.each(b,function(i,c){e.fn.cycle.createPagerAnchor(i,c,d,b,a)});a.updateActivePagerLink(a.pager,a.startingSlide,a.activePagerClass)}function r(b){function a(b){b=parseInt(b,
10).toString(16);return 2>b.length?"0"+b:b}function d(b){for(;b&&"html"!=b.nodeName.toLowerCase();b=b.parentNode){var i=e.css(b,"background-color");if(i&&0<=i.indexOf("rgb"))return b=i.match(/\d+/g),"#"+a(b[0])+a(b[1])+a(b[2]);if(i&&"transparent"!=i)return i}return"#ffffff"}h("applying clearType background-color hack");b.each(function(){e(this).css("background-color",d(this))})}if(e.support==c)e.support={opacity:!e.browser.msie};e.expr[":"].paused=function(b){return b.cyclePause};e.fn.cycle=function(b,
g){var d={s:this.selector,c:this.context};if(0===this.length&&"stop"!=b){if(!e.isReady&&d.s)return a("DOM not ready, queuing slideshow"),e(function(){e(d.s,d.c).cycle(b,g)}),this;a("terminating; zero elements found by selector"+(e.isReady?"":" (DOM not ready)"));return this}return this.each(function(){var c=i(this,b,g);if(!1!==c){c.updateActivePagerLink=c.updateActivePagerLink||e.fn.cycle.updateActivePagerLink;this.cycleTimeout&&clearTimeout(this.cycleTimeout);this.cycleTimeout=this.cyclePause=0;
var j=e(this),l=c.slideExpr?e(c.slideExpr,this):j.children(),f=l.get(),m=n(j,l,f,c,d);if(!1!==m)if(2>f.length)a("terminating; too few slides: "+f.length);else if(j=m.continuous?10:u(f[m.currSlide],f[m.nextSlide],m,!m.backwards))j+=m.delay||0,10>j&&(j=10),h("first timeout: "+j),this.cycleTimeout=setTimeout(function(){t(f,m,0,!c.backwards)},j)}})};e.fn.cycle.resetState=function(b,a){a=a||b.fx;b.before=[];b.after=[];b.cssBefore=e.extend({},b.original.cssBefore);b.cssAfter=e.extend({},b.original.cssAfter);
b.animIn=e.extend({},b.original.animIn);b.animOut=e.extend({},b.original.animOut);b.fxFn=null;e.each(b.original.before,function(){b.before.push(this)});e.each(b.original.after,function(){b.after.push(this)});var d=e.fn.cycle.transitions[a];e.isFunction(d)&&d(b.$cont,e(b.elements),b)};e.fn.cycle.updateActivePagerLink=function(b,a,d){e(b).each(function(){e(this).children().removeClass(d).eq(a).addClass(d)})};e.fn.cycle.next=function(b){y(b,1)};e.fn.cycle.prev=function(b){y(b,0)};e.fn.cycle.createPagerAnchor=
function(b,a,d,i,c){e.isFunction(c.pagerAnchorBuilder)?(a=c.pagerAnchorBuilder(b,a),h("pagerAnchorBuilder("+b+", el) returned: "+a)):a='<a href="#">'+(b+1)+"</a>";if(a){var l=e(a);if(0===l.parents("body").length){var f=[];1<d.length?(d.each(function(){var b=l.clone(!0);e(this).append(b);f.push(b[0])}),l=e(f)):l.appendTo(d)}c.pagerAnchors=c.pagerAnchors||[];c.pagerAnchors.push(l);d=function(a){a.preventDefault();c.nextSlide=b;var a=c.$cont[0],f=a.cycleTimeout;if(f)clearTimeout(f),a.cycleTimeout=0;
a=c.onPagerEvent||c.pagerClick;e.isFunction(a)&&a(c.nextSlide,i[c.nextSlide]);t(i,c,1,c.currSlide<b)};/mouseenter|mouseover/i.test(c.pagerEvent)?l.hover(d,function(){}):l.bind(c.pagerEvent,d);!/^click/.test(c.pagerEvent)&&!c.allowPagerClickBubble&&l.bind("click.cycle",function(){return!1});var m=c.$cont[0],n=!1;c.pauseOnPagerHover&&l.hover(function(){n=!0;m.cyclePause++;j(m,!0,!0)},function(){n&&m.cyclePause--;j(m,!0,!0)})}};e.fn.cycle.hopsFromLast=function(b,a){var e=b.lastSlide,d=b.currSlide;return a?
d>e?d-e:b.slideCount-e:d<e?e-d:e+b.slideCount-d};e.fn.cycle.commonReset=function(b,a,d,c,i,j){e(d.elements).not(b).hide();if("undefined"==typeof d.cssBefore.opacity)d.cssBefore.opacity=1;d.cssBefore.display="block";if(d.slideResize&&!1!==c&&0<a.cycleW)d.cssBefore.width=a.cycleW;if(d.slideResize&&!1!==i&&0<a.cycleH)d.cssBefore.height=a.cycleH;d.cssAfter=d.cssAfter||{};d.cssAfter.display="none";e(b).css("zIndex",d.slideCount+(!0===j?1:0));e(a).css("zIndex",d.slideCount+(!0===j?0:1))};e.fn.cycle.custom=
function(b,a,d,c,i,j){var f=e(b),h=e(a),l=d.speedIn,b=d.speedOut,m=d.easeIn,a=d.easeOut;h.css(d.cssBefore);j&&(l="number"==typeof j?b=j:b=1,m=a=null);var n=function(){h.animate(d.animIn,l,m,function(){c()})};f.animate(d.animOut,b,a,function(){f.css(d.cssAfter);d.sync||n()});d.sync&&n()};e.fn.cycle.transitions={fade:function(b,a,d){a.not(":eq("+d.currSlide+")").css("opacity",0);d.before.push(function(b,a,d){e.fn.cycle.commonReset(b,a,d);d.cssBefore.opacity=0});d.animIn={opacity:1};d.animOut={opacity:0};
d.cssBefore={top:0,left:0}}};e.fn.cycle.ver=function(){return"2.9999"};e.fn.cycle.defaults={activePagerClass:"activeSlide",after:null,allowPagerClickBubble:!1,animIn:null,animOut:null,aspect:!1,autostop:0,autostopCount:0,backwards:!1,before:null,center:null,cleartype:!e.support.opacity,cleartypeNoBg:!1,containerResize:1,continuous:0,cssAfter:null,cssBefore:null,delay:0,easeIn:null,easeOut:null,easing:null,end:null,fastOnEvent:0,fit:0,fx:"fade",fxFn:null,height:"auto",manualTrump:!0,metaAttr:"cycle",
next:null,nowrap:0,onPagerEvent:null,onPrevNextEvent:null,pager:null,pagerAnchorBuilder:null,pagerEvent:"click.cycle",pause:0,pauseOnPagerHover:0,prev:null,prevNextEvent:"click.cycle",random:0,randomizeEffects:1,requeueOnImageNotLoaded:!0,requeueTimeout:250,rev:0,shuffle:null,skipInitializationCallbacks:!1,slideExpr:null,slideResize:1,speed:1E3,speedIn:null,speedOut:null,startingSlide:c,sync:1,timeout:4E3,timeoutFn:null,updateActivePagerLink:null,width:null}})(jQuery);
(function(e){e.fn.cycle.transitions.none=function(c,h,a){a.fxFn=function(a,c,d,h){e(c).show();e(a).hide();h()}};e.fn.cycle.transitions.fadeout=function(c,h,a){h.not(":eq("+a.currSlide+")").css({display:"block",opacity:1});a.before.push(function(a,c,d,h,n,m){e(a).css("zIndex",d.slideCount+(!0===!m?1:0));e(c).css("zIndex",d.slideCount+(!0===!m?0:1))});a.animIn.opacity=1;a.animOut.opacity=0;a.cssBefore.opacity=1;a.cssBefore.display="block";a.cssAfter.zIndex=0};e.fn.cycle.transitions.scrollUp=function(c,
h,a){c.css("overflow","hidden");a.before.push(e.fn.cycle.commonReset);c=c.height();a.cssBefore.top=c;a.cssBefore.left=0;a.cssFirst.top=0;a.animIn.top=0;a.animOut.top=-c};e.fn.cycle.transitions.scrollDown=function(c,h,a){c.css("overflow","hidden");a.before.push(e.fn.cycle.commonReset);c=c.height();a.cssFirst.top=0;a.cssBefore.top=-c;a.cssBefore.left=0;a.animIn.top=0;a.animOut.top=c};e.fn.cycle.transitions.scrollLeft=function(c,h,a){c.css("overflow","hidden");a.before.push(e.fn.cycle.commonReset);c=
c.width();a.cssFirst.left=0;a.cssBefore.left=c;a.cssBefore.top=0;a.animIn.left=0;a.animOut.left=0-c};e.fn.cycle.transitions.scrollRight=function(c,h,a){c.css("overflow","hidden");a.before.push(e.fn.cycle.commonReset);c=c.width();a.cssFirst.left=0;a.cssBefore.left=-c;a.cssBefore.top=0;a.animIn.left=0;a.animOut.left=c};e.fn.cycle.transitions.scrollHorz=function(c,h,a){c.css("overflow","hidden").width();a.before.push(function(a,c,d,h){d.rev&&(h=!h);e.fn.cycle.commonReset(a,c,d);d.cssBefore.left=h?c.cycleW-
1:1-c.cycleW;d.animOut.left=h?-a.cycleW:a.cycleW});a.cssFirst.left=0;a.cssBefore.top=0;a.animIn.left=0;a.animOut.top=0};e.fn.cycle.transitions.scrollVert=function(c,h,a){c.css("overflow","hidden");a.before.push(function(a,c,d,h){d.rev&&(h=!h);e.fn.cycle.commonReset(a,c,d);d.cssBefore.top=h?1-c.cycleH:c.cycleH-1;d.animOut.top=h?a.cycleH:-a.cycleH});a.cssFirst.top=0;a.cssBefore.left=0;a.animIn.top=0;a.animOut.left=0};e.fn.cycle.transitions.slideX=function(c,h,a){a.before.push(function(a,c,d){e(d.elements).not(a).hide();
e.fn.cycle.commonReset(a,c,d,!1,!0);d.animIn.width=c.cycleW});a.cssBefore.left=0;a.cssBefore.top=0;a.cssBefore.width=0;a.animIn.width="show";a.animOut.width=0};e.fn.cycle.transitions.slideY=function(c,h,a){a.before.push(function(a,c,d){e(d.elements).not(a).hide();e.fn.cycle.commonReset(a,c,d,!0,!1);d.animIn.height=c.cycleH});a.cssBefore.left=0;a.cssBefore.top=0;a.cssBefore.height=0;a.animIn.height="show";a.animOut.height=0};e.fn.cycle.transitions.shuffle=function(c,h,a){c=c.css("overflow","visible").width();
h.css({left:0,top:0});a.before.push(function(a,c,d){e.fn.cycle.commonReset(a,c,d,!0,!0,!0)});if(!a.speedAdjusted)a.speed/=2,a.speedAdjusted=!0;a.random=0;a.shuffle=a.shuffle||{left:-c,top:15};a.els=[];for(c=0;c<h.length;c++)a.els.push(h[c]);for(c=0;c<a.currSlide;c++)a.els.push(a.els.shift());a.fxFn=function(a,c,d,h,n){d.rev&&(n=!n);var m=n?e(a):e(c);e(c).css(d.cssBefore);var s=d.slideCount;m.animate(d.shuffle,d.speedIn,d.easeIn,function(){for(var c=e.fn.cycle.hopsFromLast(d,n),i=0;i<c;i++)n?d.els.push(d.els.shift()):
d.els.unshift(d.els.pop());if(n){c=0;for(i=d.els.length;c<i;c++)e(d.els[c]).css("z-index",i-c+s)}else c=e(a).css("z-index"),m.css("z-index",parseInt(c,10)+1+s);m.animate({left:0,top:0},d.speedOut,d.easeOut,function(){e(n?this:a).hide();h&&h()})})};e.extend(a.cssBefore,{display:"block",opacity:1,top:0,left:0})};e.fn.cycle.transitions.turnUp=function(c,h,a){a.before.push(function(a,c,d){e.fn.cycle.commonReset(a,c,d,!0,!1);d.cssBefore.top=c.cycleH;d.animIn.height=c.cycleH;d.animOut.width=c.cycleW});
a.cssFirst.top=0;a.cssBefore.left=0;a.cssBefore.height=0;a.animIn.top=0;a.animOut.height=0};e.fn.cycle.transitions.turnDown=function(c,h,a){a.before.push(function(a,c,d){e.fn.cycle.commonReset(a,c,d,!0,!1);d.animIn.height=c.cycleH;d.animOut.top=a.cycleH});a.cssFirst.top=0;a.cssBefore.left=0;a.cssBefore.top=0;a.cssBefore.height=0;a.animOut.height=0};e.fn.cycle.transitions.turnLeft=function(c,h,a){a.before.push(function(a,c,d){e.fn.cycle.commonReset(a,c,d,!1,!0);d.cssBefore.left=c.cycleW;d.animIn.width=
c.cycleW});a.cssBefore.top=0;a.cssBefore.width=0;a.animIn.left=0;a.animOut.width=0};e.fn.cycle.transitions.turnRight=function(c,h,a){a.before.push(function(a,c,d){e.fn.cycle.commonReset(a,c,d,!1,!0);d.animIn.width=c.cycleW;d.animOut.left=a.cycleW});e.extend(a.cssBefore,{top:0,left:0,width:0});a.animIn.left=0;a.animOut.width=0};e.fn.cycle.transitions.zoom=function(c,h,a){a.before.push(function(a,c,d){e.fn.cycle.commonReset(a,c,d,!1,!1,!0);d.cssBefore.top=c.cycleH/2;d.cssBefore.left=c.cycleW/2;e.extend(d.animIn,
{top:0,left:0,width:c.cycleW,height:c.cycleH});e.extend(d.animOut,{width:0,height:0,top:a.cycleH/2,left:a.cycleW/2})});a.cssFirst.top=0;a.cssFirst.left=0;a.cssBefore.width=0;a.cssBefore.height=0};e.fn.cycle.transitions.fadeZoom=function(c,h,a){a.before.push(function(a,c,d){e.fn.cycle.commonReset(a,c,d,!1,!1);d.cssBefore.left=c.cycleW/2;d.cssBefore.top=c.cycleH/2;e.extend(d.animIn,{top:0,left:0,width:c.cycleW,height:c.cycleH})});a.cssBefore.width=0;a.cssBefore.height=0;a.animOut.opacity=0};e.fn.cycle.transitions.blindX=
function(c,h,a){c=c.css("overflow","hidden").width();a.before.push(function(a,c,d){e.fn.cycle.commonReset(a,c,d);d.animIn.width=c.cycleW;d.animOut.left=a.cycleW});a.cssBefore.left=c;a.cssBefore.top=0;a.animIn.left=0;a.animOut.left=c};e.fn.cycle.transitions.blindY=function(c,h,a){c=c.css("overflow","hidden").height();a.before.push(function(a,c,d){e.fn.cycle.commonReset(a,c,d);d.animIn.height=c.cycleH;d.animOut.top=a.cycleH});a.cssBefore.top=c;a.cssBefore.left=0;a.animIn.top=0;a.animOut.top=c};e.fn.cycle.transitions.blindZ=
function(c,h,a){h=c.css("overflow","hidden").height();c=c.width();a.before.push(function(a,c,d){e.fn.cycle.commonReset(a,c,d);d.animIn.height=c.cycleH;d.animOut.top=a.cycleH});a.cssBefore.top=h;a.cssBefore.left=c;a.animIn.top=0;a.animIn.left=0;a.animOut.top=h;a.animOut.left=c};e.fn.cycle.transitions.growX=function(c,h,a){a.before.push(function(a,c,d){e.fn.cycle.commonReset(a,c,d,!1,!0);d.cssBefore.left=this.cycleW/2;d.animIn.left=0;d.animIn.width=this.cycleW;d.animOut.left=0});a.cssBefore.top=0;a.cssBefore.width=
0};e.fn.cycle.transitions.growY=function(c,h,a){a.before.push(function(a,c,d){e.fn.cycle.commonReset(a,c,d,!0,!1);d.cssBefore.top=this.cycleH/2;d.animIn.top=0;d.animIn.height=this.cycleH;d.animOut.top=0});a.cssBefore.height=0;a.cssBefore.left=0};e.fn.cycle.transitions.curtainX=function(c,h,a){a.before.push(function(a,c,d){e.fn.cycle.commonReset(a,c,d,!1,!0,!0);d.cssBefore.left=c.cycleW/2;d.animIn.left=0;d.animIn.width=this.cycleW;d.animOut.left=a.cycleW/2;d.animOut.width=0});a.cssBefore.top=0;a.cssBefore.width=
0};e.fn.cycle.transitions.curtainY=function(c,h,a){a.before.push(function(a,c,d){e.fn.cycle.commonReset(a,c,d,!0,!1,!0);d.cssBefore.top=c.cycleH/2;d.animIn.top=0;d.animIn.height=c.cycleH;d.animOut.top=a.cycleH/2;d.animOut.height=0});a.cssBefore.height=0;a.cssBefore.left=0};e.fn.cycle.transitions.cover=function(c,h,a){var j=a.direction||"left",i=c.css("overflow","hidden").width(),d=c.height();a.before.push(function(a,c,h){e.fn.cycle.commonReset(a,c,h);"right"==j?h.cssBefore.left=-i:"up"==j?h.cssBefore.top=
d:"down"==j?h.cssBefore.top=-d:h.cssBefore.left=i});a.animIn.left=0;a.animIn.top=0;a.cssBefore.top=0;a.cssBefore.left=0};e.fn.cycle.transitions.uncover=function(c,h,a){var j=a.direction||"left",i=c.css("overflow","hidden").width(),d=c.height();a.before.push(function(a,c,h){e.fn.cycle.commonReset(a,c,h,!0,!0,!0);"right"==j?h.animOut.left=i:"up"==j?h.animOut.top=-d:"down"==j?h.animOut.top=d:h.animOut.left=-i});a.animIn.left=0;a.animIn.top=0;a.cssBefore.top=0;a.cssBefore.left=0};e.fn.cycle.transitions.toss=
function(c,h,a){var j=c.css("overflow","visible").width(),i=c.height();a.before.push(function(a,c,h){e.fn.cycle.commonReset(a,c,h,!0,!0,!0);!h.animOut.left&&!h.animOut.top?e.extend(h.animOut,{left:2*j,top:-i/2,opacity:0}):h.animOut.opacity=0});a.cssBefore.left=0;a.cssBefore.top=0;a.animIn.left=0};e.fn.cycle.transitions.wipe=function(c,h,a){var j=c.css("overflow","hidden").width(),i=c.height();a.cssBefore=a.cssBefore||{};var d;a.clip&&(/l2r/.test(a.clip)?d="rect(0px 0px "+i+"px 0px)":/r2l/.test(a.clip)?
d="rect(0px "+j+"px "+i+"px "+j+"px)":/t2b/.test(a.clip)?d="rect(0px "+j+"px 0px 0px)":/b2t/.test(a.clip)?d="rect("+i+"px "+j+"px "+i+"px 0px)":/zoom/.test(a.clip)&&(c=parseInt(i/2,10),h=parseInt(j/2,10),d="rect("+c+"px "+h+"px "+c+"px "+h+"px)"));a.cssBefore.clip=a.cssBefore.clip||d||"rect(0px 0px 0px 0px)";var c=a.cssBefore.clip.match(/(\d+)/g),l=parseInt(c[0],10),n=parseInt(c[1],10),m=parseInt(c[2],10),s=parseInt(c[3],10);a.before.push(function(a,c,d){if(a!=c){var h=e(a),A=e(c);e.fn.cycle.commonReset(a,
c,d,!0,!0,!1);d.cssAfter.display="block";var r=1,b=parseInt(d.speedIn/13,10)-1;(function k(){var a=l?l-parseInt(r*(l/b),10):0,c=s?s-parseInt(r*(s/b),10):0,d=m<i?m+parseInt(r*((i-m)/b||1),10):i,e=n<j?n+parseInt(r*((j-n)/b||1),10):j;A.css({clip:"rect("+a+"px "+e+"px "+d+"px "+c+"px)"});r++<=b?setTimeout(k,13):h.css("display","none")})()}});e.extend(a.cssBefore,{display:"block",opacity:1,top:0,left:0});a.animIn={left:0};a.animOut={left:0}}})(jQuery);