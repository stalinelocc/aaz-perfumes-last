// Document : general . gutvalente@gmail.com 

var caminhoImg = '/arquivos/';
var mobile = 'nao';

if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
	mobile = 'sim'
}

var gt_lightbox = function(gdiv, gclasse) {
	$('html, body').animate({scrollTop: 0}, 1000);
 	wHeight  = $(document).height();
	$('body').prepend('<div class="gt-ext-modal" style="height:'+wHeight+'px"><div class="gt-int-modal '+gclasse+'"><a id="iClose" class="gt-close-modal">X</a>'+gdiv+'</div></div>');		
	$('body').on('click', '.gt-close-modal', function() {
		$('.gt-ext-modal').remove();
	});
}

var espiar = function(){
	$('body').on('click', '.espiar', function(){
		var idUrl = $(this).attr('rel');				
		htmlEspiar = '<iframe src="/quick-view/?idproduto='+idUrl+'&KeepThis=true" frameborder="0" scrolling="no"></iframe>'
		gt_lightbox(htmlEspiar, 'js-espiar');
		$('html, body').animate({ scrollTop: $('body').offset().top }, 800);
	});
}

var menu_drop = function() {	
	
	$('.no-mobile li.pai').hover(
    function() {
    	$('.nivel-1', this).fadeIn(0);
    },
    function() {
    	$('.nivel-1', this).fadeOut(0);
    });

    //all
    var newMenu = '';
    $('.gt-categoria a.pai').each(function() {

    	var link_name = $(this).text();
    	var link_url = $(this).attr('href');

    	newMenu = newMenu + '<li><a href="'+link_url+'">'+link_name+'</a></li>';
    	
    });
    $('.nav-user .menu-mobile ul').html(newMenu);

    $('.nav-user').on('click', 'li.click', function(){    	
		$('.sub', this).slideToggle('fast');
		$('a.first', this).toggleClass('ativo');
	});

	$('body').on('click', '.close-menu', function(){    	
		$('.nav-user').fadeOut('fast');
	});

	$('body').on('click', '.open-menu', function(){    	
		$('.nav-user').fadeIn('fast');
		$('html, body').animate({scrollTop: 0}, 1000);
	});

	//home
    $('.drop-mobile').on('click', 'li.pai', function(){    	
		$('.sub-mobile', this).slideToggle('fast');
		$('a.pai', this).toggleClass('ativo');
	});

	if (mobile === 'sim') {
		$('.gt-categoria li.pai a.pai').removeAttr('href');
		$('.gt-categoria').removeClass('no-mobile');
		$('.gt-categoria').addClass('drop-mobile');
		$('.nav-user li.open').removeClass('desk');
		$('.nav-user li.open').addClass('click');
	}




	
}

var burger = function() {
	$('body').on('click', '.burger', function(){
		$('.gt-categoria').slideToggle('fast');
		$(this).toggleClass('ativo');
	});
}

function FormatPrecoReais(num){
  num=num.toString().replace(/\$|\,/g,'');
  if(isNaN(num))num="0";
  sign=(num==(num=Math.abs(num)));
  num=Math.floor(num*100+0.50000000001);
  cents=num%100;
  num=Math.floor(num/100).toString();
  if(cents<10)cents="0"+cents;
  for(var i=0;i<Math.floor((num.length-(1+i))/3);i++)num=num.substring(0,num.length-(4*i+3))+'.'+num.substring(num.length-(4*i+3));
  return ((sign)?'':'-')+'R$ '+num+','+cents;
}

var config_departamento = function(){
	$('.orderBy:eq(0) option:eq(0)').html('Ordenar por');
	$('.bread-crumb a:eq(0)').html('Home');
	$('.menu-navegue a').removeAttr('href');

	$('body').on('click', '.show-filtro', function(){
		$('.menu-departamento').slideToggle('fast');
		$(this).toggleClass('ativo');
	});
	
};

var busca_vazio = function() {	
	//busca vazio
	if ($('.busca-vazio').length) {
		$('.gt-menu-departamento').remove();
		$('.bread-crumb ul').append('<li>Resultado de busca</li>');
		$('.gt-produtos').css('width','100%');
	}
}



var logo_marcas = function() {
	var nome_marca = $('.brandName a').text();
	$('.brandName a').append('<img id="imgmarca" src="'+caminhoImg+'marca-'+nome_marca+'.gif" alt="'+nome_marca+'" title="'+nome_marca+'" />');
	
	$('#imgmarca').error(function() {
		$('#imgmarca').remove();
		$('.brandName').addClass('somente-texto');
	});
}

var gt_sanfona = function() {
	$('.sanfona .show-content:eq(0)').addClass('ativo');
	$('.sanfona .aba:eq(0)').addClass('show');
	$('.sanfona .show-content:eq(0) a.item').removeClass('open-aba');
	$('.sanfona .aba:eq(0)').slideDown();
	
	$('body').on('click', '.open-aba', function() {
		
		var thisAba 	= '.a-'+$(this).attr('rel');
		var thisBotao	= '.b-'+$(this).attr('rel');
		$('.sanfona .show').slideUp(function(){
			$('.sanfona .show-content').removeClass('ativo');
			$(thisBotao).addClass('ativo');
			$('.sanfona .show-content a.item').addClass('open-aba');
			$('.sanfona .aba').removeClass('show');
			$(thisAba).addClass('show');
			
			$(thisBotao+' a.item').removeClass('open-aba');
			$(thisAba).slideDown();
			
		});
	});
	
}

//qtd produto
var qtd_produto = function() {
	//verifica estoque
	qtd_Estoque = $('a.buy-button').attr('style').toString();
	if (qtd_Estoque == 'display:block') {
	  	$('.m-qtd').show();
	} else {
		$('.giftlist-insertsku-wrapper').remove();
		$('.m-qtd').remove();
		$('#calculoFrete').hide();		
	}
	$('body').on('click', 'ul.topic li label', function() {
		$('.sku-selector-container').removeClass('selecione_antes');
	});
	
	//aumenta quantidade
	$('body').on('click', '.mais-qtd', function() {
		var qtd_Futura = parseInt($('.campo-qtd').val())+1;
		var url_Passada = $('a.buy-button').attr('href');
		var skuSelecionado = url_Passada.substr(0,3);
		
		if (skuSelecionado == '/ch') {
			url_Atual = url_Passada.split('&qty=');
			url_Futura = url_Atual[0]+'&qty='+qtd_Futura+'&seller=1&redirect=true&sc=1';		
			$('a.buy-button').attr('href',url_Futura);
			$('.campo-qtd').val(qtd_Futura);
			$('#calculoFrete .quantity input').val(qtd_Futura);
			$('.glis-selected-amount').html(qtd_Futura);
		} else {
			alert('Por favor, selecione o modelo desejado');
			$('.sku-selector-container').addClass('selecione_antes');
		}
	});	
	//remove qtd
	$('.menos-qtd').live('click', function() {		
		var campoMin = parseInt($('.campo-qtd').val());		
		if (campoMin > 1) {
			var qtd_Futura = parseInt($('.campo-qtd').val())-1;
			var url_Passada = $('a.buy-button').attr('href');
			var skuSelecionado = url_Passada.substr(0,3);
			
			if (skuSelecionado == '/ch') {
				url_Atual = url_Passada.split('&qty=');
				url_Futura = url_Atual[0]+'&qty='+qtd_Futura+'&seller=1&redirect=true&sc=1';		
				$('a.buy-button').attr('href',url_Futura);
				$('.campo-qtd').val(qtd_Futura);
				$('#calculoFrete .quantity input').val(qtd_Futura);
				$('.glis-selected-amount').html(qtd_Futura);
			} else {
				alert('Por favor, selecione o modelo desejado');
				$('.sku-selector-container').addClass('selecione_antes');
				
			}
		}
	});
}

//subir.topo
var voltar_topo = function() {
	$('body').on('click', '#voltartopo', function(){
		$('html, body').animate({scrollTop: 0}, 1000);
	});
	$('body').on('click', '.top-back a', function(){
		$('html, body').animate({scrollTop: 0}, 1000);
	});
};

//nav dicas
var nav_dicas = function() {
	$('.top-info').on('click', 'li a', function() {
		var goEscolha = $(this).attr('rel');
		$('html, body').animate({ scrollTop: ($('#'+goEscolha).offset().top)-120 }, 800);
	});
}

//login
var gt_login = function() {		
	if ($('p.welcome a#login').length > 0) {		
		$('.drop-user li:eq(0)').show();
	} else {
		$('.drop-user li:eq(0)').html('<a href="/no-cache/user/logout">SAIR</a>');
	}
}

var mini_Cart = function() {
	$('th.cartSkuQuantity').html('Qtd');
	$('.cartFooter a.cartCheckout').html('Finalizar pedido');
	var atualQtd = parseInt($('.minicart .amount-items-em:eq(0)').text());
	
	if (atualQtd == 0) {
		$('.vtexsc-cart').html('<p class="mini-cart-vazio">Você ainda não adicionou produtos em seu carrinho</p>');
	}
}

var config_produto = function() {
	$('.informacoes-produto .fn').removeClass('productName');
	$('.bread-crumb a:eq(0)').html('Home');
	if ($('.compre-junto table').length) {
		$('.junto').show();
	}
	if ( $('.skuList').length < 1) {
		$('.sku-buy').show();
	}
	$('.box-product').on('click', 'h3.ini a', function() {		
		var classOpen = $(this).attr('rel');
		$('.'+classOpen).slideToggle('fast');
		$(this).toggleClass('inativo');
	});
	$('.skuList').last().addClass('last');
	$('#lnkComprar').text('Compre Junto')

		
}

var imagem_marca = function(){
	var name_marca = $('.brandName a').text();
	var txt_marca = $('.brandName a').attr('class');
	var img_marca = txt_marca.replace('brand ','');
	var src_marca = caminhoImg+'titulo-'+img_marca+'.jpg';

	$('.img-marca .logo-marca').html('<img src="'+src_marca+'" alt="'+name_marca+'" />');
	$('.productDescription').prepend('<div class="marca-descricao"><img src="'+src_marca+'" id="imgmarca" alt="'+name_marca+'" /></div>');

	$('#imgmarca').error(function() {
		$('#imgmarca').remove();		
		$('.img-marca .logo-marca').html(name_marca);
		$('.img-marca .logo-marca').addClass('txt-marca');
	});
}



var gt_carrossel_thumb = function() {  
  
  $('.apresentacao #include').append('<div class="aviso-zoom">ZOOM</div>');
  
  var totalThumb = $('.thumbs li').length;
  var inicioThumb = 0;
  var finalThumb = totalThumb;
  var lastThumb = totalThumb+2;
  var thumbExibe = 0;
  var thumbEsconde = 0;
  
  if (totalThumb > 3) {
	  $('.thumbs li').hide();
	  $('.thumbs li:eq(0)').show();
	  $('.thumbs li:eq(1)').show();
	  $('.thumbs li:eq(2)').show();
	  $('.thumbs').prepend('<div class="gt_anterior" style="display:none">Anterior</div><div class="gt_proxima" style="display:block">Proxima</div>');
	  $('.thumbs').addClass('thumb-slider');
  }
  $('body').on('click', '.gt_proxima', function() {  
	  inicioThumb = inicioThumb + 1;
	  thumbEsconde = inicioThumb-1;
	  thumbExibe = inicioThumb+2;
	  $('.gt_anterior').show();
	  if (inicioThumb < (totalThumb-3)) {
		  $('.gt_proxima').show();
	  } else {
		  $('.gt_proxima').hide();
	  }
	  $('.thumbs li:eq('+thumbExibe+')').show(300);
	  $('.thumbs li:eq('+thumbEsconde+')').hide(300);
		  
  });
  $('body').on('click', '.gt_anterior', function() {  
	  inicioThumb = inicioThumb - 1;
	  thumbExibe = inicioThumb;
	  thumbEsconde = inicioThumb+3;	  
	  $('.gt_proxima').show();
	  if (inicioThumb <= 0) {
		  $(this).hide();
	  } else {
		  $(this).show();
	  }
	  $('.thumbs li:eq('+thumbExibe+')').show(300);
	  $('.thumbs li:eq('+thumbEsconde+')').hide(300);	
  }); 
  
  $('.thumbs').show();
  //$('.thumbs li').show();
}

var gt_carrossel_thumb_click = function(){
	$('body').on('click', '.skuList label', function() {
		$('.thumbs').hide();
		$('.gt_anterior').remove();
		$('.gt_proxima').remove();
		setTimeout(gt_carrossel_thumb, 1000);		
	});
}

var config_resenhas = function() {
	
	var vtex_comment = '';
	$('ul.resenhas ul.util').remove();
	$('.avaliacao ul.resenhas li').each(function() {
		console.log('teste')
		var nome_comment = $('p.dados strong', this).text();
		var txt_comment = $('.opt-texto', this).text();
		var rattin_comment = $('.rating-wrapper').html();
		if  (nome_comment === 'Opinião de: ') {
			console.log('sem nome');
			nome_comment = 'Nome não publicado';
		}

		vtex_comment = vtex_comment+'<div class="box-comentario"><div class="info-comentario"><span class="nome">'+nome_comment+'</span>'+rattin_comment+'</div> <div class="texto-comentario">'+txt_comment+'</div></div>'

	});
	$('#caracteristicas td.Comentarios').prepend(vtex_comment);
	$('.box-product').removeClass('load');
	
}

var config_filtro = function(){
	


	$('.left-nav ul').hide();
	$('.left-nav fieldset div').hide();
	$('.left-nav fieldset:eq(0) div').show();
	$('.left-nav fieldset.refino-marca div').show();
	$('.left-nav fieldset:eq(0) h5').addClass('ativo');
	$('.left-nav fieldset.refino-marca h5').addClass('ativo');

	$('.left-nav h4').last().addClass('last');
	$('.left-nav fieldset:eq(0)').prepend('<p class="titulo-filtro">Filtre sua busca</p>');

	$('body').on('click', '.search-single-navigator h5', function() {			
		$(this).toggleClass('ativo');
		$(this).next('ul').slideToggle('fast');
	});

	$('body').on('click', '.search-multiple-navigator h5', function() {			
		$(this).toggleClass('ativo');
		$(this).next('div').slideToggle('fast');
	});

    
    
}

var num_top10 = function(){
	var numrank = 0;
	
	$('.gt-top10 .rank').each(function() {
		add_zero = '';
		numrank = numrank+1;
		if (numrank < 10) {
			add_zero = '0'	
		}
		$(this).text(add_zero+numrank);
	});
}


$(document).ready(function() {	
	
	//document.domain = document.domain;	
	
	if ($('.resume-cart').length) {
		$('.resume-cart').minicart({ showMinicart: true, showTotalizers: true });
	}	
	
	$('.newsletter-button-ok').val('ENVIAR');
	menu_drop();
	voltar_topo();
	burger();	
	
	//home
	if ($('body.home').length) {
		
	}

	//top10
	if ($('body.top10').length) {
		num_top10();
	}
	
	//escolha
	if ($('body.como-escolher').length) {
		nav_dicas();
	}
	
	
	//category pages
	if ($('body.category').length) {
		config_departamento();
		busca_vazio();		
		espiar();		
		config_filtro();
		$(".navigation-tabs input[type='checkbox']").vtexSmartResearch({
            emptySearchMsg: '<h3>Esta combinação de filtros não retornou nenhum resultado!</h3>',
            filtersMenu: '.navigation-tabs'
        });
        $(".prateleira[id*=ResultItems]").QD_infinityScroll();
	}
	
	//product
	if ($('body.produto').length) {
		config_produto();
		imagem_marca();
		gt_carrossel_thumb();
		gt_carrossel_thumb_click();
		//show add resenhas
		$('body').on('click', '.mais-avaliacao', function() {		
			$('.user-review').slideToggle('fast');
		});
	}

	//institucional
	if ($('body.conteudo').length) {
		$('.bread-crumb ul').append('<li class="last">'+$('.pinst h2:eq(0) span').text()+'</li>');
	}
	
	//ajuda
	if ($('body.ajuda').length) {
		gt_sanfona();
	}
});

$(window).load(function() {
	
});

$(document).ajaxStop(function(){
	
	mini_Cart();
	gt_login();
	if ($('body.produto').length) {
		config_resenhas();
	}
});	

$(window).scroll(function(event) {
	if ($(this).scrollTop() > 100) {
        $('#voltartopo').fadeIn(200);
		$('body').addClass('top-fixed');
    } else {
        $('#voltartopo').fadeOut(200);
		$('body').removeClass('top-fixed');
		$('.gt-categoria').removeAttr('style');

    }	
	
});