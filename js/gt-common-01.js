var caminhoImg = '/arquivos/';
var mobile = 'nao';
var janela = innerWidth;
if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) & janela < 1004) {
	mobile = 'sim'
}
var gt_lightbox = function(gdiv, gclasse) {
	$('html, body').animate({
		scrollTop: 0
	}, 1000);
	wHeight = $(document).height();
	$('body').prepend('<div class="gt-ext-modal" style="height:' + wHeight + 'px"><div class="gt-int-modal ' + gclasse + '"><a id="iClose" class="gt-close-modal">X</a>' + gdiv + '</div></div>');
	$('body').on('click', '.gt-close-modal', function() {
		$('.gt-ext-modal').remove();
	});
}
var espiar = function() {
	$('body').on('click', '.espiar', function() {
		var idUrl = $(this).attr('rel');
		var add_image = $(this).parents('li').find('.fotoProduto a').html();
		console.log(add_image);
		htmlEspiar = '<div class="img-add">' + add_image + '</div><iframe src="https://aazperfumes.com.br/quick-view/?idproduto=' + idUrl + '&lid=5e4d8b09-8750-4ff9-ad94-8245a4284aad" frameborder="0" scrolling="no"></iframe>'
		gt_lightbox(htmlEspiar, 'jn-add');
	});
}
var open_sub = function() {
	var newMenu = '';
	$('.gt-categoria a.pai').each(function() {
		var link_name = $(this).text();
		var link_url = $(this).attr('href');
		newMenu = newMenu + '<li><a href="' + link_url + '">' + link_name + '</a></li>';
	});
	$('.menu-mobile ul').html(newMenu);
	$('body').on('click', '.burger', function() {
		$('.gt-categoria').slideToggle('fast');
		$(this).toggleClass('ativo');
	});
	var navTimers = [];
	$(".no-mobile li.pai").hover(function() {
		var id = jQuery.data(this);
		var $this = $(this);
		navTimers[id] = setTimeout(function() {
			$this.children('.nivel-1').fadeIn(300);
			navTimers[id] = "";
		}, 300);
	}, function() {
		var id = jQuery.data(this);
		if (navTimers[id] != "") {
			clearTimeout(navTimers[id]);
		} else {
			$(this).children(".nivel-1").fadeOut(200);
		}
	});
	if (mobile === 'sim') {
		$('.sub-open').addClass('click-open');
		$('.sub-open').removeAttr('href');
		$('.hover-open').removeClass('hover-open');
		$('.hover-open-2').removeClass('hover-open-2');
	}
	$('body').on('click', '.click-open-2', function() {
		$(this).next('.drop-2').slideToggle('fast');
		$(this).toggleClass('active');
	});
	$('body').on('click', '.click-open', function() {
		$('.drop').slideUp('fast');
		$('body .click-close').addClass('click-open');
		$('body .click-open').removeClass('click-close');
		$(this).addClass('click-close');
		$(this).removeClass('click-open');
		$(this).next('.drop').slideDown();
	});
	$('body').on('click', '.click-close', function() {
		$(this).removeClass('click-close');
		$(this).addClass('click-open');
		$(this).next('.drop').slideUp();
	});
	$('body').on('click', '.close-menu', function() {
		$('.open-menu').removeClass('click-close');
		$('.open-menu').addClass('click-open');
		$('.nav-user').slideUp();
	});

	$('body').on('click', '.simple-open', function(){
		var show_elem = $(this).attr('rel');
		$('.'+show_elem).slideToggle('fast');
		$(this).toggleClass('active');
		/*search*/
		if (show_elem == 'open-search' && $(this).hasClass('active')) {
			$('.fulltext-search-box').focus();
		}
	});

}

function FormatPrecoReais(num) {
	num = num.toString().replace(/\$|\,/g, '');
	if (isNaN(num)) num = "0";
	sign = (num == (num = Math.abs(num)));
	num = Math.floor(num * 100 + 0.50000000001);
	cents = num % 100;
	num = Math.floor(num / 100).toString();
	if (cents < 10) cents = "0" + cents;
	for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++) num = num.substring(0, num.length - (4 * i + 3)) + '.' + num.substring(num.length - (4 * i + 3));
		return ((sign) ? '' : '-') + 'R$ ' + num + ',' + cents;
}
var config_departamento = function() {
	$('.orderBy:eq(0) option:eq(0)').html('Ordenar por');
	$('.bread-crumb a:eq(0)').html('Home');
	$('.menu-navegue a').removeAttr('href');
	$(".show-filtro").click(function() {
		$(".esconder").slideToggle();
	});
};
var busca_vazio = function() {
	if ($('.busca-vazio').length) {
		$('.gt-menu-departamento').remove();
		$('.bread-crumb ul').append('<li>Resultado de busca</li>');
		$('.gt-produtos').css('width', '100%');
	}
}
var logo_marcas = function() {
	var nome_marca = $('.brandName a').text();
	$('.brandName a').append('<img id="imgmarca" src="' + caminhoImg + 'marca-' + nome_marca + '.gif" alt="' + nome_marca + '" title="' + nome_marca + '" />');
	$('#imgmarca').error(function() {
		$('#imgmarca').remove();
		$('.brandName').addClass('somente-texto');
	});
}
var gt_sanfona = function() {
	$('.sanfona .show-content:eq(0)').addClass('ativo');
	$('.sanfona .aba:eq(0)').addClass('show');
	$('.sanfona .show-content:eq(0) a.item').removeClass('open-aba');
	$('.sanfona .aba:eq(0)').slideDown();
	$('body').on('click', '.open-aba', function() {
		var thisAba = '.a-' + $(this).attr('rel');
		var thisBotao = '.b-' + $(this).attr('rel');
		$('.sanfona .show').slideUp(function() {
			$('.sanfona .show-content').removeClass('ativo');
			$(thisBotao).addClass('ativo');
			$('.sanfona .show-content a.item').addClass('open-aba');
			$('.sanfona .aba').removeClass('show');
			$(thisAba).addClass('show');
			$(thisBotao + ' a.item').removeClass('open-aba');
			$(thisAba).slideDown();
		});
	});
}
var voltar_topo = function() {
	$('body').on('click', '#voltartopo', function() {
		$('html, body').animate({
			scrollTop: 0
		}, 1000);
	});
	$('body').on('click', '.top-back a', function() {
		$('html, body').animate({
			scrollTop: 0
		}, 1000);
	});
};
var qtd_produto = function() {
	qtd_Estoque = $('a.buy-button').attr('style').toString();
	if (qtd_Estoque == 'display:block') {
		$('.m-qtd').show();
	} else {
		$('.m-qtd').remove();
		$('.b-frete').hide();
		$('.a-frete').hide();
	}
	$('.m-qtd').on('click', '.mais-qtd', function() {
		var qtd_Futura = parseInt($('.campo-qtd').val()) + 1;
		var url_Passada = $('a.buy-button').attr('href');
		var skuSelecionado = url_Passada.substr(0, 3);
		if (skuSelecionado == '/ch') {
			url_Atual = url_Passada.split('&qty=');
			url_Futura = url_Atual[0] + '&qty=' + qtd_Futura + '&seller=1&redirect=true&sc=1';
			$('a.buy-button').attr('href', url_Futura);
			$('.campo-qtd').val(qtd_Futura);
			$('#calculoFrete .quantity input').val(qtd_Futura);
		} else {
			alert('Por favor, selecione o modelo desejado');
		}
	});
	$('.m-qtd').on('click', '.menos-qtd', function() {
		var campoMin = parseInt($('.campo-qtd').val());
		if (campoMin > 1) {
			var qtd_Futura = parseInt($('.campo-qtd').val()) - 1;
			var url_Passada = $('a.buy-button').attr('href');
			var skuSelecionado = url_Passada.substr(0, 3);
			if (skuSelecionado == '/ch') {
				url_Atual = url_Passada.split('&qty=');
				url_Futura = url_Atual[0] + '&qty=' + qtd_Futura + '&seller=1&redirect=true&sc=1';
				$('a.buy-button').attr('href', url_Futura);
				$('.campo-qtd').val(qtd_Futura);
				$('#calculoFrete .quantity input').val(qtd_Futura);
			} else {
				alert('Por favor, selecione o modelo desejado');
			}
		}
	});
};
var nav_dicas = function() {
	$('.top-info').on('click', 'li a', function() {
		var goEscolha = $(this).attr('rel');
		$('html, body').animate({
			scrollTop: ($('#' + goEscolha).offset().top) - 120
		}, 800);
	});
}

var nav_marcas = function() {
	$('.filtro-marcas').on('click', 'a', function() {
		var letra = $(this).attr('rel');
		var letraFx = 147;
		if (janela < 1198) {
			letraFx = 0;
		}
		$('html, body').animate({
			scrollTop: ($('.' + letra).offset().top) - letraFx
		}, 800);
	});
}

var ch_buy = function() {
	ch_name = $('.informacoes-produto .fn:eq(0)').text();
	ch_brand = $('.informacoes-produto .brandName a').text();
	ch_image = $('.thumbs li:eq(0) a img').attr('src');
	$('.compra-fixa .cf-name').html(ch_name);
	$('.compra-fixa .cf-brand').html(ch_brand); 
	$('.compra-fixa .cf-image').html('<img src="'+ch_image+'" alt="'+ch_name+'" />'); 
	
	$('.cf-buy').on('click', 'a', function() {
		var br_fixo = 50;
		if (janela < 1198) {
			br_fixo = 0;
		}

		$('html, body').animate({
			scrollTop: ($('.informacoes-produto').offset().top) - br_fixo
		}, 800);
	});
}
var gt_login = function() {
	if ($('p.welcome a#login').length) {
		$('.drop-user').remove();
		$('.buttons li.user a.first').attr('id', 'login');
	} else {
		$('.drop-user').html('<li><a href="/account">Minha Conta</a></li><li><a href="/account/orders">Meus Pedidos</a></li><li><a href="/no-cache/user/logout">Sair</a></li>');
	}
	$('.buttons li.user').show();
}

var gt_login2 = function() {
	$('body').on('click', '.login-redir', function(){
		var redirPage = $(this).attr('rel');
		if ($('.my_login a#login').length) {

			$('html, body').animate({scrollTop: 0}, 0);
			vtexid.setScopeName(jsnomeSite);
			vtexid.start({
				returnUrl: redirPage,
				userEmail: '',
				locale: 'pt-BR',
				forceReload: false
			});

		} else {
			window.location.href = redirPage;
		}
	});
}

var mini_Cart = function() {
	$('th.cartSkuQuantity').html('Qtd');
	$('.cartFooter a.cartCheckout').html('Finalizar pedido');
	var atualQtd = parseInt($('.minicart .amount-items-em:eq(0)').text());
	if (atualQtd == 0) {
		$('.vtexsc-cart').html('<p class="mini-cart-vazio">Você ainda não adicionou produtos em seu carrinho</p>');
	}
}
var config_produto = function() {

	$('.informacoes-produto .fn').removeClass('productName');
	$('.bread-crumb a:eq(0)').html('Home');
	if ($('.compre-junto table').length) {
		$('.junto').show();
	}
	if ($('.skuList').length < 1) {
		$('.sku-buy').show();
	}
	$('.box-product').on('click', 'h3.ini a', function() {
		var classOpen = $(this).attr('rel');
		$('.' + classOpen).slideToggle('fast');
		$(this).toggleClass('inativo');
	});
	$('.skuList').last().addClass('last');
	$('#lnkComprar').text('Compre Junto')


}
var imagem_marca = function() {
	var name_marca = $('.brandName a').text();
	var txt_marca = $('.brandName a').attr('class');
	var img_marca = txt_marca.replace('brand ', '');
	var src_marca = caminhoImg + 'titulo-' + img_marca + '.jpg';
	$('.img-marca .logo-marca').html('<img src="' + src_marca + '" alt="' + name_marca + '" />');
	$('.productDescription').prepend('<div class="marca-descricao"><img src="' + src_marca + '" id="imgmarca" alt="' + name_marca + '" /></div>');
	$('#imgmarca').error(function() {
		$('#imgmarca').remove();
		$('.img-marca .logo-marca').html(name_marca);
		$('.img-marca .logo-marca').addClass('txt-marca');
	});

	setTimeout(function(){
		$(".logo-marca").removeAttr("href");
		$(".logo-marca").attr("href", "/"+img_marca);
	},3000);

}

var gt_carrossel_thumb = function() {  

	var totalThumb = $('.thumbs li').length;
	console.log(totalThumb);
	var inicioThumb = 0;
	var finalThumb = totalThumb;
	var lastThumb = totalThumb+2;
	var thumbExibe = 0;
	var thumbEsconde = 0;

	if (totalThumb > 3) {
		$('.thumbs li').hide();
		$('.thumbs li:eq(0)').show();
		$('.thumbs li:eq(1)').show();
		$('.thumbs li:eq(2)').show();
		$('.thumbs').prepend('<div class="gt_anterior" style="display:none">Anterior</div><div class="gt_proxima" style="display:block">Proxima</div>');
		$('.thumbs').addClass('thumb-slider');
	}
	$('body').on('click', '.gt_proxima', function() {  
		inicioThumb = inicioThumb + 1;
		thumbEsconde = inicioThumb-1;
		thumbExibe = inicioThumb+2;
		$('.gt_anterior').show();
		if (inicioThumb < (totalThumb-3)) {
			$('.gt_proxima').show();
		} else {
			$('.gt_proxima').hide();
		}
		$('.thumbs li:eq('+thumbExibe+')').show(300);
		$('.thumbs li:eq('+thumbEsconde+')').hide(300);

	});
	$('body').on('click', '.gt_anterior', function() {  
		inicioThumb = inicioThumb - 1;
		thumbExibe = inicioThumb;
		thumbEsconde = inicioThumb+3;	  
		$('.gt_proxima').show();
		if (inicioThumb <= 0) {
			$(this).hide();
		} else {
			$(this).show();
		}
		$('.thumbs li:eq('+thumbExibe+')').show(300);
		$('.thumbs li:eq('+thumbEsconde+')').hide(300);	
	});


	$('.thumbs').show();
}

var gt_carrossel_thumb_click = function(){
	$('body').on('click', '.skuList label', function() {
		$('.thumbs').hide();
		$('.gt_anterior').remove();
		$('.gt_proxima').remove();
		$('.thumbs').removeClass('thumb-slider');
		setTimeout(gt_carrossel_thumb, 1000);		
	});
}

var config_filtro = function() {
	$('.left-nav ul').hide();
	$('.left-nav fieldset div').hide();
	$('.left-nav fieldset:eq(0) div').show();
	$('.left-nav fieldset.refino-marca div').show();
	$('.left-nav fieldset:eq(0) h5').addClass('ativo');
	$('.left-nav fieldset.refino-marca h5').addClass('ativo');
	$('.left-nav h4').last().addClass('last');
	$('.left-nav fieldset:eq(0)').prepend('<p class="titulo-filtro">Filtre sua busca</p>');
	$('body').on('click', '.search-single-navigator h5', function() {
		$(this).toggleClass('ativo');
		$(this).next('ul').slideToggle('fast');
	});
	$('body').on('click', '.search-multiple-navigator h5', function() {
		$(this).toggleClass('ativo');
		$(this).next('div').slideToggle('fast');
	});
}
var num_top10 = function() {
	$('.top10 .content-top > div').each(function() {
		var numrank = 0;
		$(this).find('.rank').each(function() {
			var add_zero = '';
			numrank = numrank + 1;
			if (numrank < 10) {
				add_zero = '0'
			}
			$(this).text(add_zero + numrank);
		});
	});
}

var precoMenu = function(){

	$('#mmenu .all').append('<li class="pai"><a class="pai sub-open click-open">Preços</a><div class="nivel-1 sub drop" style="display: none;"><div class="nivel-2 precos"></div></div></a></li>');
	var tabelaPreco = $('.filtroPrecos');
	$('#mmenu .all .nivel-2.precos').append(tabelaPreco);
	$('.filtroPrecos:eq(1)').css('display', 'none');
}
var skuClick = function(){
	$('.skuList.item-dimension-Tamanho').each(function(){
		$(this).find("label:not(.item_unavailable):eq(0)").trigger("click").siblings('click');

		return false;

	});
}

var carrossel_colecao = function(elemWrap, numIni){
	$(elemWrap).slick({
		infinite: true,
		slidesToShow: numIni,
		slidesToScroll: 1,
		responsive: [			
		{
			breakpoint: 768,
			settings: {
				slidesToShow: 3,
				slidesToScroll: 1
			}
		},
		{
			breakpoint: 640,
			settings: {
				slidesToShow: 2,
				slidesToScroll: 1
			}
		}      	
		]
	});
}

$(document).ready(function() {
	gt_login2();
	if($('body.thumbcores ').length > 0){
		setTimeout(function(){
			$('div#TRUSTEDCOMPANY_widget_56').insertAfter('.informacoes-produto .box-product');
		},300);
	}

	if($('body.checkbox ').length > 0){
		setTimeout(function(){
			$('div#TRUSTEDCOMPANY_widget_56').insertAfter('.informacoes-produto .box-product');
		},300);
	}

	if($('body.new-marcas').length){
		nav_marcas();
		$('.best-brand ul').slick({
			infinite: true,
			slidesToShow: 6,
			slidesToScroll: 1,
			responsive: [			
			{
				breakpoint: 920,
				settings: {
					slidesToShow: 5,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 4,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 640,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 360,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}      	
			]
		});
	}
	

	setTimeout(function(){
		if($("body.produto").length > 0){
			// Alterando Para foto o item Cor
			$('body').append('<div class="variations" style="display:none"/>');
			vtexjs.catalog.getCurrentProductWithVariations().done(function(product){
				var obj = product.skus;
				var variacao = obj.length;

				$.each(obj, function(ind, obj){
					var img = product.skus[ind].image;
					var cor = product.skus[ind].dimensions["Cor"];
					$('.variations').append('<img src="'+img+'" cor="'+cor+'" />');
				});
			});

			$('.item-dimension-Cor span label').each(function(){
				var txt = $(this).text();
				var el = $('.variations').find('img[cor="'+txt+'"]:eq(0)').attr('src');
				$(this).css("background-image","url("+el+")");
			});
		}
	},400);


	// console.log('V51');
	if ($('.resume-cart').length) {
		$('.resume-cart').minicart({
			showMinicart: true,
			showTotalizers: true
		});
	}
	$('.newsletter-button-ok').val('ENVIAR');
	voltar_topo();
	espiar();
	open_sub();

	$('.flag.discount').each(function(){
		var flagPrice = $('span', this).text();
		flagPrice = flagPrice.split(',');
		$('span', this).text(flagPrice[0]);
		$(this).css('display','block');

	});

	if ($('body.home').length) {
		$('.helperComplement').remove();
		$('#TRUSTEDCOMPANY_widget_101382').append('<script async src="https://d3643s33l1rfut.cloudfront.net/js/widget?w=BQYHDBEcXkJYQ1hWS1RSW0VbXUU"></script>');
		console.log('v1');		
		$('.banner.posicao1').slick({
			infinite: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			autoplay: true,
			autoplaySpeed: 2000,
			speed: 1000,
			dots: true
		});
		carrossel_colecao('.collection.group-1 ul', 4);
		carrossel_colecao('.collection.group-2 ul', 4);

		/*
		$(".carrossel .prateleira ul").each(function() {
			var t = $(this).find('>li').length;
			if (t > 4) {
				$(this).flexisel({
					visibleItems: 4,
					animationSpeed: 500,
					autoPlay: false,
					clone: false,
					enableResponsiveBreakpoints: true,
					responsiveBreakpoints: {
						portrait: {
							changePoint: 480,
							visibleItems: 2
						},
						landscape: {
							changePoint: 640,
							visibleItems: 2
						},
						tablet: {
							changePoint: 768,
							visibleItems: 3
						}
					}
				});
			};
		});
		*/
		$('.video').on('click', function() {
			var rel = $(this).find('#rel').html();
			var htmlV = rel;
			gt_lightbox(htmlV, 'videoLB');
			return false;
		});
	}

	if ($('body.blackfriday-2016').length) {
		console.log('blackfriday');
		$('.lista-produtos .sub:eq(0)').prepend('<div class="bread-crumb"><ul><li><a title="AZPerfumes" href="/">Home</a></li><li class="last"><strong><a title="Black Friday">Black Friday</a></strong></li></ul></div>');
	}

	if ($('body.produto').length) {
		$('.helperComplement').remove();

		if ($('.flPromocao p.flag.entrega_45_dias').length) {
			$('.flPromocao p.flag.entrega_45_dias').html('<strong>PRODUTO EXCLUSIVO</strong> - Entrega diferenciada de <strong>30</strong> à <strong>45 dias</strong>');
			$('.flPromocao p.flag.entrega_45_dias').show();
		}

		if ($('.productPrice strong.skuBestPrice').length) {
			precoCheio = $('.productPrice strong.skuBestPrice').text();
			precoCheio = precoCheio.replace('R$','');
			precoCheio = precoCheio.replace('.','');
			precoCheio = precoCheio.replace(',','.');
			precoCheio = parseFloat(precoCheio);
			
			if(precoCheio > 150){
				$('.action-buy').addClass('fretegratis');
			}
			
		}

		carrossel_colecao('.collection.group-2 ul', 4);

		$('#TRUSTEDCOMPANY_widget_101382').append('<script async src="https://d3643s33l1rfut.cloudfront.net/js/widget?w=BQYHDBEcXkJYQ1hWS1RSW0VbXUU"></script>');
		
		$('.video').on('click', function() {
			var rel = $(this).find('#rel').html();
			var htmlV = rel;
			gt_lightbox(htmlV, 'videoLB');
			return false;
		});
	}


	if ($('.busca-vazio').length > 0) {
		var termo = window.location.pathname;
		termo = termo.replace('/', '');
		termo = '<a title="' + termo + '" href="/' + termo + '">' + termo + '</a>';
		$('.bread-crumb ul li').append(termo);
		var conteudoBuscaVazia = '<p>Sua busca não encontrou nenhum resultado. Mas, temos diversos produtos que podem ser do seu interesse. Confira abaixo.</p>';
		$('.busca-vazio').html(conteudoBuscaVazia);
		$('.prateleiras_resultado_de_busca_nao_encontrado, .side-nao-encontrada').fadeIn();
		$('.left-nav').fadeOut();
		$('.lista-produtos').addClass('vazio');
	}

	if ($('.didyoumean').length) {
		var termo = window.location.pathname;
		termo = termo.replace('/', '');
		termo = '<a title="' + termo + '" href="/' + termo + '">' + termo + '</a>';
		$('.bread-crumb ul li').append(termo);
		var conteudoBuscaVazia = '<p>Sua busca não encontrou nenhum resultado. Mas, temos diversos produtos que podem ser do seu interesse. Confira abaixo.</p>';
		$('.busca-vazio').html(conteudoBuscaVazia);
		$('.prateleiras_resultado_de_busca_nao_encontrado, .side-nao-encontrada').fadeIn();
		$('.left-nav').fadeOut();
		$('.lista-produtos').addClass('vazio');
	}

	if ($('body.top10').length) {
		num_top10();
		$('.menu-top li a').click(function() {
			var ref = $(this).attr('href');
			$('.menu-top li a').removeClass('ativo');
			$(this).addClass('ativo');
			$('.content-top > div').hide();
			$('.content-top > div' + ref).fadeIn();
			return false;
		});
	};
	if ($('body.como-escolher').length) {
		nav_dicas();
	}
	if ($('body.category').length) {
		config_departamento();
		busca_vazio();
		config_filtro();
		$(".resultado-busca-filtro:eq(0) .orderBy").insertBefore(".pager.top .pages");
	}

	if ($('body.resultado-busca').length) {

		//banners coleção
		var ref = window.location.search;
		ref = ref.split("H:");
		ref = ref[1];
		//inserindo banner
		$("#banners_colecao").find(".box-banner").each(function(ndx,item){
			var alt = $(item).find("img").attr("alt");
		});
		$("#bg_colecao").find(".box-banner").each(function(ndx,item){
			var alt = $(item).find("img").attr("alt");
			var imagem = $(item).find('>a').find('img').attr('src');
			if(alt==ref){
				console.log('entrou')
				$("#gcontent").css("background","url('"+imagem+"')");
				$('body').addClass('fixShelf');
			};
		});

		$('#banners_colecao').find('.box-banner').each(function(){
			var $this = $(this);
			$(this).find('img').each(function(){
				var altt = $(this).attr('alt');
				$(this).parents('.box-banner').attr('alt', altt);
				// console.log(this);
			});
		});
		$('.box-banner[alt= "'+ ref +'"]').insertBefore('.lista-produtos').show();

		if($('.box-marcas-resize-novo').length > 0){
			$('.left-nav').addClass('SP');
		} else{
			$('.left-nav').addClass('CP');
		}

	};

	if ($('body.como-escolher').length) {
		$('#olfativa .tags a').click(function() {
			var ref = $(this).attr('href');
			$('#olfativa .tags a').removeClass('ativo');
			$(this).addClass('ativo');
			$('#olfativa #conteudo-tabs > div').hide();
			$('#olfativa #conteudo-tabs > div' + ref).css('display', 'table-cell');
			return false;
		});
		$('#estacao .tags a').click(function() {
			var ref = $(this).attr('href');
			$('#estacao .tags a').removeClass('ativo');
			$(this).addClass('ativo');
			$('#estacao #conteudo-tabs2 > div').hide();
			$('#estacao #conteudo-tabs2 > div' + ref).css('display', 'table-cell');
			return false;
		});
	};
	if ($('body.produto').length) {
		ch_buy();
		$('.box-product h3.ini > a').click(function(){
			if($('#login').length > 0){
				var novaURL = "/login";
				$(window.document.location).attr('href',novaURL);
			}else{
				console.log('você pode avaliar este produto');
			}
		});

		setTimeout(function(){

			if($('.box-product.load').length > 0){
				$('.box-product.load').removeClass('load');
				$('.box.comentarios').css("display","block");

			}
		},500);
		setTimeout(function(){
			var url_avalie1 = $("#rdoInteresse").attr("onclick");
			url_avalie1 = url_avalie1.replace("http", "https");
			$("#opcoes-avalie input").attr("onclick", url_avalie1);

			var url_avalie2 = $("#lnkPubliqueResenha").attr("href");
			url_avalie2 = url_avalie2.replace("http", "https");
			$("#lnkPubliqueResenha").attr("href", url_avalie2);

		},600);

		config_produto();
		if (document.location.href.indexOf('?idsku=') > 0){
			console.log('Tem SKU');
		}else{
			skuClick();
		}
		imagem_marca();
		gt_carrossel_thumb();
		gt_carrossel_thumb_click();
		$('body').on('click', '.mais-avaliacao', function() {
			$('.user-review').slideToggle('fast');
		});
		if ($('.brinde_controle ul > li').length < 1) {
			$('.brinde_controle').hide();
		};
		$('<span class="msg-juros"> sem juros</span>').insertAfter('.valor-dividido.price-installments strong');
		$('.trustedcompany-widget').append("<script>(function(){document.getElementById('trustedcompany-widget').src='//trustedcompany.com/embed/widget/v2?domain=aazperfumes.com.br&type=d&review=1&text=a';})();</script>")
		$('#TRUSTEDCOMPANY_widget_101381').append('<script async src="https://d3643s33l1rfut.cloudfront.net/js/widget?w=BQYHDBEcXkJYQ1hWS1RSW0VbXUY"></script>');
		var lk = $('.brandName a').attr('href');
		$('a.logo-marca').attr('href', lk);

		var notifyme = $('.portal-notify-me-ref');
		notifyme.insertAfter('.sku-buy');

	// if($('.flags .entrega_45_dias').length){
	// 	var html45dias = "<div id='raridadesEntrega45dias'><p><strong>PRODUTO EXCLUSIVO</strong> - Entrega diferenciada de <strong>30</strong> à <strong>45 dias</strong></p></div>";
	// 	$(html45dias).insertAfter('.portal-notify-me-ref');
	// }

	//$('.brinde_controle').insertBefore('.flPromocao');



};
if ($('body.produto.checkbox, body.produto.thumbcores').length) {
	qtd_produto();
};
if ($('body.conteudo').length) {
	$('.bread-crumb ul').append('<li class="last">' + $('.pinst h2:eq(0) span').text() + '</li>');
};
if ($('body.ajuda').length) {
	gt_sanfona();
};

/*
if($('body.home').length > 0){
	$('.f_precos').prepend('<a href=""></a>');
	$('.f_precos:eq(0) a').attr("href", "/de-0,5-a-50?PS=30&map=priceFrom");
	$('.f_precos:eq(1) a').attr("href", "/de-50-a-100?PS=30&map=priceFrom");
	$('.f_precos:eq(2) a').attr("href", "/de-100-a-150?PS=30&map=priceFrom");
	$('.f_precos:eq(3) a').attr("href", "/de-150-a-200?PS=30&map=priceFrom");
	$('.f_precos:eq(4) a').attr("href", "/de-200-a-250?PS=30&map=priceFrom");
};
*/

// if($('body.blackfriday-2016').length > 0){
// 	$('.lista-produtos .gt-vitrine li').each(function(){
// 		$(this).find('.flags').append('<span class="flag blackfriday"></s>');
// 	});
// };

});

$(document).ajaxStop(function() {
	mini_Cart();
	//gt_login();
	
	if ($('body.produto').length) {
		// config_resenhas();
	}
	if ($('body.home').length) {
		$('.helperComplement').remove();
	};
	if($('body.blackfriday-2016').length > 0){
		$('.lista-produtos .gt-vitrine li').each(function(){
			$(this).find('.flags').append('<span class="flag blackfriday"></s>');
		});
	};

	$('.prateleira ul').find('li').each(function(){
		if($(this).hasClass('flagAdicionada') != true){
			$(this).addClass('flagAdicionada');
		}else{
			$(this).removeClass('flagAdicionada');
		};
	});
	$('.prateleira ul').find('li.flagAdicionada').each(function(){
		var esse = $(this);
		var economiaBoleto = "";
		var precoTotal = "";
		var ecomomiaBoleto2 = "";
		var precoTotal =  esse.find('span.preco-por').text();

		precoTotal = precoTotal.split("R$ ");
		precoTotal = precoTotal[1];
		precoTotal = parseInt(precoTotal);

		var total = precoTotal;
		if(total > 150){
			esse.find('.show-buy').append('<span class="freteGratis"></span>');
		}

	});


});
$(window).scroll(function(event) {
	if ($(this).scrollTop() > 192) {
		$('#voltartopo').fadeIn(200);
		$('body').addClass('top-fixed');
	} else {
		$('#voltartopo').fadeOut(200);
		$('body').removeClass('top-fixed');
		$('.gt-categoria').removeAttr('style');
		$('.burger').removeClass('ativo');
		$('#header').removeAttr('style');
	}


	if(janela < 1030 & $('.nivel-2.precos').length == 0 ){
		precoMenu();
	}
	if(janela >1030 & $('.nivel-2.precos').length == 1 ){
		$('#mmenu .all .nivel-2.precos').remove();
		$('.filtroPrecos:eq(1)').css('display', 'block');
	}

	if ($(this).scrollTop() > 193) {
		$('.top-fixed #header').fadeIn('slow');
	}

	if ($(this).scrollTop() > 590) {
		$('body').addClass('buy-fixed');
	} else {
		$('body').removeClass('buy-fixed');
		$('.compra-fixa').removeAttr('style');
	}

	if ($(this).scrollTop() > 591) {
		$('.buy-fixed .compra-fixa').fadeIn('slow');
	}

});