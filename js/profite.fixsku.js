// var profiteFixSku = ProfiteFixSku("Metal, Pedra, Tamanho", true);


function ProfiteFixSku(topics, debug)
{
	String.prototype.trim=function(){return this.replace(/^\s+|\s+$/g, '');};

	// var defaultSettings = objConfig;
	var _isDebug = typeof(debug) != "undefined" ? debug : false;
	var listSkusContainer = topics;
	var countAjaxComplete = 0;

	if(_isDebug)
	{
		console.log("_isDebug");
		var urlWindow = window.location.href.toString();
		if(urlWindow.indexOf("develop") > 0)
		{
			$(document).ajaxComplete(managerSkuChoice);
			$(document).ready(initManagerProduct);		
		}		
	}else
	{
		$(document).ajaxComplete(managerSkuChoice);
		$(document).ready(initManagerProduct);		
	}

	function desmarkAll()
	{
		$(".skuList").each(function(){
		    var containerList = $(this);
		    $(this).find(".sku-selector").each(function(){
		        var id = $(this).attr("id");
		        if(containerList.find("label").eq($(this).index()).hasClass("sku-picked")) console.log("desmarkthis");
		    });
		});
	}

	function cleanListItens()
	{
		if(listSkusContainer.indexOf(",") > -1)
		{
			listSkusContainer = listSkusContainer.split(",");

			$.each(listSkusContainer, function(index, value){
				listSkusContainer[index] = listSkusContainer[index].trim();
			});
		}else
		{
			var valSingleItem = listSkusContainer;
			listSkusContainer = new Array();
			listSkusContainer[0] = valSingleItem;
		}

	}

	function initManagerProduct()
	{
		$(".sku-selector").live("change", function()
		{
			if(!$(this).is(":checked"))
			{
				$(this).parent().find("label").removeClass("sku-picked");
				$('.Cor .specification').html('Disponível nas cores:');

			}else{
				if ( $('.Cor label.sku-picked').length > 0 ){
					var nomeLabel = $('.Cor label.sku-picked').text();
					$('.Cor .specification').html('Você selecionou a cor: <strong>'+nomeLabel+'</strong>');
				}
			}

		});
	}	

	function managerSkuChoice()
	{
		countAjaxComplete++;

		if(countAjaxComplete == 1)
		{		
			if($("body").hasClass("produto"))
			{
				cleanListItens();

				parseListSpec();	
			}
		}
	}


	function parseListSpec()
	{
		checkNextAvailableSku(0, listSkusContainer.length);
	}

	function checkNextAvailableSku(countListSpec, totalSpecs)
	{
		var listSkus = listSkusContainer;
		var countSpecs = countListSpec;			
		var totalLabels = ($("."+listSkus[countSpecs]+" .skuList label").length -1);
		var countCheckeds = 0;
		var lengthSpecs = (totalSpecs-1);

		for(var i = 0; i <= totalLabels; i++)
		{
			if(countCheckeds == 0)
			{
				if(!$("."+listSkus[countSpecs]+" .skuList label").eq(i).hasClass("item_unavaliable") && !$("."+listSkus[countSpecs]+" .skuList label").eq(i).hasClass("combination_unavaliable"))
				{
					console.log("Class", $("."+listSkus[countSpecs]+" .skuList label").eq(i).attr("class"));

					$("."+listSkus[countSpecs]+" .skuList input").eq(i).one("change", function(){
						console.log("Change", $("."+listSkus[countSpecs]+" .skuList input").attr("class"));

						if($(this).hasClass("item_unavaliable") || $(this).hasClass("combination_unavaliable"))
						{
							$(this).trigger("click");
							$(this).trigger("change");
							countCheckeds = 0;
							console.log("Item-Inavaliable");
						}else
						{
							console.log("Item-Avaliable");
							countCheckeds++;		
						}						
					});						

					$("."+listSkus[countSpecs]+" .skuList input").eq(i).trigger("click");

					$("."+listSkus[countSpecs]+" .skuList input").eq(i).trigger("change");

				}
			}

			if(i == totalLabels)
			{
				console.log("Finish FOR:", i, totalLabels);

				if(countSpecs < lengthSpecs)
				{
					console.log("Count Specs < Total Specs", countSpecs, lengthSpecs, "Atual:", listSkus[countSpecs], "Próximo:", listSkus[(countSpecs+1)]);
					countSpecs++;
					checkNextAvailableSku(countSpecs, lengthSpecs);
				}
			}
		}
	}


}

