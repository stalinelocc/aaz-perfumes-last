var IsEmail = function(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}

var envia_blacknovember = function(){
	console.log('news');
	// 1. Evento do botão
	$('body').on('click', '.submitCadastre', function(){
		console.log('click');

		
		// 2. recuperar os dados
		var
		txtEmail = $('.news-content .Email').val(),
		mdStore = 'azperfumes',
		urlProtocol = window.location.protocol,
		contentHeaders =
		{
			'Accept': 'application/vnd.vtex.masterdata.v10+json',
			'Content-Type': 'application/json',
			'X-VTEX-API-AppKey': 'vtexappkey-azperfumes-FZDQFL', // Pedir para o cliente o AppKey
			'X-VTEX-API-AppToken': 'MLPDCMAOCSFFICMBCQGBQEVXFVROYUFTZCMTRAGSJXOHIJEVRLSBSGGIPRYGULPIMMFGPTKCJORTQNPVQWXNBDATVEEACZXYIFVWEYFYBQREPNRBCDPYNBGILMLLOJAK', // Pedir para o cliente AppToken
		};

		// 3. verifica os campos (se alguma campo tiver vazio para a função)
		if (txtEmail == '') {
	        $('.Email').addClass('error').focus();

	    }  else if (!IsEmail(txtEmail)) {
	     	$('.Email').addClass('error').focus();

	    } else {

	    // 4. Envia para o mastertada (se todos os campos tiverm corretos)

	    	var 
			fields = {
				Email : txtEmail, //varchar
			},				
			
			apiUrl = urlProtocol + '//api.vtexcrm.com.br/'+ mdStore +'/dataentities/BL/documents',
			who = { 'Email': txtEmail },			    
			data = $.extend(who, fields);			    

			$.ajax({
			    'headers': contentHeaders,
			    'url': apiUrl,
			    'async' : false,
			    'crossDomain': true,
			    'type': 'PATCH',
			    'data': JSON.stringify(data)

			}).success(function(data) {
				// sucesso
				//alert('Cadastro enviado com sucesso');
				
				//docCookies.setItem("jaentrou", 1);				

			}).fail(function(data) {
			   	//console.log('Erro Newsletter');
			   	//console.log(data);
			   	alert('Cadastro não enviado');			    	
			});

		}

	});

}

$(document).ready(function(){
	envia_blacknovember();
});
