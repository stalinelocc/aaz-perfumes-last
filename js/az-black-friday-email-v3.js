var sendBlack = {

	init:function()
	{
		this.send_md();
	}	

	IsEmail:function(email)
	{
  		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  		return regex.test(email);
	},

	send_md:function()
	{

		$('body').on('keypress', '.lz-formulario-2018 input', function(){
	        $(this).removeClass('empty');
	    }); 

		$('body').on('click', '#submitBlack', function(e){

			e.preventDefault();

			var txtEmail = $('.lz-formulario-2018 .news_email').val();

	        if (txtEmail == '') {
	            $('.lz-formulario-2018 .news_email').addClass('empty').focus();

	        }  else if (!sendBlack.IsEmail(txtEmail)) {
	        	$('.lz-formulario-2018 .news_email').addClass('empty').focus();

	        } else {
	        	//send	        	
	        	$('.lz-formulario-2018').addClass('load');        	

	        	var
				urlProtocol = window.location.protocol,
			    mdStore = 'azperfumes';
				contentHeaders =
				{
					'Accept': 'application/vnd.vtex.masterdata.v10+json',
					'Content-Type': 'application/json',
					'X-VTEX-API-AppKey': 'vtexappkey-azperfumes-FZDQFL',
					'X-VTEX-API-AppToken': 'MLPDCMAOCSFFICMBCQGBQEVXFVROYUFTZCMTRAGSJXOHIJEVRLSBSGGIPRYGULPIMMFGPTKCJORTQNPVQWXNBDATVEEACZXYIFVWEYFYBQREPNRBCDPYNBGILMLLOJAK'
				};

	        	var 
				fields = {				   
				    BL_Email : txtEmail
				},

				apiUrl = urlProtocol + '//api.vtexcrm.com.br/'+mdStore+'/dataentities/BL/documents',
			    who = { 'BL_Email': txtEmail },
			    data = $.extend(who, fields);
			    
			    $.ajax({
			        'headers': contentHeaders,
			        'url': apiUrl,
			        'async' : false,
			        'crossDomain': true,
			        'type': 'PATCH',
			        'data': JSON.stringify(data)

			    }).success(function(data) {
			    	alert('Enviado');
			    	$('.lz-formulario-2018').removeClass('load');			    	

			    }).fail(function(data) {
			    	alert('Erro');
			    	console.log(data);
			    	$('.lz-formulario-2018').removeClass('load');
			    });			   
	        }
		});
	}
}

$(document).ready(function(){
	sendBlack.init()
});