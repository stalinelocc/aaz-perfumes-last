var _comment = function(){
	$('body').on('click', '.show-comment', function(){
		var mHeight  = $(document).height();
		$('.form-modal').css('height',mHeight+'px').show();
	});

	$('body').on('click', '.custom-file', function(){
		$('input#file-custom').click();
	});

	$('body').on('change', 'input#file-custom', function(){
		var
		fileName = $(this).val(),
		fileText = $(this)[0].files[0].name,
		fileType = $(this)[0].files[0].type,
		fileSize = $(this)[0].files[0].size;

       	if (fileName != '' || fileName != undefined) {
       		$('.custom-file .txt').hide();
       		$('.custom-file .txt-load').html('Anexado ('+fileText+' - '+fileType+')');
       		$('.custom-file').addClass('active');
       		$('#have-file').val('true');
       	} else {
       		$('.custom-file .txt').show();
       		$('.custom-file .txt-load').html('');
       		$('.custom-file').removeClass('active');
       		$('#have-file').val('false');
       	}
	});

	$('body').on('click', '.form-btn', function(){
		var
		nameComment = $('#your-name').val(),
		textComment = $('#your-mensage').val();

		if (nameComment == '') {
    		$('#your-name').addClass('error').focus();

		} else if (textComment == '') {
    		$('#your-mensage').addClass('error').focus();

		} else {
    		$('.fm-custom').addClass('load');

		}		

	});

	$('body').on('keypress', '#your-name, #your-mensage', function(){
		$(this).removeClass('error');	  
	});

	$('body').on('click', '.form-modal .close', function(){
		$('.form-modal').removeAttr('style').hide();
	});	

}

var _nav_top10 = function(){
	
	//$('.top10-female ul li:eq(0)').addClass('current');
	//$('.top10-male ul li:eq(0)').addClass('current');

	$('.top10-nav').on('click', 'a', function(){
		
		if (!$(this).hasClass('current')) {

			$('.top10-nav a').removeClass('current');
			$(this).addClass('current');

			var
			nextItem = $(this).attr('rel'),
			eqItem = parseInt(nextItem);

			$('.top10-female ul li').hide().removeClass('current');
			$('.top10-male ul li').hide().removeClass('current');

			$('.top10-female ul li:eq('+eqItem+')').fadeIn('fast').addClass('current');
			$('.top10-male ul li:eq('+eqItem+')').fadeIn('fast').addClass('current');
		}
		
	});

	$('.top10-nav a:eq(0)').click();
}

$(document).ready(function(){
	_comment();

	console.log('top10')

	$('li.helperComplement').remove();

	$('.banner.posicao1').slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		autoplay: true,
		autoplaySpeed: 2000,
		speed: 1000,
		dots: true
	});

	var iDots = 10;
	var zDots = '';
	$('.collection .slick-dots li').each(function(){
		if (iDots != 10) {
			zDots = '0';
		}
		$('button', this).html(iDots);
		iDots = iDots - 1;
	});

	$('.collection ul .ct').each(function(){
		var
		esse = $(this),		
		precoTotal =  esse.find('span.preco-por').text(),
		genero = $(this).parents('.collection').find('h2').text();		

		precoTotal = precoTotal.split("R$ ");
		precoTotal = precoTotal[1];
		precoTotal = parseInt(precoTotal);

		var total = precoTotal;
		if(total > 150){
			esse.find('.action-buy').append('<span class="freteGratis"></span>');
		}

		$('.ver-mais a', this).attr('href', '/perfumaria/perfumes-importados/'+genero)

	});


	$('.collection .slick-dots').show();

	_nav_top10();
});