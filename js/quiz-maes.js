var start_quiz = function(){
	$('body').on('click', '.go-quiz', function(){
		
		$('html, body').animate({ scrollTop: ($('.main-content').offset().top) - 110 }, 800);

		$('.start-quiz').hide();
		$('.step-1').show().addClass('current');
		$('.question-quiz').fadeIn();

		
	});

	$('body').on('click', '.item-quiz', function(){
		
		$('html, body').animate({ scrollTop: ($('.main-content').offset().top) - 110 }, 800);

		var
		next_step = $(this).parents('.step').attr('rel'),
		collection_quiz = $(this).attr('rel');

		$(this).parents('.step').find('.item-quiz').removeClass('active');
		$(this).addClass('active');
		$(this).parents('.step').hide();
			

		if (next_step == 'last') {
			$('.question-quiz').hide();
			$('.answer-quiz').fadeIn();
		} else {
			$('.'+next_step).fadeIn();
		}

		if (next_step == 'step-2') {
			$('.collection-quiz').hide();
			$('.collection-'+collection_quiz).show();
		}
	});

	$('body').on('click', '.step-back', function(){
		$('html, body').animate({ scrollTop: ($('.main-content').offset().top) - 110 }, 800);
		var back_step = $(this).attr('rel');
		$(this).parents('.step').hide();
		$('.step-'+back_step).fadeIn();

	});
}

$(document).ready(function(){
	start_quiz();
});

$(window).load(function() {
	$('.all-quiz').removeClass('gt-load');
});