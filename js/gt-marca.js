var ini_width = 0;
var base_marca = 0;
var m_percentual = 0;

var marca_tela = 0;

var start_marca = function(vm_elem) {
	if ($(vm_elem+' li').length > 6) {
		$(vm_elem+' .ma-prev').show();
		$(vm_elem+' .ma-next').show();
	}
}

var congif_marca = function(ma_elem) {
	
	m_percentual = 0;
	ini_width = 0;
	base_marca = 0;
	
	$(ma_elem+' .marca-carrossel ul').css('left','0%');
	
	marca_tela = $(window).width();
	
	if (marca_tela <= 620 && marca_tela > 362 ) {
		$('.marca-base').css('width','25%');
		m_percentual = 25;
	}
	else if (marca_tela <= 362 ) {
		$('.marca-base').css('width','50%');
		m_percentual = 50;
	}
	else {
		$('.marca-base').css('width','16.5%');
		m_percentual = 16.5;
	}
	
	$(ma_elem+' .marca-carrossel li').each(function() {
		$(this).css('left', ini_width+'%');
		ini_width = ini_width+m_percentual;

    });
}

var slide_marca = function(m_elem) {
	
	//proximo
	$('body').on('click', m_elem+' .ma-next', function(e) {
		e.preventDefault();
		base_marca = base_marca-m_percentual;		
		
		$(m_elem+' .marca-carrossel ul').animate({left:base_marca+'%'}, function() {
			$(m_elem+' .marca-carrossel li').last().after($(m_elem+' .marca-carrossel li').first());
			$(m_elem+' .marca-carrossel li').last().css('left',ini_width+'%');
			ini_width = ini_width+m_percentual;
			
		});
	});
	
	//anterior
	$('body').on('click', m_elem+' .ma-prev', function(e) {

		e.preventDefault();
		$(m_elem+' .marca-carrossel li').first().before($(m_elem+' .marca-carrossel li').last());
		$(m_elem+' .marca-carrossel ul').css('left',-m_percentual+'%');
		
		ini_width = 0;
		base_marca = 0;			

		$(m_elem+' .marca-carrossel li').each(function() {
			$(this).css('left',ini_width+'%');
			ini_width = ini_width+m_percentual;
		});
		$(m_elem+' .marca-carrossel ul').animate({left:'0%'});
		
	});
}


$(document).ready(function() {
	congif_marca('.marcas');
	slide_marca('.marcas');
	start_marca('.marcas');	
});

window.onresize = function(event) {
   congif_marca('.marcas');
};
