// Document : lista de desejos
var wishilist_after_insert = function() {
	if ($('.productWishlist .save-success').length) {
		
		console.log('produto inserido na lista');
		$('.giftlist-insertsku').html('<div class="giftlist-insertsku-ok"><span class="giftlist-title-ok">Produto inserido com sucesso</span><a class="giftlist-go-ok" href="/Wishlist/verificar" target="_top">Ver minha lista</a></div>');
		$('.giftlist-insertsku').show();
	}
	
	$('.productWishlist').removeClass('lista-load');
	
}

var add_wishlist = function() {
	
	$('a.glis-submit').removeAttr('href');
		
	var data = new Date();
	dia     = data.getDate();
	mes     = data.getMonth();
	ano2    = data.getYear();
	hora    = data.getHours();
	min     = data.getMinutes();
	seg     = data.getSeconds();
	mseg    = data.getMilliseconds();	
	var id_list = dia+''+mes+''+hora+''+min+''+seg+''+mseg; //nome da lista

	if ($('.glis-existing-title').length) {
		//ja tem lista
		$('.glis-submit-list').html('Confirmo');
		$('.glis-existing-title').html('Deseja incluir esse produto na sua lista de desejos?');
	} else {
		//nova lista
		$('input.glis-form-name').val(id_list);
		$('.glis-submit-new').html('Confirmo');
		$('.glis-new-title').html('Deseja incluir esse produto na sua lista de desejos?');		
	}
	

	$('.giftlist-insertsku').show();
	
	if ($('.glis-save').length) {
		$('.productWishlist').addClass('lista-load');
		setTimeout(wishilist_after_insert, 1500);
	}
	if ($('.giftlist-insertsku-must-login').length) {
		$('.giftlist-insertsku-must-login a.must-login').attr('target','_top');
	}
}



$(document).ajaxStop(function(){
	console.log('Lista V2');
	if ($('body.wishlist').length) {
		add_wishlist();

		setTimeout(function(){
			if($('.giftlist-title-ok').is(':visible')){
				$('.insert-sku-quantity').remove();
				$('.glis-sku-single-item').remove();
			}

		},1500);
		if($('.glis-link.must-login').is(':visible')){
			$('.giftlist-insertsku-must-login').prepend('<h4>Wishlist</h4><p>Faça login para adicionar produtos à Wishlist</p>');
			$('.glis-link.must-login').html('Login');
		}
	}
});	

$(document).ready(function(){


});