var caminhoImg = '/arquivos/';
var mobile = 'nao';
var janela = innerWidth;
if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) & janela < 1004) {
	mobile = 'sim'
}
var gt_lightbox = function(gdiv, gclasse) {
	$('html, body').animate({
		scrollTop: 0
	}, 1000);
	wHeight = $(document).height();
	$('body').prepend('<div class="gt-ext-modal" style="height:' + wHeight + 'px"><div class="gt-int-modal ' + gclasse + '"><a id="iClose" class="gt-close-modal">X</a>' + gdiv + '</div></div>');
	$('body').on('click', '.gt-close-modal', function() {
		$('.gt-ext-modal').remove();
	});
}
var espiar = function() {
	$('body').on('click', '.espiar', function() {
		var idUrl = $(this).attr('rel');
		var add_image = $(this).parents('li').find('.fotoProduto a').html();
		console.log(add_image);
		htmlEspiar = '<div class="img-add">' + add_image + '</div><iframe src="https://aazperfumes.com.br/quick-view/?idproduto=' + idUrl + '&lid=5e4d8b09-8750-4ff9-ad94-8245a4284aad" frameborder="0" scrolling="no"></iframe>'
		gt_lightbox(htmlEspiar, 'jn-add');
	});
}



var nav_store = 
{
	open_sub:function()
	{

		if (mobile === 'sim') {	
			console.log('mobile');		
			$('.hover-open .sub-open').addClass('click-open');		
			$('.hover-open .sub-open').removeAttr('href');
			$('.hover-open').removeClass('hover-open');
		}

		$('body').on('click', '.click-open', function(){
			$('.drop').slideUp('fast');
			$('body .click-close').addClass('click-open');
			$('body .click-open').removeClass('click-close');
			$(this).addClass('click-close');
			$(this).removeClass('click-open');
			$(this).next('.drop').slideDown();
		});

		$('body').on('click', '.click-close', function(){		
			$(this).removeClass('click-close');
			$(this).addClass('click-open');
			$(this).next('.drop').slideUp();
		});

		$('body').on('click', '.simple-open', function(){
			var lheight  = $(document).height();
			var show_elem = $(this).attr('rel');
			$('.'+show_elem).slideToggle('fast');
			$(this).toggleClass('active');		
		});

		var navTimers = [];
		$('.hover-open').hover(
			function () {
				var id = jQuery.data( this );
				var $this = $( this );
				navTimers[id] = setTimeout( function() {
					$this.children('.drop').fadeIn(300);
					navTimers[id] = "";
				}, 300 );
			},
			function () {
				var id = jQuery.data( this );
				if ( navTimers[id] != "" ) {
					clearTimeout( navTimers[id] );
				} else {
					$( this ).children('.drop').fadeOut(200);
				}
			}
			);

	},

	nav_mobile:function()
	{

		var slideObjectIn = function(side,elem) {
			var anim = {};  
			anim[side] = '0';  
			$(elem).show().animate(anim, 300);
		}

		var slideObjectOut = function(side,elem) {
			var heightOut = '-100%';
		  //if (side == 'bottom') {
			//heightOut = screen.width+'px';
		  //}
		  var anim = {};
		  anim[side] = heightOut;
		  $(elem).animate(anim, 300, function(){
		  	$(elem).hide();
		  });
		}

		$('#mmenu .click-out').click(function(){
			var show_elem = $('a.close-categoria.click-hide').attr('rel');
			var direcao   = $('a.close-categoria.click-hide').attr('rev');
			slideObjectOut(direcao,'.'+show_elem);
		});

		$('body').on('click', '.click-show', function(){		
			var show_elem = $(this).attr('rel');
			var direcao   = $(this).attr('rev');
			var animtop = $(this).attr('top');
			if (animtop == 'true') {
				$('html, body').animate({scrollTop: 0}, 500);
			}
			/*
			if (show_elem == 'resume-cart') {
				$('.close-cart').show();
			}
			*/
			slideObjectIn(direcao,'.'+show_elem);				
		});	

		$('body').on('click', '.click-hide', function(){			
			var show_elem = $(this).attr('rel');
			var direcao   = $(this).attr('rev');
			slideObjectOut(direcao,'.'+show_elem);
			//$('.'+show_elem).slideUp('fast');
			//$('.'+class_elem).removeClass('active');
			
		});

		$('body').on('click', '.mbt-cart', function(){
			$('.gt-categoria').find('.click-hide').click();
		});

		$('body').on('click', '.mbt-menu', function(){
			$('.minicart').find('.click-hide').click();			
		});

		$('.bar-mobile').on('click', 'a.top', function(){
			$('html, body').animate({scrollTop: 0}, 500);
		});
	},

	top_page:function()
	{
		$('body').on('click', '#voltartopo', function(){
			$('html, body').animate({scrollTop: 0}, 1000);
		});
	},

	init_config:function()
	{	
		$('.newsletter-button-ok').val('ENVIAR');
		$('li.helperComplement').remove();
		$('.bread-crumb ul li:eq(0) a').text('Home');	
		//$('.bread-crumb ul').show();
	}
}

function FormatPrecoReais(num) {
	num = num.toString().replace(/\$|\,/g, '');
	if (isNaN(num)) num = "0";
	sign = (num == (num = Math.abs(num)));
	num = Math.floor(num * 100 + 0.50000000001);
	cents = num % 100;
	num = Math.floor(num / 100).toString();
	if (cents < 10) cents = "0" + cents;
	for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++) num = num.substring(0, num.length - (4 * i + 3)) + '.' + num.substring(num.length - (4 * i + 3));
		return ((sign) ? '' : '-') + 'R$ ' + num + ',' + cents;
}
var config_departamento = function() {
	$('.orderBy:eq(0) option:eq(0)').html('Ordenar por');
	
	$('.menu-navegue a').removeAttr('href');
	$(".show-filtro").click(function() {
		$(".esconder").slideToggle();
	});
};
var busca_vazio = function() {
	if ($('.busca-vazio').length) {
		$('.gt-menu-departamento').remove();
		$('.bread-crumb ul').append('<li>Resultado de busca</li>');
		$('.gt-produtos').css('width', '100%');
	}
}
var logo_marcas = function() {
	var nome_marca = $('.brandName a').text();
	$('.brandName a').append('<img id="imgmarca" src="' + caminhoImg + 'marca-' + nome_marca + '.gif" alt="' + nome_marca + '" title="' + nome_marca + '" />');
	$('#imgmarca').error(function() {
		$('#imgmarca').remove();
		$('.brandName').addClass('somente-texto');
	});
}
var gt_sanfona = function() {
	$('.sanfona .show-content:eq(0)').addClass('ativo');
	$('.sanfona .aba:eq(0)').addClass('show');
	$('.sanfona .show-content:eq(0) a.item').removeClass('open-aba');
	$('.sanfona .aba:eq(0)').slideDown();
	$('body').on('click', '.open-aba', function() {
		var thisAba = '.a-' + $(this).attr('rel');
		var thisBotao = '.b-' + $(this).attr('rel');
		$('.sanfona .show').slideUp(function() {
			$('.sanfona .show-content').removeClass('ativo');
			$(thisBotao).addClass('ativo');
			$('.sanfona .show-content a.item').addClass('open-aba');
			$('.sanfona .aba').removeClass('show');
			$(thisAba).addClass('show');
			$(thisBotao + ' a.item').removeClass('open-aba');
			$(thisAba).slideDown();
		});
	});
}

var qtd_produto = function() {
	qtd_Estoque = $('a.buy-button').attr('style').toString();
	if (qtd_Estoque == 'display:block') {
		$('.m-qtd').show();
	} else {
		$('.m-qtd').remove();
		$('.b-frete').hide();
		$('.a-frete').hide();
	}
	$('.m-qtd').on('click', '.mais-qtd', function() {
		var qtd_Futura = parseInt($('.campo-qtd').val()) + 1;
		var url_Passada = $('a.buy-button').attr('href');
		var skuSelecionado = url_Passada.substr(0, 3);
		if (skuSelecionado == '/ch') {
			url_Atual = url_Passada.split('&qty=');
			url_Futura = url_Atual[0] + '&qty=' + qtd_Futura + '&seller=1&redirect=true&sc=1';
			$('a.buy-button').attr('href', url_Futura);
			$('.campo-qtd').val(qtd_Futura);
			$('#calculoFrete .quantity input').val(qtd_Futura);
		} else {
			alert('Por favor, selecione o modelo desejado');
		}
	});
	$('.m-qtd').on('click', '.menos-qtd', function() {
		var campoMin = parseInt($('.campo-qtd').val());
		if (campoMin > 1) {
			var qtd_Futura = parseInt($('.campo-qtd').val()) - 1;
			var url_Passada = $('a.buy-button').attr('href');
			var skuSelecionado = url_Passada.substr(0, 3);
			if (skuSelecionado == '/ch') {
				url_Atual = url_Passada.split('&qty=');
				url_Futura = url_Atual[0] + '&qty=' + qtd_Futura + '&seller=1&redirect=true&sc=1';
				$('a.buy-button').attr('href', url_Futura);
				$('.campo-qtd').val(qtd_Futura);
				$('#calculoFrete .quantity input').val(qtd_Futura);
			} else {
				alert('Por favor, selecione o modelo desejado');
			}
		}
	});
};
var nav_dicas = function() {
	$('.top-info').on('click', 'li a', function() {
		var goEscolha = $(this).attr('rel');
		$('html, body').animate({
			scrollTop: ($('#' + goEscolha).offset().top) - 120
		}, 800);
	});
}

var nav_marcas = function() {
	$('.filtro-marcas').on('click', 'a', function() {
		var letra = $(this).attr('rel');
		var letraFx = 147;
		if (janela < 1198) {
			letraFx = 0;
		}
		$('html, body').animate({
			scrollTop: ($('.' + letra).offset().top) - letraFx
		}, 800);
	});
}

var ch_buy = function() {
	ch_name = $('.informacoes-produto .fn:eq(0)').text();
	ch_brand = $('.informacoes-produto .brandName a').text();
	ch_image = $('.thumbs li:eq(0) a img').attr('src');
	$('.compra-fixa .cf-name').html(ch_name);
	$('.compra-fixa .cf-brand').html(ch_brand); 
	$('.compra-fixa .cf-image').html('<img src="'+ch_image+'" alt="'+ch_name+'" />'); 
	
	$('.cf-buy').on('click', 'a', function() {
		var br_fixo = 50;
		if (janela < 1198) {
			br_fixo = 0;
		}

		$('html, body').animate({
			scrollTop: ($('.informacoes-produto').offset().top) - br_fixo
		}, 800);
	});
}
var gt_login = function() {
	if ($('p.welcome a#login').length) {
		$('.drop-user').remove();
		$('.buttons li.user a.first').attr('id', 'login');
	} else {
		$('.drop-user').html('<li><a href="/account">Minha Conta</a></li><li><a href="/account/orders">Meus Pedidos</a></li><li><a href="/no-cache/user/logout">Sair</a></li>');
	}
	$('.buttons li.user').show();
}

var gt_login2 = function() {
	$('body').on('click', '.login-redir', function(){
		var redirPage = $(this).attr('rel');
		if ($('.my_login a#login').length) {

			$('html, body').animate({scrollTop: 0}, 0);
			vtexid.setScopeName(jsnomeSite);
			vtexid.start({
				returnUrl: redirPage,
				userEmail: '',
				locale: 'pt-BR',
				forceReload: false
			});

		} else {
			window.location.href = redirPage;
		}
	});
}

var mini_Cart = function() {
	$('th.cartSkuQuantity').html('Qtd');
	$('.cartFooter a.cartCheckout').html('Finalizar pedido');
	var atualQtd = parseInt($('.minicart .amount-items-em:eq(0)').text());
	if (atualQtd == 0) {
		$('.vtexsc-cart').html('<p class="mini-cart-vazio">Você ainda não adicionou produtos em seu carrinho</p>');
	}
}
var config_produto = function() {

	$('.informacoes-produto .fn').removeClass('productName');
	
	if ($('.compre-junto table').length) {
		$('.junto').show();
	}
	if ($('.skuList').length < 1) {
		$('.sku-buy').show();
	}
	$('.box-product').on('click', 'h3.ini a', function() {
		var classOpen = $(this).attr('rel');
		$('.' + classOpen).slideToggle('fast');
		$(this).toggleClass('inativo');
	});
	$('.skuList').last().addClass('last');
	$('#lnkComprar').text('Compre Junto')


}
var imagem_marca = function() {
	var name_marca = $('.brandName a').text();
	var txt_marca = $('.brandName a').attr('class');
	var img_marca = txt_marca.replace('brand ', '');
	var src_marca = caminhoImg + 'titulo-' + img_marca + '.jpg';
	$('.img-marca .logo-marca').html('<img src="' + src_marca + '" alt="' + name_marca + '" />');
	$('.productDescription').prepend('<div class="marca-descricao"><img src="' + src_marca + '" id="imgmarca" alt="' + name_marca + '" /></div>');
	$('#imgmarca').error(function() {
		$('#imgmarca').remove();
		$('.img-marca .logo-marca').html(name_marca);
		$('.img-marca .logo-marca').addClass('txt-marca');
	});

	setTimeout(function(){
		$(".logo-marca").removeAttr("href");
		$(".logo-marca").attr("href", "/"+img_marca);
	},3000);

}

var gt_carrossel_thumb = function() {  

	var totalThumb = $('.thumbs li').length;
	console.log(totalThumb);
	var inicioThumb = 0;
	var finalThumb = totalThumb;
	var lastThumb = totalThumb+2;
	var thumbExibe = 0;
	var thumbEsconde = 0;

	if (totalThumb > 3) {
		$('.thumbs li').hide();
		$('.thumbs li:eq(0)').show();
		$('.thumbs li:eq(1)').show();
		$('.thumbs li:eq(2)').show();
		$('.thumbs').prepend('<div class="gt_anterior" style="display:none">Anterior</div><div class="gt_proxima" style="display:block">Proxima</div>');
		$('.thumbs').addClass('thumb-slider');
	}
	$('body').on('click', '.gt_proxima', function() {  
		inicioThumb = inicioThumb + 1;
		thumbEsconde = inicioThumb-1;
		thumbExibe = inicioThumb+2;
		$('.gt_anterior').show();
		if (inicioThumb < (totalThumb-3)) {
			$('.gt_proxima').show();
		} else {
			$('.gt_proxima').hide();
		}
		$('.thumbs li:eq('+thumbExibe+')').show(300);
		$('.thumbs li:eq('+thumbEsconde+')').hide(300);

	});
	$('body').on('click', '.gt_anterior', function() {  
		inicioThumb = inicioThumb - 1;
		thumbExibe = inicioThumb;
		thumbEsconde = inicioThumb+3;	  
		$('.gt_proxima').show();
		if (inicioThumb <= 0) {
			$(this).hide();
		} else {
			$(this).show();
		}
		$('.thumbs li:eq('+thumbExibe+')').show(300);
		$('.thumbs li:eq('+thumbEsconde+')').hide(300);	
	});


	$('.thumbs').show();
}

var gt_carrossel_thumb_click = function(){
	$('body').on('click', '.skuList label', function() {
		$('.thumbs').hide();
		$('.gt_anterior').remove();
		$('.gt_proxima').remove();
		$('.thumbs').removeClass('thumb-slider');
		setTimeout(gt_carrossel_thumb, 1000);		
	});
}

var config_filtro = function() {
	$('.left-nav ul').hide();
	$('.left-nav fieldset div').hide();
	// $('.left-nav fieldset:eq(0) div').show();
	// $('.left-nav fieldset.refino-marca div').show();
	$('.left-nav fieldset:eq(0) h5').addClass('ativo');
	$('.left-nav fieldset.refino-marca h5').addClass('ativo');
	$('.left-nav h4').last().addClass('last');
	$('.left-nav fieldset:eq(0)').prepend('<p class="titulo-filtro">Filtre sua busca</p>');
	$('body').on('click', '.search-single-navigator h5', function() {
		$(this).toggleClass('ativo');
		$(this).next('ul').slideToggle('fast');
	});
	$('body').on('click', '.search-multiple-navigator h5', function() {
		$(this).toggleClass('ativo');
		$(this).next('div').slideToggle('fast');
	});
}
var num_top10 = function() {
	$('.top10 .content-top > div').each(function() {
		var numrank = 0;
		$(this).find('.rank').each(function() {
			var add_zero = '';
			numrank = numrank + 1;
			if (numrank < 10) {
				add_zero = '0'
			}
			$(this).text(add_zero + numrank);
		});
	});
}

var precoMenu = function(){

	$('#mmenu .all').append('<li class="pai"><a class="pai sub-open click-open">Preços</a><div class="nivel-1 sub drop" style="display: none;"><div class="nivel-2 precos"></div></div></a></li>');
	var tabelaPreco = $('.filtroPrecos');
	$('#mmenu .all .nivel-2.precos').append(tabelaPreco);
	$('.filtroPrecos:eq(1)').css('display', 'none');
}
var skuClick = function(){
	$('.skuList.item-dimension-Tamanho').each(function(){
		$(this).find("label:not(.item_unavailable):eq(0)").trigger("click").siblings('click');

		return false;

	});
}

var carrossel_colecao = function(elemWrap, numIni){
	$(elemWrap).slick({
		infinite: true,
		slidesToShow: numIni,
		slidesToScroll: 1,
		responsive: [			
		{
			breakpoint: 768,
			settings: {
				slidesToShow: 3,
				slidesToScroll: 1
			}
		},
		{
			breakpoint: 620,
			settings: {
				slidesToShow: 2,
				slidesToScroll: 1
			}
		}      	
		]
	});
}

var carrossel_colecao_2 = function(elemWrap, numIni){
	$(elemWrap).slick({
		infinite: true,
		slidesToShow: numIni,
		slidesToScroll: 1,
		responsive: [			
		{
			breakpoint: 768,
			settings: {
				slidesToShow: 3,
				slidesToScroll: 1
			}
		},
		{
			breakpoint: 620,
			settings: {
				slidesToShow: 2,
				slidesToScroll: 1
			}
		},
		{
			breakpoint: 480,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			}
		}      	
		]
	});
}


/*NOVO CATALOGO*/
var pgCatalog = 
{

	total_product:function()
	{
		var qtdProduct = $('.resultado-busca-numero:eq(0) .value').text();	
		$('.header-category .total-page').html('<strong>'+qtdProduct+'</strong> Produtos encontrados');
		$('.pager.top').prepend('<div class="p-total-page"><strong>'+qtdProduct+'</strong> Produtos encontrados</div>');
	},

	new_order:function()
	{
		var nextOrder = $('.orderBy:eq(0) select').attr('onchange');
		var nextOrder2 = nextOrder.split("href= '");
		var nextOrder3 = nextOrder2[1].split("' + 'O=");
		var htmlOrder = '';

		$('.orderBy:eq(0) option').each(function() {
			var classOrder="";	
			var valueOrder = $(this).val();
			var textOrder = $(this).text();
			if($(this).is(':selected')) {
				classOrder = 'current-order';			
			}
			htmlOrder = htmlOrder + '<li><a href="'+nextOrder3[0]+'O='+valueOrder+'" class="'+classOrder+'">'+textOrder+'</a></li>';		
		});

		htmlOrder = '<a class="bt-new-order simple-open" rel="option-new-order">ORDENAR POR <span class="ico"></span></a><ul class="option-new-order">'+htmlOrder+'</ul></div>';
		$('.new-order').html(htmlOrder);
		$('.new-order li:eq(0) a').remove();
		$('.new-order li:eq(0)').hide();
		$('.new-order li:eq(0)').addClass('title');
		$('.new-order li a.current-order').attr('href', 'javascript:void(0);');
	},	

	nav_filter:function()
	{
		
		/*
		$('.search-multiple-navigator h5').addClass('close');
	
		$('body').on('click', '.menu-departamento h5.close', function() {			
			$(this).addClass('active').removeClass('close');
			$(this).next('div').slideDown('fast');
		});

		$('body').on('click', '.menu-departamento h5.active', function() {
			$(this).removeClass('active').addClass('close');
			$(this).next('div').slideUp('fast');
		});
		*/

		var largW = $(window).width();
		if(largW < 768){
			$('.search-multiple-navigator').find('fieldset').each(function(){
				$(this).find('h5.click-close').removeClass('click-close').addClass('click-open');
				$('.search-multiple-navigator fieldset:eq(0) div').slideUp();
			});
		}

		$('.search-multiple-navigator fieldset h5').addClass('click-open');
		$('.search-multiple-navigator fieldset div').addClass('drop');

		
		$('body').on('click', '.bt-refinar', function(){
			/*$('html, body').animate({scrollTop: 0}, 1000);*/
			$('body').addClass('gtn-load');
		});
		

		

		var
		txtDepartament = vtxctx.departmentName,
		txtCategory = vtxctx.categoryName,
		linkDefault = '';

		if (txtDepartament == txtCategory) {
			linkDefault = '/'+txtDepartament;
		} else {
			linkDefault = '/'+txtDepartament+'/'+txtCategory;
		}

		linkDefault = linkDefault.toLowerCase();
		linkDefault = linkDefault.replace(/ /, '-');
		console.log(linkDefault);

		
		if ($('.filtro-ativo').length) {
			$('<div class="filtros-ativos"><h2>Filtrado por:</h2><ul></ul><a href="'+linkDefault+'">Limpar filtros aplicados</a></div>').insertBefore('.search-multiple-navigator .bt-refinar');
			//$('.menu-departamento').prepend('<div class="filtros-ativos"><h3>Filtrado por:</h3><ul></ul><a href="'+linkDefault+'">Limpar filtros aplicados</a></div>');
			$('.filtro-ativo').each(function(){
				var nomeAtivo = $(this).text();
				var linkAtivo = $(this).next('.ver-filtros').attr('href');				
				$('.filtros-ativos ul').append('<li>'+nomeAtivo+'</li>');
			});
		}

		
		$('.search-multiple-navigator a.ver-filtros').parents('fieldset').remove();
		$('.search-multiple-navigator fieldset:eq(0)').prepend('<p class="titulo-filtro">Filtre sua busca</p>');
		
		/*$('.bt-refinar').attr('href', 'javascript:void(0)');*/
		$('.search-multiple-navigator h5:eq(0)').click();

		$('.menu-departamento').show();		
		$('.col-cat').removeClass('gt-load');
		
	},

	list_grid:function()
	{
		$('body').on('click', '.edit-grid', function(){
			var nextGrid = $(this).attr('rel');
			$('.show-grid a').removeClass('active');
			$(this).addClass('active');
			$('body').removeClass('grid-line').removeClass('grid-col');
			$('body').addClass(nextGrid);
		});
	}
}

var congif_mobile = function(){
	var max_window = $(window).width();
	var max_height  = $(document).height();

	if (max_window > 768) {
		$('.gt-categoria, .gt-categoria .wrap-1, .col-cat, .nav-user, .sub.resume-cart').removeAttr('style');

	} else {		
		$('.gt-categoria, .gt-categoria .wrap-1, .col-cat, .nav-user, .sub.resume-cart').css('height',wHeight+'px');		
	}		
}

$(window).load(function() {
	congif_mobile();
});

window.onresize = function(event) {
	congif_mobile();
};

$(document).ready(function() {
	gt_login2();
	console.log('v5');

	nav_store.open_sub();
	nav_store.nav_mobile();
	nav_store.top_page();
	nav_store.init_config();

	

	var largW = $(window).width();
	if(largW = 768){
		$(".btFilter a").click(function() {
			$('html, body').animate({
				scrollTop: $(".masterBannerTopBar").offset().top
			}, 900);
		});
	}

	if($('body.thumbcores ').length > 0){
		setTimeout(function(){
			$('div#TRUSTEDCOMPANY_widget_56').insertAfter('.informacoes-produto .box-product');
		},300);
	}

	if($('body.checkbox ').length > 0){
		setTimeout(function(){
			$('div#TRUSTEDCOMPANY_widget_56').insertAfter('.informacoes-produto .box-product');
		},300);
	}

	if($('body.new-marcas').length){
		nav_marcas();
		$('.best-brand ul').slick({
			infinite: true,
			slidesToShow: 6,
			slidesToScroll: 1,
			responsive: [			
			{
				breakpoint: 920,
				settings: {
					slidesToShow: 5,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 4,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 640,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 360,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}      	
			]
		});
	}


	setTimeout(function(){
		if($("body.produto").length > 0){
			// Alterando Para foto o item Cor
			$('body').append('<div class="variations" style="display:none"/>');
			vtexjs.catalog.getCurrentProductWithVariations().done(function(product){
				var obj = product.skus;
				var variacao = obj.length;

				$.each(obj, function(ind, obj){
					var img = product.skus[ind].image;
					var cor = product.skus[ind].dimensions["Cor"];
					$('.variations').append('<img src="'+img+'" cor="'+cor+'" />');
				});
			});

			$('.item-dimension-Cor span label').each(function(){
				var txt = $(this).text();
				var el = $('.variations').find('img[cor="'+txt+'"]:eq(0)').attr('src');
				$(this).css("background-image","url("+el+")");
			});
		}
	},400);


	// console.log('V51');
	if ($('.resume-cart').length) {
		$('.resume-cart').minicart({
			showMinicart: true,
			showTotalizers: true
		});
	}
	espiar();
	

	// $('.flag.discount').each(function(){
	// 	var flagPrice = $('span', this).text();
	// 	flagPrice = flagPrice.split(',');
	// 	$('span', this).text(flagPrice[0]);
	// 	$(this).css('display','block');

    // });
    
    // var descontoPrateleira = function(){
    //     $('.flag.discount').each(function(){
    //         var flagPrice = $('span', this).text();
    //         flagPrice = flagPrice.split(',');
    //         $('span', this).text(flagPrice[0]);
    //         $(this).css('display','block');
    //      });
    // }
    // $(document).ready(function(){
    //     setTimeout(function() {
    //         descontoPrateleira();
    //     }, 3000);
    //     $('body').on('click', '.pages li', function(){
    //         setTimeout(function() {
    //             descontoPrateleira();
    //         }, 1000);
    //     });
    // });  


	if ($('body.home').length) {
		$('.helperComplement').remove();
		$('#TRUSTEDCOMPANY_widget_101382').append('<script async src="https://d3643s33l1rfut.cloudfront.net/js/widget?w=BQYHDBEcXkJYQ1hWS1RSW0VbXUU"></script>');

		$('.banner.posicao1').slick({
			infinite: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			autoplay: true,
			autoplaySpeed: 2000,
			speed: 1000,
			dots: true
		});

		$('.banner-mobile').slick({
			infinite: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			autoplay: true,
			autoplaySpeed: 2000,
			speed: 1000,
			dots: true
		});
		

		carrossel_colecao('.collection.group-1 ul', 4);
		carrossel_colecao('.collection.group-2 ul', 4);
		
		$('.video').on('click', function() {
			var rel = $(this).find('#rel').html();
			var htmlV = rel;
			gt_lightbox(htmlV, 'videoLB');
			return false;
		});
	}

	if ($('body.blackfriday-2016').length) {
		console.log('blackfriday');
		$('.lista-produtos .sub:eq(0)').prepend('<div class="bread-crumb"><ul><li><a title="AZPerfumes" href="/">Home</a></li><li class="last"><strong><a title="Black Friday">Black Friday</a></strong></li></ul></div>');
	}

	if ($('body.produto').length) {
		$('.helperComplement').remove();

		if ($('.flPromocao p.flag.entrega_45_dias').length) {
			$('.flPromocao p.flag.entrega_45_dias').html('<strong>PRODUTO EXCLUSIVO</strong> - Entrega diferenciada de <strong>30</strong> à <strong>45 dias</strong>');
			$('.flPromocao p.flag.entrega_45_dias').show();
		}

		if ($('.productPrice strong.skuBestPrice').length) {
			precoCheio = $('.productPrice strong.skuBestPrice').text();
			precoCheio = precoCheio.replace('R$','');
			precoCheio = precoCheio.replace('.','');
			precoCheio = precoCheio.replace(',','.');
			precoCheio = parseFloat(precoCheio);
			
			if(precoCheio > 150){
				$('.action-buy').addClass('fretegratis');
			}
			
		}

		carrossel_colecao('.collection.group-2 ul', 4);

		$('#TRUSTEDCOMPANY_widget_101382').append('<script async src="https://d3643s33l1rfut.cloudfront.net/js/widget?w=BQYHDBEcXkJYQ1hWS1RSW0VbXUU"></script>');
		
		$('.video').on('click', function() {
			var rel = $(this).find('#rel').html();
			var htmlV = rel;
			gt_lightbox(htmlV, 'videoLB');
			return false;
		});
	}


	if ($('.busca-vazio').length > 0) {
		var termo = window.location.pathname;
		termo = termo.replace('/', '');
		termo = '<a title="' + termo + '" href="/' + termo + '">' + termo + '</a>';
		$('.bread-crumb ul li').append(termo);
		var conteudoBuscaVazia = '<p>Sua busca não encontrou nenhum resultado. Mas, temos diversos produtos que podem ser do seu interesse. Confira abaixo.</p>';
		$('.busca-vazio').html(conteudoBuscaVazia);
		$('.prateleiras_resultado_de_busca_nao_encontrado, .side-nao-encontrada').fadeIn();
		$('.left-nav').fadeOut();
		$('.lista-produtos').addClass('vazio');
	}

	if ($('.didyoumean').length) {
		var termo = window.location.pathname;
		termo = termo.replace('/', '');
		termo = '<a title="' + termo + '" href="/' + termo + '">' + termo + '</a>';
		$('.bread-crumb ul li').append(termo);
		var conteudoBuscaVazia = '<p>Sua busca não encontrou nenhum resultado. Mas, temos diversos produtos que podem ser do seu interesse. Confira abaixo.</p>';
		$('.busca-vazio').html(conteudoBuscaVazia);
		$('.prateleiras_resultado_de_busca_nao_encontrado, .side-nao-encontrada').fadeIn();
		$('.left-nav').fadeOut();
		$('.lista-produtos').addClass('vazio');
	}

	if ($('body.top10').length) {
		num_top10();
		$('.menu-top li a').click(function() {
			var ref = $(this).attr('href');
			$('.menu-top li a').removeClass('ativo');
			$(this).addClass('ativo');
			$('.content-top > div').hide();
			$('.content-top > div' + ref).fadeIn();
			return false;
		});
	};
	if ($('body.como-escolher').length) {
		nav_dicas();
	}
	if ($('body.category').length) {
		//config_departamento();
		busca_vazio();
		//config_filtro();
		//$(".resultado-busca-filtro:eq(0) .orderBy").insertBefore(".pager.top .pages");		
		pgCatalog.total_product();
		pgCatalog.new_order();
		pgCatalog.nav_filter();
		pgCatalog.list_grid();
	}

	if ($('body.departament').length) {		
		busca_vazio();			
		pgCatalog.total_product();
		pgCatalog.new_order();
		pgCatalog.nav_filter();
		pgCatalog.list_grid();
	}

	if ($('body.resultado-busca').length) {

		pgCatalog.total_product();
		pgCatalog.new_order();
		pgCatalog.nav_filter();
		pgCatalog.list_grid();

		//banners coleção
		var ref = window.location.search;
		ref = ref.split("H:");
		ref = ref[1];
		//inserindo banner
		$("#banners_colecao").find(".box-banner").each(function(ndx,item){
			var alt = $(item).find("img").attr("alt");
		});
		$("#bg_colecao").find(".box-banner").each(function(ndx,item){
			var alt = $(item).find("img").attr("alt");
			var imagem = $(item).find('>a').find('img').attr('src');
			if(alt==ref){
				console.log('entrou')
				$("#gcontent").css("background","url('"+imagem+"')");
				$('body').addClass('fixShelf');
			};
		});

		$('#banners_colecao').find('.box-banner').each(function(){
			var $this = $(this);
			$(this).find('img').each(function(){
				var altt = $(this).attr('alt');
				$(this).parents('.box-banner').attr('alt', altt);
				// console.log(this);
			});
		});
		$('.box-banner[alt= "'+ ref +'"]').insertBefore('.header-category').show();

		if($('.box-marcas-resize-novo').length > 0){
			$('.left-nav').addClass('SP');
			$('.new-order').addClass('SP');
		}else if(($('body.resultado-busca').length > 0) && ($('.box-marcas-resize-novo').length > 0)){
			$('.left-nav').addClass('CP');
			$('.new-order').addClass('CP');
		}else if(($('body.resultado-busca').length > 0) && ($('.col-prod .box-banner').length > 0)){
			$('.left-nav').addClass('CP');
			$('.new-order').addClass('CP');
		}else if(($('body.resultado-busca').length > 0) && ($('.box-marcas-resize-novo').length = -1)){
			$('.left-nav').addClass('SP');
			$('.new-order').addClass('SP');
		} else{
			$('.left-nav').addClass('CP');
			$('.new-order').addClass('CP');
		}
	};

	if ($('body.como-escolher').length) {
		$('#olfativa .tags a').click(function() {
			var ref = $(this).attr('href');
			$('#olfativa .tags a').removeClass('ativo');
			$(this).addClass('ativo');
			$('#olfativa #conteudo-tabs > div').hide();
			$('#olfativa #conteudo-tabs > div' + ref).css('display', 'table-cell');
			return false;
		});
		$('#estacao .tags a').click(function() {
			var ref = $(this).attr('href');
			$('#estacao .tags a').removeClass('ativo');
			$(this).addClass('ativo');
			$('#estacao #conteudo-tabs2 > div').hide();
			$('#estacao #conteudo-tabs2 > div' + ref).css('display', 'table-cell');
			return false;
		});
	};
	if ($('body.produto').length) {
		ch_buy();
		$('.box-product h3.ini > a').click(function(){
			if($('#login').length > 0){
				var novaURL = "/login";
				$(window.document.location).attr('href',novaURL);
			}else{
				console.log('você pode avaliar este produto');
			}
		});

		setTimeout(function(){

			if($('.box-product.load').length > 0){
				$('.box-product.load').removeClass('load');
				$('.box.comentarios').css("display","block");

			}
		},500);

		/*
		setTimeout(function(){
			var url_avalie1 = $("#rdoInteresse").attr("onclick");
			url_avalie1 = url_avalie1.replace("http", "https");
			$("#opcoes-avalie input").attr("onclick", url_avalie1);

			var url_avalie2 = $("#lnkPubliqueResenha").attr("href");
			url_avalie2 = url_avalie2.replace("http", "https");
			$("#lnkPubliqueResenha").attr("href", url_avalie2);

		},600);
		*/

		config_produto();
		if (document.location.href.indexOf('?idsku=') > 0){
			console.log('Tem SKU');
		}else{
			skuClick();
		}
		imagem_marca();
		gt_carrossel_thumb();
		gt_carrossel_thumb_click();
		$('body').on('click', '.mais-avaliacao', function() {
			$('.user-review').slideToggle('fast');
		});
		if ($('.brinde_controle ul > li').length < 1) {
			$('.brinde_controle').hide();
		};
		$('<span class="msg-juros"> sem juros</span>').insertAfter('.valor-dividido.price-installments strong');
		$('.trustedcompany-widget').append("<script>(function(){document.getElementById('trustedcompany-widget').src='//trustedcompany.com/embed/widget/v2?domain=aazperfumes.com.br&type=d&review=1&text=a';})();</script>")
		$('#TRUSTEDCOMPANY_widget_101381').append('<script async src="https://d3643s33l1rfut.cloudfront.net/js/widget?w=BQYHDBEcXkJYQ1hWS1RSW0VbXUY"></script>');
		var lk = $('.brandName a').attr('href');
		$('a.logo-marca').attr('href', lk);

		var notifyme = $('.portal-notify-me-ref');
		notifyme.insertAfter('.sku-buy');

	// if($('.flags .entrega_45_dias').length){
	// 	var html45dias = "<div id='raridadesEntrega45dias'><p><strong>PRODUTO EXCLUSIVO</strong> - Entrega diferenciada de <strong>30</strong> à <strong>45 dias</strong></p></div>";
	// 	$(html45dias).insertAfter('.portal-notify-me-ref');
	// }

	//$('.brinde_controle').insertBefore('.flPromocao');



};
if ($('body.produto.checkbox, body.produto.thumbcores').length) {
	qtd_produto();
};
if ($('body.conteudo').length) {
	$('.bread-crumb ul').append('<li class="last">' + $('.pinst h2:eq(0) span').text() + '</li>');
};
if ($('body.ajuda').length) {
	gt_sanfona();
};

/*
if($('body.home').length > 0){
	$('.f_precos').prepend('<a href=""></a>');
	$('.f_precos:eq(0) a').attr("href", "/de-0,5-a-50?PS=30&map=priceFrom");
	$('.f_precos:eq(1) a').attr("href", "/de-50-a-100?PS=30&map=priceFrom");
	$('.f_precos:eq(2) a').attr("href", "/de-100-a-150?PS=30&map=priceFrom");
	$('.f_precos:eq(3) a').attr("href", "/de-150-a-200?PS=30&map=priceFrom");
	$('.f_precos:eq(4) a').attr("href", "/de-200-a-250?PS=30&map=priceFrom");
};
*/

// if($('body.blackfriday-2016').length > 0){
// 	$('.lista-produtos .gt-vitrine li').each(function(){
// 		$(this).find('.flags').append('<span class="flag blackfriday"></s>');
// 	});
// };

});

$(document).ajaxStop(function() {
	mini_Cart();
	//gt_login();
	
	if ($('body.produto').length) {
		// config_resenhas();
	}
	
	if($('body.blackfriday-2016').length > 0){
		$('.lista-produtos .gt-vitrine li').each(function(){
			$(this).find('.flags').append('<span class="flag blackfriday"></s>');
		});
	};

	$('.prateleira ul').find('li').each(function(){
		if($(this).hasClass('flagAdicionada') != true){
			$(this).addClass('flagAdicionada');
		}else{
			$(this).removeClass('flagAdicionada');
		};
	});
	$('.prateleira ul').find('li.flagAdicionada').each(function(){
		var esse = $(this);
		var economiaBoleto = "";
		var precoTotal = "";
		var ecomomiaBoleto2 = "";
		var precoTotal =  esse.find('span.preco-por').text();

		precoTotal = precoTotal.split("R$ ");
		precoTotal = precoTotal[1];
		precoTotal = parseInt(precoTotal);

		var total = precoTotal;
		if(total > 150){
			esse.find('.show-buy').append('<span class="freteGratis"></span>');
		}

	});


});



$(window).scroll(function(event) {	
	var w_window = $(window).width();
	if ($(this).scrollTop() > 192) {
		$('#voltartopo').fadeIn(200);
		$('body').addClass('top-fixed');
	} else {
		$('#voltartopo').fadeOut(200);
		$('body').removeClass('top-fixed');

		if(w_window > 768){
			console.log(w_window);
			$('.gt-categoria').removeAttr('style');
			$('.burger').removeClass('active');
			$('#header').removeAttr('style');
		}
	}

	if ($(this).scrollTop() > 193) {
		$('.top-fixed #header').fadeIn('slow');
	}


	/*
	if(janela < 1030 & $('.nivel-2.precos').length == 0 ){
		precoMenu();
	}
	if(janela >1030 & $('.nivel-2.precos').length == 1 ){
		$('#mmenu .all .nivel-2.precos').remove();
		$('.filtroPrecos:eq(1)').css('display', 'block');
	}
	*/

	

	if ($(this).scrollTop() > 590) {
		$('body').addClass('buy-fixed');
	} else {
		$('body').removeClass('buy-fixed');
		$('.compra-fixa').removeAttr('style');
	}

	if ($(this).scrollTop() > 591) {
		$('.buy-fixed .compra-fixa').fadeIn('slow');
	}

});

var wHeight  = $(document).height();
var wW