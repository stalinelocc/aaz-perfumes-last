// Globals Var
var dataskumanager;
var countAjaxComplete = 0;
var idInputSelected;
var countAjaxStop = 0;
(function(e) {
    e.fn.numeric = function(t, n) {
        if (typeof t === "boolean") {
            t = {
                decimal: t
            }
        }
        t = t || {};
        if (typeof t.negative == "undefined") t.negative = true;
        var r = t.decimal === false ? "" : t.decimal || ".";
        var i = t.negative === true ? true : false;
        var n = typeof n == "function" ? n : function() {};
        return this.data("numeric.decimal", r).data("numeric.negative", i).data("numeric.callback", n).keypress(e.fn.numeric.keypress).keyup(e.fn.numeric.keyup).blur(e.fn.numeric.blur)
    };
    e.fn.numeric.keypress = function(t) {
        var n = e.data(this, "numeric.decimal");
        var r = e.data(this, "numeric.negative");
        var i = t.charCode ? t.charCode : t.keyCode ? t.keyCode : 0;
        if (i == 13 && this.nodeName.toLowerCase() == "input") {
            return true
        } else if (i == 13) {
            return false
        }
        var s = false;
        if (t.ctrlKey && i == 97 || t.ctrlKey && i == 65) return true;
        if (t.ctrlKey && i == 120 || t.ctrlKey && i == 88) return true;
        if (t.ctrlKey && i == 99 || t.ctrlKey && i == 67) return true;
        if (t.ctrlKey && i == 122 || t.ctrlKey && i == 90) return true;
        if (t.ctrlKey && i == 118 || t.ctrlKey && i == 86 || t.shiftKey && i == 45) return true;
        if (i < 48 || i > 57) {
            if (this.value.indexOf("-") != 0 && r && i == 45 && (this.value.length == 0 || e.fn.getSelectionStart(this) == 0)) return true;
            if (n && i == n.charCodeAt(0) && this.value.indexOf(n) != -1) {
                s = false
            }
            if (i != 8 && i != 9 && i != 13 && i != 35 && i != 36 && i != 37 && i != 39 && i != 46) {
                s = false
            } else {
                if (typeof t.charCode != "undefined") {
                    if (t.keyCode == t.which && t.which != 0) {
                        s = true;
                        if (t.which == 46) s = false
                    } else if (t.keyCode != 0 && t.charCode == 0 && t.which == 0) {
                        s = true
                    }
                }
            } if (n && i == n.charCodeAt(0)) {
                if (this.value.indexOf(n) == -1) {
                    s = true
                } else {
                    s = false
                }
            }
        } else {
            s = true
        }
        return s
    };
    e.fn.numeric.keyup = function(t) {
        var n = this.value;
        if (n.length > 0) {
            var r = e.fn.getSelectionStart(this);
            var i = e.data(this, "numeric.decimal");
            var s = e.data(this, "numeric.negative");
            if (i != "") {
                var o = n.indexOf(i);
                if (o == 0) {
                    this.value = "0" + n
                }
                if (o == 1 && n.charAt(0) == "-") {
                    this.value = "-0" + n.substring(1)
                }
                n = this.value
            }
            var u = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, "-", i];
            var a = n.length;
            for (var f = a - 1; f >= 0; f--) {
                var l = n.charAt(f);
                if (f != 0 && l == "-") {
                    n = n.substring(0, f) + n.substring(f + 1)
                } else if (f == 0 && !s && l == "-") {
                    n = n.substring(1)
                }
                var c = false;
                for (var h = 0; h < u.length; h++) {
                    if (l == u[h]) {
                        c = true;
                        break
                    }
                }
                if (!c || l == " ") {
                    n = n.substring(0, f) + n.substring(f + 1)
                }
            }
            var p = n.indexOf(i);
            if (p > 0) {
                for (var f = a - 1; f > p; f--) {
                    var l = n.charAt(f);
                    if (l == i) {
                        n = n.substring(0, f) + n.substring(f + 1)
                    }
                }
            }
            this.value = n;
            e.fn.setSelection(this, r)
        }
    };
    e.fn.numeric.blur = function() {
        var t = e.data(this, "numeric.decimal");
        var n = e.data(this, "numeric.callback");
        var r = this.value;
        if (r != "") {
            var i = new RegExp("^\\d+$|\\d*" + t + "\\d+");
            if (!i.exec(r)) {
                n.apply(this)
            }
        }
    };
    e.fn.removeNumeric = function() {
        return this.data("numeric.decimal", null).data("numeric.negative", null).data("numeric.callback", null).unbind("keypress", e.fn.numeric.keypress).unbind("blur", e.fn.numeric.blur)
    };
    e.fn.getSelectionStart = function(e) {
        if (e.createTextRange) {
            var t = document.selection.createRange().duplicate();
            t.moveEnd("character", e.value.length);
            if (t.text == "") return e.value.length;
            return e.value.lastIndexOf(t.text)
        } else return e.selectionStart
    };
    e.fn.setSelection = function(e, t) {
        if (typeof t == "number") t = [t, t];
        if (t && t.constructor == Array && t.length == 2) {
            if (e.createTextRange) {
                var n = e.createTextRange();
                n.collapse(true);
                n.moveStart("character", t[0]);
                n.moveEnd("character", t[1]);
                n.select()
            } else if (e.setSelectionRange) {
                e.focus();
                e.setSelectionRange(t[0], t[1])
            }
        }
    }
})(jQuery);
// Events
$(document).ajaxComplete(managerSkuChoice);
$(document).ready(initManagerProduct);
$(window).load(onWindowLoad);

function initManagerProduct() {
    dataskumanager = new DataSkuManager(".topic.Cor");
    variacoesSku();

    // Setinput numeric
    $("input#quantidade").numeric();

    $(".sku-selector").live("change", function() {
        idInputSelected = $(this).attr("id");

        if (!$(this).is(":checked")) {
            $(this).parent().find("label").removeeClass("sku-picked");
            $(".cont_sku h2").text("Nenhuma cor selecionada");
            var ValorCorSelecionada = $('.produto .cont_sku h2').html();
            if (ValorCorSelecionada == 'Nenhuma cor selecionada') {

                $('.cont_sku').show();

            }
        } else {
            $(".select.skuList label").each(function() {
                if (idInputSelected == $(this).attr("for")) {
                    var spanText = $(this).text();
                    $(".cont_sku h2").text(spanText);
                }
            });

        }
    });
}

function managerSkuChoice() {
    // $(".load_sku").fadeOut(500);
    countAjaxComplete++;

    $(".thumb-color .Cor label").each(function() {
        if ($(this).find("span").length == 0) {
            if ($(this).hasClass("item_unavailable")) {
                $(this).append("<span></span>");
            }
        }
    });

    // console.info("Total labels: "+$("#produto .Variacao .skuList label").length);

    if ($("#produto .Cor .skuList label").length > 0) {
        // console.info("Pronto, pode marcar", countAjaxStop);

        if (countAjaxStop == 0) {
            if ($("body").hasClass("produto") /* || $("body").hasClass("productQuickView")*/ ) {
                checkNextAvaibleSku("topic");
                // checkNextAvaibleSku("topic");
                // checkNextAvaibleSku("Cores");
                // checkNextAvaibleSku("Tamanho");
                // checkNextAvaibleSku("Variacao");
            }

            countAjaxStop++;
        }
    }

    if (countAjaxComplete == 1) {
        if ($("ul.topic").length <= 0) {
            $(".cont_sku h2").hide();
        }
    }

}

$(".thumb-color .Cor label").live('click', function() {
    if ($('.notifyme').css('display') == 'block')
        $('.botao, #qtd_produto').hide();
    else
        $('.botao, #qtd_produto').show();
});
//fim managerSkuChoice

function checkNextAvaibleSku(skuSpec) {
    // console.info("Marque this topic: "+ skuSpec);
    var totalLabels = $("#produto ." + skuSpec + " .skuList label").length;
    var totalLabelsPicked = $("#produto .Cor .skuList label.sku-picked").length;
    //alert('Alguem marcado: '+totalLabelsPicked+'')
    var countCheckeds = 0;
    // console.info("totalLabels: "+totalLabels);

    if (totalLabelsPicked >= '1') {
        countCheckeds++;
    }

    //alert(countCheckeds);

    for (var i = 0; i < totalLabels; i++) {
        if (countCheckeds == 0) {
            // console.info("countCheckeds: "+ countCheckeds);
            if (!$("#produto ." + skuSpec + " .skuList label").eq(i).hasClass("item_unavaliable")) {
                //console.log("Check this"+ countCheckeds);
                $("#produto ." + skuSpec + " .skuList input").eq(i).trigger("click");

                $("#produto ." + skuSpec + " .skuList input").eq(i).trigger("change");

                countCheckeds++;

                // formaPagamentoExe();
            }
        }

        if (countCheckeds == 0 && i == totalLabels - 1) {
            $("#produto ." + skuSpec + " .skuList input").eq(0).trigger("click");

            $("#produto ." + skuSpec + " .skuList input").eq(0).trigger("change");
        }

    }
}

function variacoesSku() {
    /*sem variacao sku*/
    if ($('.selectSku ul li.skuList input').length == 1) {

        $('.dir').addClass("nenhum_sku");

        //pega a segunda parte do nome do produto para ser exibido em cima da referencia do produto
        var nameProduct = $('.productName').text();
        nameSplit = nameProduct.split(' - ');
        $('.cont_sku h2').html(nameSplit[1]);

        $('.cont_sku h2:contains("-")').show();
    }

    /*com variacao sku*/
    if ($('.selectSku ul li.skuList input').length > 1) {
        $('.dir').addClass("com_sku");
    }

}

function onWindowLoad() {
    $(".selectSku").removeClass("load");

    $(".selectSku label").click(function() {
        $(document).one("ajaxStop", function() {
            // FUNCACAO DE VALIDACAO DE SKU
            variacoesSku();
        });
    });
}


//alterar nome da marca na pagina de produto

function verificaNum(str) {
    var er = /^[0-9]+$/;
    return (er.test(str));
}

function DataSkuManager(selectorGroup) {
    var selector = selectorGroup;
    var _owner = this;
    var pathDataSku = "/produto/sku/";
    this.objSkusInfo = {
        skuList: []
    };
    var countLoaded = 0;

    setTimeout(function() {
        // Inicia
        initManager();
        //console.log('iniciou');
    }, 500);

    function initManager() {
        var textMl = $(".selectSku label").text();
        var textCopoBanho = $(".bread-crumb ul li:first-child + li a").html();
        var pName = $(".productName").text();

        // CASO TENHA "ml" NO SKU ELE REMOVE A CLASSE QUE FAZ O QUADRADINHO DE COR
        /*if(textMl.match("Cápsulas") || pName.match("Cápsulas") || textMl.match(/ml/) || textMl.match(/kit/) || textMl.match(/Kit/) || textMl.match(/0g/) || textMl.match(/1g/) || textMl.match(/2g/) || textMl.match(/3g/) || textMl.match(/4g/) || textMl.match(/5g/) || textMl.match(/6g/) || textMl.match(/7g/) || textMl.match(/8g/) || textMl.match(/9g/) || textCopoBanho == "Corpo e Banho"){
			
			$("body").removeClass("thumb-color");
			$(".produto .cont_princ_prod .dir.com_sku .cont_sku h2").hide();
			$(".load_sku").fadeOut(300);
						
		}else
		{*/
        if ($(selector).length > 0) {

            $("body").addClass("thumb-color");
            var lengthSkus;
            lengthSkus = $(selector).find("label").length;

            // Coleta infos iniciais

            $.each(skuJson_0.skus, function(index, value) {
                _owner.objSkusInfo.skuList[index] = new Object();
                _owner.objSkusInfo.skuList[index].id = value.sku;
                _owner.objSkusInfo.skuList[index].name = value.values[0];
                _owner.objSkusInfo.skuList[index].thumb = "";
                //_owner.objSkusInfo.skuList[index].toolTip = value.values[1];

                getJsonObjectFromSkuId(_owner.objSkusInfo.skuList[index].id, _owner.objSkusInfo.skuList[index].name);
            });

            // getJsonObjectFromSkuId(_owner.objSkusInfo.skuList[index].id, _owner.objSkusInfo.skuList[index].name);

        }
        //}
    }

    function getJsonObjectFromSkuId(intIdSku, skuName) {
        var urlJSONSkuInfos = pathDataSku + intIdSku;
        var objSucess;
        var _skuName = skuName;

        $.getJSON(urlJSONSkuInfos, function(data) {
            objSucess = data;

            try {
                var totalImages = objSucess[0]["Images"][1].length;
                if (typeof totalImages != "undefined") {

                    for (var i = 0; i < totalImages; i++) {
                        var archiveTypeId = objSucess[0]["Images"][1][i]["ArchiveTypeId"];

                        if (archiveTypeId == 3) {
                            var urlThumbSku = objSucess[0]["Images"][1][i]["Path"];

                            //console.info('Url Thumb'+urlThumbSku);
                            //console.info('Url Texture'+urlTexture);

                            $.each(_owner.objSkusInfo.skuList, function(index, value) {
                                if (value.id == intIdSku) {
                                    _owner.objSkusInfo.skuList[index].thumb = urlThumbSku;
                                    //_owner.objSkusInfo.skuList[index].toolTip = skuName;
                                }
                            });

                            $(selector).find("label").each(function(i, val) {
                                var thisTxt = $(this).text().replace("X", "");
                                if (skuName == thisTxt) {
                                    $(this).attr("title", skuName);
                                    $(this).css("background", "url('" + urlThumbSku + "') center center no-repeat scroll transparent");
                                    $(this).wrap("<div class='sku-option-wrap'></div>");

                                    $(this).parent(".sku-option-wrap").append("<div class='thumb-tooltip'>" + skuName + "</div>");
                                }
                            });

                            $(".load_sku").fadeOut(300);

                            $("#include ul.thumbs li").each(function() {
                                var thisThumb = $(this);
                                $.each(_owner.objSkusInfo.skuList, function(i, val) {

                                    if (thisThumb.find("img").attr("src") == val.thumb || thisThumb.find("span.tooltip").html() == skuName) {
                                        thisThumb.hide();
                                    }
                                });
                            });

                            $(selector).find("input").live("change", function() {
                                $("#include ul.thumbs li").each(function() {
                                    var thisThumb = $(this);
                                    $.each(_owner.objSkusInfo.skuList, function(i, val) {
                                        if (thisThumb.find("img").attr("src") == val.thumb || thisThumb.find("span.tooltip").html() == skuName) {
                                            thisThumb.hide();
                                        }
                                    });
                                });
                            });

                        }
                    }


                }
            } catch (error) {
                // console.warn(error);
            }

        });
    }
}


$(document).ready(function() {
    $(".thumb-color .Cor label").live('click', function() {
        if ($(this).find('span').length > 0) {
            $('.botao, #qtd_produto').hide();
        } else {
            $('.botao, #qtd_produto').show();
        }
    });
});
