$(document).ready(function(){

	$('.iniciar').click(function(){
		$(this).parent().fadeOut();
		$('.quiz').fadeIn();
		$('.perg-1').fadeIn();
	});


	$('.perg-1 ul li').click(function(){
		$('.perg-2').fadeIn();
		$(this).parents('.perg-1').fadeOut();

	});

	$('.perg-2 ul li').click(function(){
		$('.perg-3').fadeIn();
		$(this).parents('.perg-2').fadeOut();
	});

	$('.perg-3 ul li').click(function(){
		$('.perg-4').fadeIn();
		$(this).parents('.perg-3').fadeOut();
	});
	$('.perg-4 ul li').click(function(){
		$('.resultado').fadeIn();
		$(this).parents('.perg-4').fadeOut();
		if ($('.perg-1').hasClass('add-1')) {
			$('.resp-1').fadeIn();
			$('.colecao-1').fadeIn();
			$('.questionario').remove();
			
		}else if ($('.perg-1').hasClass('add-2')) {
			$('.resp-2').fadeIn();
			$('.colecao-2').fadeIn();
			$('.questionario').remove();
			
		}else if ($('.perg-1').hasClass('add-3')) {
			$('.resp-3').fadeIn();
			$('.colecao-3').fadeIn();
			$('.questionario').remove();
			
		}

	});


	$('.perg-1 ul li:eq(0)').click(function() {
		$('.perg-1').addClass('add-1');
	});

	$('.perg-1 ul li:eq(1)').click(function() {
		$('.perg-1').addClass('add-2');
	});

	$('.perg-1 ul li:eq(2)').click(function() {
		$('.perg-1').addClass('add-3');
	});

// Botão Voltar
	$('.voltar-1').click(function(){
		$('.perg-1').fadeIn();
		$(this).parents('.perg-2').fadeOut();

	});

	$('.voltar-2').click(function(){
		$('.perg-2').fadeIn();
		$(this).parents('.perg-3').fadeOut();

	});

	$('.voltar-3').click(function(){
		$('.perg-3').fadeIn();
		$(this).parents('.perg-4').fadeOut();

	});

});

