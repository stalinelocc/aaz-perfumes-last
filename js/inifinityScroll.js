/**
* Inifinity Scroll
* @author Carlos Vinicius
* @version 2.2
* @date 2011-04-10
*/
"function"!==typeof String.prototype.trim&&(String.prototype.trim=function(){return this.replace(/^\s+|\s+$/g,"")});
jQuery.fn.infinitScroll=function(g){var e=jQuery.extend({toTop:"#returnToTop",lastShelf:".prateleira:last",elemLoading:'<div id="scrollLoading">Carregando ... </div>',searchUrl:null,callback:function(){},getShelfHeight:function(){return c.scrollTop()+c.height()}},g),c=jQuery(this);jQuery("");var f="object"===typeof console;if(c.length<1)return c;var b=$(window),m=$(document),j=$("body"),d=j.find(e.toTop),k=jQuery(e.elemLoading),l=true;currentPage=2;var h={scrollToTop:function(){var a=b.height();b.bind("resize",
function(){a=b.height()});b.bind("scroll",function(){m.scrollTop()>a?d.stop(true).fadeTo(300,1,function(){d.show()}):d.stop(true).fadeTo(300,0,function(){d.hide()})});d.find("a").bind("click",function(){jQuery("html,body").animate({scrollTop:0},"slow");return false})},getSearchUrl:function(){var a,b,c;jQuery("script:not([src])").each(function(){b=jQuery(this)[0].innerHTML;c=/\/buscapagina\?.+&PageNumber=/i;if(b.search(/\/buscapagina\?/i)>-1){a=c.exec(b);return false}});if(typeof a!=="undefined"&&
typeof a[0]!=="undefined")return a[0];f&&console.log("[Erro] N\u00e3o foi poss\u00edvel localizar a url de busca da p\u00e1gina.\n Tente adicionar o .js ao final da p\u00e1gina. \n[M\u00e9todo: getSearchUrl]");return""},infinitScroll:function(){var a=j.find(".pager[id*=PagerTop]:first").attr("id")||"";if(""===a){f&&console.log("[Erro] N\u00e3o foi poss\u00edvel localizar o div.pages contendo o atributo id*=PagerTop");return""}var d=eval("pagecount_"+a.split("_").pop()),g=null!==e.searchUrl?e.searchUrl:
h.getSearchUrl(),i=true;"undefined"===typeof d&&console.log("[Erro] N\u00e3o foi poss\u00edvel localizar quantidade de p\u00e1ginas.\n Tente adicionar o .js ao final da p\u00e1gina. \n[M\u00e9todo: infinitScroll]");b.bind("scroll",function(){if(currentPage<=d&&l){if(b.scrollTop()+b.height()>=e.getShelfHeight()&&i){var a=c.find(e.lastShelf);if(a.length<1){f&&console.log("[Erro] \u00daltima Prateleira/Vitrine n\u00e3o encontrada \n ("+a.selector+")");return false}a.after(k);i=false;jQuery.ajax({url:g+
currentPage,success:function(b){if(b.trim().length<1){l=false;f&&console.log("[Aviso] N\u00e3o existem mais resultados a partir da p\u00e1gina: "+currentPage)}else a.after(b);i=true;k.remove()},error:function(){f&&console.log("[Error] Houve um erro na requisi\u00e7\u00e3o Ajax de uma nova p\u00e1gina.")}});currentPage++}}else return false})}};h.scrollToTop();h.infinitScroll();return c};

$(window).load(function(){
	$(".prateleira[id*=ResultItems]").infinitScroll();
});
$(document).ready(function(){
	$("body").append('<div id="returnToTop"><a href="#"><span class="text">voltar ao</span><span class="text2">TOPO</span><span class="arrowToTop"></span></a></div>');
});
